%module(directors="1") cosmox

%include "exception.i"
%exception {
    try {
        $action
    } catch (cosmox::CosmoxException& e) {
        PyErr_SetString(PyExc_RuntimeError, const_cast<char*>(e.what()));
        return NULL;
    } catch(const std::exception& e) {
      PyErr_SetString(PyExc_RuntimeError, const_cast<char*>(e.what()));
      return NULL;
    } catch(...) {
      SWIG_exception(SWIG_RuntimeError, "Unknown exception");
    }
}
%{
#include "cosmox_exception.h"
%};

%include "std_shared_ptr.i"
%shared_ptr(cosmox::Wigner)
%{
#include "special_functions.h"
%}
%include "std_complex.i"
%include "special_functions.h"

%include "std_pair.i"
%include "std_string.i"
%shared_ptr(cosmox::Perturbations)
%template(paird) std::pair<double, double>;
%{
#include "perturbations.h"
%};
%include "perturbations.h"

%shared_ptr(cosmox::GalaxyBias)
%shared_ptr(cosmox::HiBias)
%shared_ptr(cosmox::GeneralSpectra::get_perturbations)
%shared_ptr(cosmox::GeneralSpectra::get_galaxy_bias)
%feature("director") cosmox::GalaxyBias; /* Allow Python3 to override. */
%{
#include "survey_specs.h"
%};
%include "survey_specs.h"

%{
#include "general_spectra.h"
%};
%include "general_spectra.h"

%{
#include "spectrum.h"
%};
%include "spectrum.h"

%{
#include "bispectrum.h"
%};
%include "bispectrum.h"
