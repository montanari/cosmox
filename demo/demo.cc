// demo.cc -- CosmoX C++ interface demo.

// Compile and run:
// $  g++ -o demo demo.cc -lcosmox -std=c++17
// $  ./demo

// Copyright 2017, 2018, 2019 Francesco Montanari

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <iostream>
#include <string>
#include <tuple>

#include <cosmox/general_spectra.h>
#include <cosmox/perturbations.h>

void
main_perturbations ()
{
  // Let's run the CLASS code to create a CosmoX `Perturbations`
  // instance.
  auto inifile = "cosmoclass.ini";
  auto pt = cosmox::Perturbations::create_perturbs (cosmox::kCosmoclass,
                                                    inifile);

  // Print range of valid wavenumbers.
  double kmin;
  double kmax;
  std::tie(kmin, kmax) = pt->get_k_lims();
  std::cout << "Wavenumbers range [1/Mpc]: ("
            << kmin << ", " << kmax
            << ")\n";

  // Background functions.
  std::cout << "Comoving distance [Mpc] at z=0.5: "
            << pt->bg_comoving_distance(0.5) << "\n";

  std::cout << "Hubble parameter [1/Mpc] at z=0.5: "
            << pt->bg_Hubble(0.5) << "\n";

  // Perturbations.
  std::cout << "Primordial power spectrum at k=0.01: "
            << pt->get_primordial_spectrum (0.01) << "\n";

  auto stype = cosmox::Perturbations::SourceType::kDeltaM;
  std::cout << "Density source function at k=0.01, z=0.5: "
            << pt->get_source(stype, 0.01, 0.5) << "\n";
}

void
main_generalized_spectra ()
{
  // Create an instance of CosmoX GeneralSpectra using the CLASS code
  // to compute perturbations.
  auto inifile = "cosmoclass.ini";
  auto pt = cosmox::Perturbations::create_perturbs (cosmox::kCosmoclass,
                                                    inifile);
  auto gsp = cosmox::GeneralSpectra(pt);

  // Generalized spectra are complex numbers in general (see equation
  // A.8 of arxiv:1510.04202). For instance, consider the following
  // one based on the density transfer function.
  auto n = 0;
  auto z1 = 0.5;
  auto z2 = 0.5;
  auto l1 = 10;
  auto l2 = l1+1;
  std::cout << "A purely imaginary generalized power spectrum: "
            << gsp.compute(l1, l2, z1, z2, n,
                           gsp.kDensity, gsp.kDensity) << "\n";

  // Compute density and velocity (redshift-space distortions) angular
  // spectra.
  l2 = l1;
  std::cout << "Density C_l(z,z) for l=" << l1
            << " and z=" << z1 << ": "
            << gsp.compute(l1, l2, z1, z2, n,
                           gsp.kDensity, gsp.kDensity).real() << "\n";
  std::cout << "Velocity C_l(z,z) for l=" << l1
            << " and z=" << z1 << ": "
            << gsp.compute(l1, l2, z1, z2, n,
                           gsp.kVelocity1, gsp.kVelocity1).real() << "\n";
}

int
main ()
{
  main_perturbations ();
  main_generalized_spectra ();
}
