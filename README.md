<!-- -*- mode: markdown -*- -->

This is CosmoX, a cosmological correlations calculator.

  Copyright 2017-2020 Francesco Montanari<br />
  Copyright 2017-2018 Enea Di Dio

  Copying and distribution of this file, with or without modification,
  are permitted in any medium without royalty provided the copyright
  notice and this notice are preserved.

Home page: https://gitlab.com/montanari/cosmox

CosmoX is free software.  See the file COPYING for copying conditions.

# Description

CosmoX computes cosmological correlations via interfaces to external
codes (for instance, to the CLASS Boltzmann solver code to compute
cosmological perturbations and initial power spectra).

The library is focused on exploratory computation of generalized
angular power spectra and of angular bispectra (for efficient and
robust computation of angular spectra refer directly, e.g., to the
CLASS code).

# Setup

## Dependencies

To build the software you need a compiler (preferably GCC) supporting
`C++17`, and `Python3` (including header files, e.g., *Debian:* `apt
install python3-dev`).

The following libraries are required to build and run CosmoX. See their
documentation for installation instructions. Common GNU distributions provide
some pre-built packages available for simple installation through the their
package managers (we show the package names on Debian repositories, if
available).

- [CLASS](http://class-code.net/) (>= 2; last tested version: 2.6.3).
  Cosmological perturbations.

- [Cuba](http://www.feynarts.de/cuba/) (>= 4.2). Multidimensional
  numerical integration.

- [Gnu Scientific Library (GSL)](https://www.gnu.org/software/gsl/)
  (>= 2.3). *Debian:* `apt install libgsl23 libgsl-dev`.

- [SWIG](http://www.swig.org/) (>=3). Generate Python3
  wrapper. *Debian:* `apt install swig`.

- [WIGXJPF](http://fy.chalmers.se/subatom/wigxjpf/) (>= 1.7). Evaluate
  Wigner 3j, 6j and 9j symbols accurately using prime factorization
  and multi-word integer arithmetic.

### Optional

- [Doxygen](http://www.stack.nl/~dimitri/doxygen/) (>=
  1.8.13). Generate documentation. *Debian users:* `apt install doxygen`.

## Download

Releases are available [here](https://gitlab.com/montanari/cosmox/tags). 

Cloning the [git repository](https://gitlab.com/montanari/cosmox) (or
downloading an archive not prepared for distribution) is only recommended for
internal code development, see [CONTRIBUTING](CONTRIBUTING.md). Only the release
archives linked above contain pre-built configuration scripts that allows
standard installation steps.

## Installation

**Note.** If you do not use a release archive (e.g., if you clone the git
repository) or change `configure.ac` you'll need to have GNU Autotools and
Libtool installed to build configuration scripts. See
[CONTRIBUTING](CONTRIBUTING.md).

Installation follows a standard GNU build system. See the section
below about non-installed dependencies for more configuration
information.
```shell
./configure   # See configuration section below
make
make check    # Optional
make install  # Needs root privileges
```
(Running all tests currently takes about 5 minutes.)

Documentation is generated with:
```shell
make docs
```

### Configuration
#### Non-installed dependencies

The CLASS, WIGXJPF and CUBA libraries don't provide install targets in
their build system. This means that after compiling the libraries,
their paths must be manually specified in the `LIBS` and `CPPFLAGS`
compiler flags. One way to do this is to configure CosmoX as follows.
(Replace, `/path/to/` with your path to the code directory.)

```shell
./configure LIBS="-L/path/to/class/ -L/path/to/wigxjpf/lib/ -L/path/to/cuba/" \
            CPPFLAGS="-I/path/to/class/include/ -I/path/to/wigxjpf/inc -I/path/to/cuba/"
```

If the libraries are updated or change location, CosmoX must be
configured again (it is convenient to write the former configuration
line into a shell script).

#### Optimization flags
To enhance performance, configure optimization flags for the
compiler. For instance (`<...>` denotes other optional flags):

```shell
./configure <...> CXXFLAGS="-O3"
```


## Usage

We provide interfaces for C++ and Python3. See the documentation for more
information, and the [demo](demo) directory for usage examples.

All length units in the code are *Mpc*.

Spatial curvature is always assumed to be vanishing (setting a
non-zero value, e.g., in the CLASS code would bring inconsistent
results).

### C++
The following headers are distributed with the C++ library
`libcosmox`:
- `<cosmox/bispectrum.h>`
- `<cosmox/bispectrum_density.h>`
- `<cosmox/cosmox_exception.h>`
- `<cosmox/cubapp.h>`
- `<cosmox/general_spectra.h>`
- `<cosmox/numeric.h>`
- `<cosmox/perturbations.h>`
- `<cosmox/special_functions.h>`
- `<cosmox/survey_specs.h>`
- `<cosmox/spectrum.h>`

Compile with the `-lcosmox` flag.

### Python3
Import the `cosmox` module. This is a unified wrapper to all of the
C++ library public headers.

When referring to the C++ documentation, namespaces for a given class
are accessed replacing dots by underscores. For instance, the C++
member `Perturbations.create_perturbs()` is available through the
Python3 interface as `cosmox.Perturbations_create_perturbs()`.

To minimize the use of ad-hoc wrapper functions, the Python3 API is
somewhat conservative. E.g., if a function expects an integer, passing
a float will give error (because of C++ type checking).

# Development

See [CONTRIBUTING](CONTRIBUTING.md).

# Cite

If you use this code in a scientific publication, please cite the
following work:

- E. Di Dio, R. Durrer, R. Maartens, F. Montanari, and O. Umeh, *The
  Full-Sky Angular Bispectrum in Redshift Space*,
  [arXiv:1812:09297](https://arxiv.org/abs/1812.09297)
  [astro-ph.CO]. <!-- [Bibtex] -->
