// perturbations.cc -- compute perturbations.
//
// Copyright 2017, 2018, 2019 Francesco Montanari
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// Including CLASS requires special handle due to name clashes with
// standard macros.

#include "perturbations.h"
#include "perturbations_cosmoclass.h"

namespace cosmox {
  std::shared_ptr<Perturbations>
  Perturbations::create_perturbs(const PerturbMethod method,
                                 const std::string inifile)
  {
    switch (method)
      {
      case kCosmoclass:
        return std::make_shared<PerturbsCosmoclass>(inifile);
        // Add a condition for each method.
      }

    throw CosmoxException("requested source function method not implemented",
                          __FILE__, __LINE__);
  }
} // namespace cosmox
