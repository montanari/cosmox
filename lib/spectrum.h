// spectrum.h -- angular power spectrum.
//
// Copyright 2020 Francesco Montanari
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/** @file spectrum.h
 *
 *  Angular power spectrum.
 */

#ifndef SPECTRUM_H
#define SPECTRUM_H

#include <utility>
#include <vector>

#include "cubapp.h"
#include "general_spectra.h"
#include "survey_specs.h"

namespace cosmox
{

/** @brief Angular power spectrum.
 *
 * This convenience class wraps cosmox::GeneralSpectra to compute
 * the frequent case \f$ C_{l}^{AB}(z_1, z_2) \equiv {}^0 C_{l \
 * l}^{AB}(z_1, z_2) \f$ case.
 */
class Spectrum : public GeneralSpectra
{
public:
  // This pedantic constructor definition allows it to wrap it
  // automatically with SWIG.
  Spectrum (std::shared_ptr<Perturbations> perturbs,
            std::shared_ptr<GalaxyBias> galbias)
      : GeneralSpectra (perturbs, galbias)
  {
  }

  Spectrum (std::shared_ptr<Perturbations> perturbs)
      : GeneralSpectra (perturbs)
  {
  }

  /** @brief Compute power spectrum.
   *
   *  @param[in] ell     multipole.
   *  @param[in] z1      ttype_A redshift.
   *  @param[in] z2      ttype_B redshift.
   *  @param[in] ttype_A source function type.
   *  @param[in] ttype_B source function type.
   *  @param[in] epsrel  relative precision for the integration.
   *  @param[in] epsabs  absolute precision for the integration.
   */
  double compute (const int ell, const double z1, const double z2,
                  const TransferType ttype_A, const TransferType ttype_B,
                  const double epsrel = 1e-4, const double epsabs = 0);

  /** @brief Compute power spectrum including window functions.
   *
   *  @param[in] ell     multipole.
   *  @param[in] w1      ttype_A window.
   *  @param[in] w2      ttype_B window.
   *  @param[in] ttype_A source function type.
   *  @param[in] ttype_B source function type.
   *  @param[in] epsrel  relative precision for the integration.
   *  @param[in] epsabs  absolute precision for the integration.
   */
  //
  // If needed we could define epsrel_w, epsabs_w for the
  // integration over window functions (Cuba), different from
  // epsrel, epsabs for the integration along the line-of-sight
  // (GSL). It may also be useful to allow users to set maxeval
  // (Cuba) and choose IntegType (GSL).
  double compute (const int ell, const std::shared_ptr<Window> w1,
                  const std::shared_ptr<Window> w2, const TransferType ttype_A,
                  const TransferType ttype_B, const double epsrel = 1e-2,
                  const double epsabs = 0);

private:
  int maxeval = 100000;
  static std::vector<double> compute_sp_integrand (std::vector<double> zz,
                                                   void* params);

  struct Params
  {
    Spectrum* sp;
    int ell;
    TransferType ttype_A;
    TransferType ttype_B;
    double epsrel;
    double epsabs;
    std::shared_ptr<Window> w1;
    std::shared_ptr<Window> w2;
  };
};
} /* namespace cosmox */

#endif /* SPECTRUM_H */
