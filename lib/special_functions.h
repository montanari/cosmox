// special_functions.h -- Special functions.

// Copyright 2017, 2018, 2019 Francesco Montanari

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/** @file special_functions.h
 *  @brief Special functions.
 */

#ifndef SPECIAL_FUNCTIONS_H
#define SPECIAL_FUNCTIONS_H

#include <algorithm>
#include <cmath>
#include <complex>
#include <iomanip>
#include <mutex>
#include <vector>

namespace cosmox {
  /** @brief Gamma function (Lanczos approximation).
   *
   *  Depending on the arguments, the performance is similar to the
   *  Gamma function computed by the ARB library, or better (tested up
   *  to a factor \f$\sim5\f$). Note that ARB is more accurate and may
   *  compute up to larger arguments without failing (inf/nan).
   */
  std::complex<double> sf_gamma(std::complex<double> z);

  /// @cond DEV
  /** Spherical Bessel function, closed form for l<7. */
  double sf_bessel_j_less_7(const int l, const double x);

  /** Spherical Bessel function for l>=7. */
  double sf_bessel_j_geq_7(const int l, const double x);
  /// @endcond

  /** @brief Spherical Bessel function \f$ j_l(x) \f$.
   *
   *  Fast algorithm adapted from the <a
   *  href="http://arxiv.org/abs/1104.2933">CLASS</a> code (v1).
   */
  double sf_bessel_j(const int l, const double x);

  /** @brief Spherical Bessel function 1st derivative \f$ j'_l(x) \f$. */
  double sf_d_bessel_j(const int l, const double x);

  /** @brief Spherical Bessel function 2nd derivative \f$ j''_l(x) \f$. */
  double sf_dd_bessel_j(const int l, const double x);

  /** @brief Spherical Bessel function 3rd derivative \f$ j'''_l(x) \f$. */
  double sf_ddd_bessel_j(const int l, const double x);

  /** @brief Evaluate Wigner 3j, 6j and 9j symbols.
   *
   *  This is a C++ wrapper for the <a
   *  href="http://fy.chalmers.se/subatom/wigxjpf/">WIGXJPF</a>
   *  library. WIGXJPF evaluates Wigner 3j, 6j and 9j symbols
   *  accurately using prime factorisation and multi-word integer
   *  arithmetic.
   *
   *  @warning As a consequence of WIGXJPF table initialization, the
   *           __constructor is not thread-safe__. However, class
   *           members are thread-safe and can be used in
   *           multi-threaded program sections, provided that the
   *           class is instantiated by a single thread.
   *
   *  @note To be able to handle integer and half-integer arguments,
   *  the Wigner symbols arguments are \f$ 2j \f$ and \f$ 2m \f$.
   *
   *  Example:
   *  @code{cpp}
   *  #include "special_functions.h"
   *
   *  int
   *  main (int argc, char **argv)
   *  {
   *    cosmox::Wigner wig(2*100, 9);
   *    auto val3j = wig.get_3j(2*10, 2*15, 2*10,
   *                            2*(-3), 2*12 , 2*(-9));
   *    printf ("3J(10  15  10; -3  12  -9):      %#25.15f\n", val3j);
   *    return 0;
   *  }
   *  @endcode
   */
  class Wigner {
  public:
    /** @warning Not thread-safe.
     *
     *  @param[in] max_two_j   Twice the maximum required \f$ j \f$.
     */
    Wigner(const int max_two_j);
    ~Wigner();

    /** @brief 3j Wigner symbol. */
    double get_3j(const int two_j1, const int two_j2, const int two_j3,
                  const int two_m1, const int two_m2, const int two_m3);

    /** @brief 6j Wigner symbol. */
    double get_6j(const int two_j1, const int two_j2, const int two_j3,
                  const int two_j4, const int two_j5, const int two_j6);

    /** @brief 9j Wigner symbol. */
    double get_9j(const int two_j1, const int two_j2, const int two_j3,
                  const int two_j4, const int two_j5, const int two_j6,
                  const int two_j7, const int two_j8, const int two_j9);

    /** @brief Maximum 2*j value used to initialize the Wigner table. */
    int get_max_two_j();

  private:
    int max_two_j;

    // Type 9 is relatively rarely needed, but runtime is little
    // affected if only lower-order types are needed. Setting here the
    // largest possible value (among 3, 6, 9) avoids requiring the
    // user to pass it as input parameter.
    int wigner_type = 9;

    void table_init(int max_two_j, int wigner_type);
    void temp_init(int max_two_j);
  };

} /* namespace cosmox */

#endif /* SPECIAL_FUNCTIONS_H */
