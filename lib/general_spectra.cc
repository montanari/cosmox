// general_spectra.cc -- generalized power spectra.
//
// Copyright 2017, 2018, 2019 Francesco Montanari
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "cosmox_exception.h"

#include "general_spectra.h"

namespace cosmox {
  void check_wigner(const int max_two_j, const std::shared_ptr<Wigner> wig)
  {
    if (wig->get_max_two_j() < max_two_j)
      {
        auto msg = "Need Wigner::max_two_j >= " + std::to_string(max_two_j);
        throw CosmoxException(msg, __FILE__, __LINE__);
      }
  }

  double
  geometry_A (const int l1, const int l2, const int l3,
              const std::shared_ptr<Wigner> wig)
  {
    int two_l1=2.*l1;
    int two_l2=2.*l2;
    int two_l3=2.*l3;
    int two_m=2.;
    int max_two_l = std::max(std::max(two_l1, two_l2), two_l3);
    check_wigner(max_two_l, wig);
    return 0.5 / wig->get_3j(two_l1, two_l2, two_l3, 0, 0, 0)
      * (wig->get_3j(two_l1, two_l2, two_l3, 0, two_m, -two_m)
         + wig->get_3j(two_l1, two_l2, two_l3, 0, -two_m, two_m));
  }


  double
  geometry_reduced_Gaunt (const int l1, const int l2, const int l3,
              const std::shared_ptr<Wigner> wig)
  {
    int two_l1=2.*l1;
    int two_l2=2.*l2;
    int two_l3=2.*l3;
    int max_two_j = std::max(std::max(two_l1, two_l2), two_l3);
    check_wigner(max_two_j, wig);
    return sqrt((two_l1+1.) * (two_l2+1.) * (two_l3+1.) / (4.*M_PI))
      * wig->get_3j(two_l1, two_l2, two_l3, 0, 0, 0);
  }


  double
  geometry_Q (const int l1, const int l2, const int l3,
              const int l4, const int l5, const int l6,
              const std::shared_ptr<Wigner> wig)
  {
  int two_l1=2.*l1;
  int two_l2=2.*l2;
  int two_l3=2.*l3;
  int two_l4=2.*l4;
  int two_l5=2.*l5;
  int two_l6=2.*l6;
  int max_two_j = std::max(std::max(std::max(two_l1, two_l2),
                                    std::max(two_l3, two_l4)),
                           std::max(two_l5, two_l6));
  check_wigner(max_two_j, wig);

  double geomI = sqrt(pow(4.*M_PI, 3.)
                      * (two_l1+1.) * (two_l2+1.) * (two_l3+1.))
    * wig->get_3j(two_l4, two_l6, two_l1, 0, 0, 0)
    * wig->get_3j(two_l5, two_l4, two_l2, 0, 0, 0)
    * wig->get_3j(two_l6, two_l5, two_l3, 0, 0, 0);

  double geomQ = geomI * pow(-1., l4+l5+l6)
    * wig->get_6j(two_l1, two_l2, two_l3, two_l5, two_l6, two_l4);

  return geomQ;
  }

  GeneralSpectra::GeneralSpectra (std::shared_ptr<Perturbations> pt,
                                  std::shared_ptr<GalaxyBias> gb)
    : perturbs(pt)
    , galbias(gb)
  {
    std::tie (this->kmin, this->kmax) = this->perturbs->get_k_lims ();
  }

  GeneralSpectra::GeneralSpectra (std::shared_ptr<Perturbations> pt)
    : perturbs(pt)
  {
    std::tie (this->kmin, this->kmax) = this->perturbs->get_k_lims ();
    this->galbias = std::make_shared<GalaxyBias>();
  }

  std::shared_ptr<Perturbations>
  GeneralSpectra::get_perturbations ()
  {
    return perturbs;
  }

  std::shared_ptr<GalaxyBias>
  GeneralSpectra::get_galaxy_bias ()
  {
    return galbias;
  }

  std::complex<double>
  GeneralSpectra::compute (const int l1, const int l2,
                           const double z1, const double z2,
                           const int n,
                           const TransferType ttype_A,
                           const TransferType ttype_B,
                           const double epsrel,
                           const double epsabs,
                           const IntegType integtype)
  {
    Params params;
    params.pt = perturbs;
    params.l1 = l1;
    params.l2 = l2;
    params.z1 = z1;
    params.z2 = z2;
    params.n = n;
    params.ttype_A = ttype_A;
    params.ttype_B = ttype_B;
    params.galbias_z1 = this->galbias->b1(z1);
    params.galbias_z2 = this->galbias->b1(z2);
    GslIntegratorResult result;
    if (((integtype==kMix) and (z1==z2)) or (integtype==kQag))
      {
        GslIntegrator integrator(1e4);
        integrator.epsrel = epsrel;
        integrator.epsabs = epsabs;
        integrator.key = GSL_INTEG_GAUSS61;
        integrator.limit = 1e4;
        result = integrator.qag(&integrand, kmin, kmax, &params);
      }
    else if (((integtype==kMix) and (z1!=z2)) or (integtype==kCquad))
      {
        GslIntegratorCquad integrator;
        integrator.epsrel = epsrel;
        integrator.epsabs = epsabs;
        auto rounding_eps = 1e-8;   // Avoid out of range interpolation error.
        result = integrator.cquad(&integrand,
                                  (1.+rounding_eps)*kmin,
                                  (1.-rounding_eps)*kmax,
                                  &params);
      }
    else
      {
        throw CosmoxException("Not implemented", __FILE__, __LINE__);
      }
    std::complex<double> i(0, 1);
    return std::pow(i, l1-l2) * 4.*M_PI * result.integral;
  }


  double
  GeneralSpectra::integrand (double k, void* params)
  {
    Params* pars = static_cast<Params*>(params);
    return pow (k, pars->n-1.)
      * pars->pt->get_primordial_spectrum (k)
      * get_transfer (pars->ttype_A, pars->pt, pars->l1, k, pars->z1,
                      pars->galbias_z1)
      * get_transfer (pars->ttype_B, pars->pt, pars->l2, k, pars->z2,
                      pars->galbias_z2);
  }

  double
  transfer_density (std::shared_ptr<Perturbations> perturbs, const int ell,
                    const double k, const double z)
  {
    auto stype = Perturbations::SourceType::kDeltaM;
    return perturbs->get_source (stype, k, z)
           * sf_bessel_j (ell, k * perturbs->bg_comoving_distance (z));
  }

  double
  transfer_density_1 (std::shared_ptr<Perturbations> perturbs, const int ell,
                      const double k, const double z)
  {
    auto stype = Perturbations::SourceType::kDeltaM;
    auto comoving_H = perturbs->bg_Hubble (z) / (1. + z);
    return k / comoving_H * perturbs->get_source (stype, k, z)
           * sf_d_bessel_j (ell, k * perturbs->bg_comoving_distance (z));
  }

  double
  transfer_velocity_0 (std::shared_ptr<Perturbations> perturbs, const int ell,
                       const double k, const double z)
  {
    auto stype = Perturbations::SourceType::kThetaM;
    auto comoving_H = perturbs->bg_Hubble (z) / (1. + z);
    return perturbs->get_source (stype, k, z) / comoving_H
           * sf_bessel_j (ell, k * perturbs->bg_comoving_distance (z));
  }

  double
  transfer_velocity (std::shared_ptr<Perturbations> perturbs, const int ell,
                     const double k, const double z)
  {
    auto stype = Perturbations::SourceType::kThetaM;
    return perturbs->get_source (stype, k, z) / k
           * sf_d_bessel_j (ell, k * perturbs->bg_comoving_distance (z));
  }

  double
  transfer_velocity_1 (std::shared_ptr<Perturbations> perturbs, const int ell,
                       const double k, const double z)
  {
    auto stype = Perturbations::SourceType::kThetaM;
    auto comoving_H = perturbs->bg_Hubble (z) / (1. + z);
    return perturbs->get_source (stype, k, z) / comoving_H
           * sf_dd_bessel_j (ell, k * perturbs->bg_comoving_distance (z));
  }

  double
  transfer_velocity_2 (std::shared_ptr<Perturbations> perturbs, const int ell,
                       const double k, const double z)
  {
    auto stype = Perturbations::SourceType::kThetaM;
    auto comoving_H = perturbs->bg_Hubble (z) / (1. + z);
    return perturbs->get_source (stype, k, z) * k / pow (comoving_H, 2.)
           * sf_ddd_bessel_j (ell, k * perturbs->bg_comoving_distance (z));
  }

  double
  transfer_lensing_integrand (double x, void *params)
  {
    LensingParams *pars = static_cast<LensingParams *> (params);
    auto stype = Perturbations::SourceType::kPhiPlusPsi;
    auto z_at_x = pars->pt->bg_z_of_tau (pars->pt->bg_comoving_age () - x);
    auto r = pars->pt->bg_comoving_distance (pars->z);
    return (r - x) / (r * x) * pars->pt->get_source (stype, pars->k, z_at_x)
           * sf_bessel_j (pars->ell, pars->k * x);
  }

  double
  transfer_lensing (std::shared_ptr<Perturbations> perturbs, const int ell,
                    const double k, const double z, double epsrel)
  {
    LensingParams params;
    params.pt = perturbs;
    params.ell = ell;
    params.k = k;
    params.z = z;
    auto r = perturbs->bg_comoving_distance (z);
    GslIntegratorCquad integrator;
    integrator.epsrel = epsrel;
    auto result
        = integrator.cquad (&transfer_lensing_integrand, 0.0, r, &params);
    return ell * (ell + 1.) * result.integral;
  }

  double
  get_transfer (const GeneralSpectra::TransferType ttype,
                std::shared_ptr<Perturbations> perturbs, const int ell,
                const double k, const double z, const double galbias)
  {
    switch (ttype)
      {
      case GeneralSpectra::kDensity:
        return transfer_density (perturbs, ell, k, z);
      case GeneralSpectra::kDensity1:
        return transfer_density_1 (perturbs, ell, k, z);
      case GeneralSpectra::kVelocity0:
        return transfer_velocity_0 (perturbs, ell, k, z);
      case GeneralSpectra::kVelocity:
        return transfer_velocity (perturbs, ell, k, z);
      case GeneralSpectra::kVelocity1:
        return transfer_velocity_1 (perturbs, ell, k, z);
      case GeneralSpectra::kVelocity2:
        return transfer_velocity_2 (perturbs, ell, k, z);
      case GeneralSpectra::kDensityG_kVelocity1:
        return (galbias * transfer_density (perturbs, ell, k, z)
                + transfer_velocity_1 (perturbs, ell, k, z));
      case GeneralSpectra::kLensing:
        return transfer_lensing (perturbs, ell, k, z);
      default:
        throw CosmoxException ("Transfer type not implemented", __FILE__,
                               __LINE__);
      }
  }
  } /* namespace cosmox */
