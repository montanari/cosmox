// perturbations_cosmoclass.cc -- perturbations from CLASS code.
//
// Copyright 2017, 2018, 2019 Francesco Montanari
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// Including CLASS requires special handle due to name clashes with
// standard macros.

#include "perturbations_cosmoclass.h"

namespace cosmox {

  // BackgroundAtTau::BackgroundAtTau(const double redshift)
  // {
  //   double tau;
  //   background_tau_of_z(&ba, redshift, &tau);
  //   double* pvecback = new double[ba.bg_size*sizeof(double)];
  //   int last_index;
  //   if (background_at_tau(&ba, tau, ba.long_info, ba.inter_normal,
  //                         &last_index, pvecback) == _FAILURE_)
  //     {
  //       std::string msg = "Error in background_at_tau \n";
  //       msg += ba.error_message;
  //       throw CosmoxException(msg, __FILE__, __LINE__);
  //     }
  // }


  // BackgroundAtTau::get_quantity(const int idx)
  // {
  //   return pvecback[ba.index_bg_H];
  // }

  // BackgroundAtTau::~BackgroundAtTau
  // {
  //   delete[] pvecback;
  // }

  double
  PerturbsCosmoclass::get_background_double(const double redshift, const int index)
  {
    double tau;
    background_tau_of_z(&ba, redshift, &tau);
    double* pvecback = new double[ba.bg_size*sizeof(double)];
    int last_index;
    if (background_at_tau(&ba, tau, ba.long_info, ba.inter_normal,
                          &last_index, pvecback) == _FAILURE_)
      {
        std::string msg = "Error in background_at_tau \n";
        msg += ba.error_message;
        throw CosmoxException(msg, __FILE__, __LINE__);
      }
    auto quantity = pvecback[index];
    delete[] pvecback;
    return quantity;
  }

  PerturbsCosmoclass::PerturbsCosmoclass(std::string inifile)
  {
    this->argc = 2;
    char *cinifile = new char[inifile.length()+1];
    strcpy(cinifile, inifile.c_str());
    char* argv[] = {nullptr, cinifile};
    this->argv = argv;

    // File access may be worth a separate check, e.g., verifying if
    // there is any *.ini from command line arguments.
    if ((inifile.length() == 0) || (access(this->argv[1], F_OK) == -1))
      {
        // Terminate to avoid segmentation fault from CLASS.
        throw CosmoxException("CLASS *.ini file not provided, "
                              "or not accessible\n", __FILE__, __LINE__);
      }

    const int stp_status = this->setup();
    if (stp_status == EXIT_FAILURE)
      throw CosmoxException("Error in CLASS", __FILE__, __LINE__);
    if (!this->pt.has_perturbations)
      throw CosmoxException("Error: No perturbation requested, you must set "
                            "CLASS output to nCl or similar",
                            __FILE__, __LINE__);

    this->set_index_pt();       // Set the pt.index_... values.
    this->interpolate();        // Interpolate all source functions.
    delete [] cinifile;
  }


  PerturbsCosmoclass::~PerturbsCosmoclass()
  {
    this->teardown();
  }


  void
  PerturbsCosmoclass::interpolate()
  {
    const int k_size = pt.k_size[index_md];
    const int tau_size = pt.tau_size;
    double* source_kt = new double[k_size*tau_size]; // Check delete[].

    for (auto it : index_tp)
      {
        // First element of index_tp: source type.
        // Second element of index_tp: corresponding CLASS index.

        // Source at interpolating points.
        this->fill_source(it.second, k_size, tau_size,
                          source_kt);

        // Interpolation table.
        interp2d[it.first] = std::make_unique<GslInterp2d>
          (pt.k[index_md], pt.tau_sampling, source_kt,
           k_size, tau_size);
      }

    delete[] source_kt;
  }


  void
  PerturbsCosmoclass::fill_source(const int index_type,  const int k_size,
                                  const int tau_size, double *source_kt)
  {
    int index_k, index_tau;
    int count = 0;
    for(index_k=0; index_k<k_size; ++index_k)
      for(index_tau=0; index_tau<tau_size; ++index_tau)
        {
          source_kt[count] = pt.sources[index_md]
            [index_ic * pt.tp_size[index_md] + index_type]
            [index_tau * pt.k_size[index_md] + index_k];
          ++count;
        }
  }


  void
  PerturbsCosmoclass::set_index_pt()
  {
    // Mode.
    if ((!pt.has_scalars) or (pt.md_size > 1))
        throw CosmoxException("Only scalar modes are implemented.",
                              __FILE__, __LINE__);
    index_md = pt.index_md_scalars;

    // Initial conditions.
    if ((!pt.has_ad) or (pt.ic_size[index_md] > 1))
        throw CosmoxException("Only adiabatic conditions are implemented.",
                              __FILE__, __LINE__);
    this->index_ic = pt.index_ic_ad;

    // Source type.
    if (pt.has_source_delta_m)
      index_tp[kDeltaM] = pt.index_tp_delta_m;
    if (pt.has_source_theta_m)
      index_tp[kThetaM] = pt.index_tp_theta_m;
    if (pt.has_source_phi_plus_psi)
      index_tp[kPhiPlusPsi] = pt.index_tp_phi_plus_psi;
  }


  const double
  PerturbsCosmoclass::get_source(const SourceType stype,
                                 const double wavenumber,
                                 const double redshift)
  {
    double tau;
    background_tau_of_z(&ba, redshift, &tau);
    try
      {
        return interp2d.at(stype)->eval(wavenumber, tau);
      }
    catch (const std::out_of_range& e)
      {
        throw CosmoxException("SourceType " + std::to_string(stype) +
                              " is out of range. The corresponding "
                              "perturbation may have not been requested "
                              "in the CLASS configuration file.",
                              __FILE__, __LINE__);
      }
  }


  const double
  PerturbsCosmoclass::get_primordial_spectrum(const double wavenumber)
  {
    if (pm.ic_size[index_md] > 1)
      throw CosmoxException("Only adiabatic initial conditions are "
                            "implemented.", __FILE__, __LINE__);

    double primordial_pk[1];    // Only one type of initial conditions
                                // (adiabatic).
    if (primordial_spectrum_at_k(&pm, index_md,
                                 linear, // This is an enum from CLASS.
                                 wavenumber, primordial_pk) == EXIT_FAILURE)
      {
        throw CosmoxException("Error in primordial power spectrum.",
                              __FILE__, __LINE__);
      }

    return primordial_pk[0];
  }


  std::pair<double, double>
  PerturbsCosmoclass::get_k_lims()
  {
    return std::make_pair(pt.k_min, pt.k_max);
  }


  double
  PerturbsCosmoclass::bg_comoving_distance(const double redshift)
  {
    double tau;                               // Conformal time.
    background_tau_of_z(&ba, redshift, &tau); // Get tau given the redshift z
    double tau0 = ba.conformal_age;
    return tau0-tau;
  }

  double
  PerturbsCosmoclass::bg_Hubble(const double redshift)
  {
    return get_background_double(redshift, ba.index_bg_H);
  }

  double
  PerturbsCosmoclass::bg_velocity_growth_factor(const double redshift)
  {
    return get_background_double(redshift, ba.index_bg_f);
  }

  double
  PerturbsCosmoclass::bg_z_of_tau(const double tau)
  {
    if (z_of_tau_interp == nullptr)
        z_of_tau_interp = std::make_unique<GslInterpCspline>
          (ba.tau_table, ba.z_table, ba.bt_size);
    return z_of_tau_interp->eval(tau);
  }


  double
  PerturbsCosmoclass::bg_comoving_age()
  {
    return ba.conformal_age;
  }


  void
  PerturbsCosmoclass::print_version()
  {
    std::cout << "Running CLASS" << _VERSION_ << std::endl;
  }


  const int
  PerturbsCosmoclass::setup()
  {
    if (input_init_from_arguments(argc, argv, &pr, &ba, &th, &pt, &tr,
                                  &pm, &sp, &nl, &le, &op,
                                  errmsg)==EXIT_FAILURE)
      {
        printf("\n\nError running input_init_from_arguments \n=>%s\n", errmsg);
        return EXIT_FAILURE;
      }
    if (background_init(&pr, &ba) == EXIT_FAILURE)
      {
        printf("\n\nError running background_init \n=>%s\n", ba.error_message);
        return EXIT_FAILURE;
      }
    if (thermodynamics_init(&pr, &ba, &th) == EXIT_FAILURE)
      {
        printf("\n\nError in thermodynamics_init \n=>%s\n", th.error_message);
        return EXIT_FAILURE;
      }
    if (perturb_init(&pr, &ba, &th, &pt) == EXIT_FAILURE)
      {
        printf("\n\nError in perturb_init \n=>%s\n", pt.error_message);
        return EXIT_FAILURE;
      }
    if (primordial_init(&pr, &pt, &pm) == EXIT_FAILURE)
      {
        printf("\n\nError in primordial_init \n=>%s\n", pm.error_message);
        return EXIT_FAILURE;
      }
    return EXIT_SUCCESS;
  }


  const int
  PerturbsCosmoclass::teardown()
  {
    if (primordial_free(&pm) == EXIT_FAILURE)
      {
        printf("\n\nError in primordial_free \n=>%s\n", pt.error_message);
        return EXIT_FAILURE;
      }

    if (perturb_free(&pt) == EXIT_FAILURE)
      {
        printf("\n\nError in perturb_free \n=>%s\n", pt.error_message);
        return EXIT_FAILURE;
      }

    if (thermodynamics_free(&th) == EXIT_FAILURE)
      {
        printf("\n\nError in thermodynamics_free \n=>%s\n", th.error_message);
        return EXIT_FAILURE;
      }

    if (background_free(&ba) == EXIT_FAILURE)
      {
        printf("\n\nError in background_free \n=>%s\n", ba.error_message);
        return EXIT_FAILURE;
      }

    return EXIT_SUCCESS;
  }

} // namespace cosmox
