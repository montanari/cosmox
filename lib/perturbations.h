// perturbations.h -- compute perturbations and source functions.
//
// Source functions are linear combinations of cosmological
// perturbations, dependent on wavelength and redshift. Each class of
// this module corresponds to a method to compute source
// functions. This may require an interface to an external Boltzmann
// solver code. The class Perturbations retrieves the correct
// computation method and it is the interface meant to be imported in
// other modules.
//
// Copyright 2017, 2018, 2019 Francesco Montanari
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/** @file perturbations.h
 *
 *  Perturbations.
 */

#ifndef PERTURBATIONS_H
#define PERTURBATIONS_H

#include <memory>
#include <string>
#include <unistd.h>
#include <unordered_map>
#include <utility>

namespace cosmox {

  /** @brief Flags to specify a source function computation method. */
  enum PerturbMethod {
    kCosmoclass                /**< CLASS Boltzmann solver. */
    // List here new flags.
  };

  /**
   * @brief Interface for the computation of cosmological
   * perturbations.
   *
   * This abstract class provides a factory method returning concrete
   * classes depending on the perturbation computation method.
   *
   * To use existing computation methods, create a smart pointer with
   * Perturbations::create_perturbs specifying which
   * Perturbations::PerturbMethod to use. All other methods will be
   * accessible through the class pointer so created.
   *
   * To implement a new computation method, define a new class
   * inheriting from this base class. Add the respective flag under
   * the Perturbations::PerturbMethod and modify the factory method
   * Perturbations::create_perturbs to add the new option. A new
   * computation method could be an interface to a cosmological
   * Boltzmann solver code (e.g., CLASS, CAMB, etc.) or an analytical
   * form (e.g., BBKS).
   */
  class Perturbations {
  public:
    /** Flags to specify a source function in
     *  Perturbations::get_source.
     */
    enum SourceType {
      kDeltaM,                  /**< Relative density perturbation of
                                 *   all non-relativistic species. */
      kThetaM,                  /**< Velocity divergence theta of all
                                 *   non-relativistic species. */
      kPhiPlusPsi,              /**< Sum of metric perturbations
                                 *   \f$\Phi+\Psi\f$. */
    };

    /** The factory method.  */
    static std::shared_ptr<Perturbations>
      create_perturbs(const PerturbMethod method, const std::string inifile);
    virtual ~Perturbations() {}

    /** @brief Return the source function for a given type.
     *
     *  We define source functions as linear combinations of
     *  cosmological perturbations rescaled to the initial value of
     *  the curvature perturbation (see equation A.1 of <a
     *  href="https://arxiv.org/abs/1307.1459">arXiv:1307.1459</a>). This
     *  is the same convention adopted in the CLASS code.
     *
     *  @param[in] stype      Source type (Perturbations::SourceType).
     *  @param[in] wavenumber Wavenumber in 1/Mpc.
     *  @param[in] redshift   Redshift.
     *  @return               Source function.
     */
    virtual const double
      get_source(const SourceType stype, const double wavenumber,
                 const double redshift) = 0;

    /** @brief Return the primordial power spectrum.
     *
     *  The quantity computed is the dimensionless primordial power
     *  spectrum of curvature perturbations,
     *  \f$\mathcal{P}_{\mathcal{R}}(k) = A_s (k/k_*)^{n_s-1}\f$.
     *
     *  For more information, see eq. (A.2) of <a
     *  href="https://arxiv.org/abs/1307.1459">arXiv:1307.1459</a>. More
     *  details about non-flat models are given in <a
     *  href="https://arxiv.org/abs/1603.09073">arXiv:1603.09073</a>.
     *
     *  @param[in] wavenumber Wavenumber.
     *  @return               Dimensionless primordial power spectrum.
     */
    virtual const double
      get_primordial_spectrum(const double wavenumber) = 0;

    /** @brief Return the range of valid wave-numbers.
     *
     *  Get a pair (k_min, k_max) containing the minimum and maximum
     *  wave-numbers where perturbations are defined.
     *
     *  Units: 1/Mpc.
     */
    virtual std::pair<double, double> get_k_lims() = 0;

    /** @brief Background comoving age of the Universe [Mpc]. */
    virtual double bg_comoving_age() = 0;

    /** @brief Background comoving line-of-sight distance at given
     *  redshift [Mpc]. */
    virtual double bg_comoving_distance(const double redshift) = 0;

    /** @brief Background Hubble parameter at given redshift [1/Mpc]. */
    virtual double bg_Hubble(const double redshift) = 0;

    /** @brief Scale independent velocity growth factor.
     *
     *  Logarithmic derivative of density growth factor for CDM
     *  perturbations.
     *
     *  @note Depending on the cosmological model and redshift, this
     *        function may be an inconsistent approximation since
     *        Boltzmann codes includes, e.g., radiation and neutrinos
     *        that give scale dependence to the growth factor.
     */
    virtual double bg_velocity_growth_factor(const double redshift) = 0;

    /** @brief Background redshift at given conformal time (the latter
     * in [Mpc]). */
    virtual double bg_z_of_tau(const double tau) = 0;
  };



} /* namespace cosmox */

#endif  /* PERTURBATIONS_H */
