// bispectrum_density.cc -- Density bispectrum.
//
// Copyright 2017, 2018 Francesco Montanari
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "bispectrum_density.h"

namespace cosmox {

  void
  check_multipoles(const int l1, const int l2, const int l3)
  {
    if (l1<3 || l2<3 || l3<3)
      throw CosmoxException("Multipoles must be l>=3", __FILE__, __LINE__);
  }

  bool
  is_gaunt_zero(const int l1, const int l2, const int l3)
  {
    auto tri = [](const int i, const int j, const int k)
               {return (fabs(j-k) <= i) && (i <= j+k);};
    auto on_triangle = (tri(l1, l2, l3) && tri(l1, l2, l3)
                        && tri(l1, l2, l3));
    auto sum_even = ((l1+l2+l3)%2)==0;
    return !(on_triangle && sum_even);
  }

  bool
  zclose(const double z1, const double z2)
  {
    auto atol = 1e-8;
    auto absclose = (z1<atol) && (z2<atol); // z1=z2=0
    if (absclose)
        return absclose;
    auto reltol = 1e-5;
    auto relclose = (std::fabs(1-z1/z2) < reltol);
    return relclose;
  }

  BispectrumDensity::BispectrumDensity(const std::shared_ptr<GeneralSpectra> gsp,
                                       const std::shared_ptr<Wigner> wig,
                                       const bool linear_rsd,
                                       const double epsrel,
                                       const double epsabs,
                                       GeneralSpectra::IntegType integtype):
    wig(wig), epsrel(epsrel), epsabs(epsabs), gsp(gsp),
    linear_rsd(linear_rsd), integtype(integtype)
  {
  }

  double
  BispectrumDensity::rdc_monopole_oneperm(const int l1, const int l2,
                                          const double z1, const double z2,
                                          const double z3)
  {
    auto ttype = gsp->kDensity;
    auto ttype_G = linear_rsd ? gsp->kDensityG_kVelocity1 : gsp->kDensity;
    auto n = 0;
    auto res = (gsp->compute(l1, l1, z1, z3, n, ttype_G, ttype,
                            epsrel, epsabs, integtype)
                * gsp->compute(l2, l2, z2, z3, n, ttype_G, ttype,
                              epsrel, epsabs, integtype));
    auto galbias = gsp->get_galaxy_bias();
    res *= (34. / 21. * galbias->b1(z3) + galbias->b2(z3));
    if (!linear_rsd)
      res *= galbias->b1(z1)*galbias->b1(z2); // gsp.kDensity has no bias.
    if (win1 && win2 && win3)
      res *= (win1->eval(z1) * win2->eval(z2) * win3->eval(z3));
    return res.real();
  }

  double
  BispectrumDensity::rdc_dipole_oneperm(const int l1, const int l2,
                                        const int l3, const double z1,
                                        const double z2, const double z3)
  {
    auto ttype = gsp->kDensity;
    auto ttype_G = linear_rsd ? gsp->kDensityG_kVelocity1 : gsp->kDensity;
    auto gaunt = geometry_reduced_Gaunt(l1, l2, l3, wig);
    if (gaunt==0)               // Total bispectrum not defined.
      return 0.0;
    auto lp_grid = {l2-1, l2+1};
    auto lpp_grid = {l1-1, l1+1};
    std::complex<double> res=0;
    for (auto lp : lp_grid)
      for (auto lpp : lpp_grid)
        res += ((2*lp+1) * (2*lpp+1) * geometry_Q(l1, l2, l3, 1, lp, lpp, wig)
                * (gsp->compute(lpp, l1, z3, z1, 1, ttype, ttype_G,
                                epsrel, epsabs, integtype)
                   * gsp->compute(lp, l2, z3, z2, -1, ttype, ttype_G,
                                  epsrel, epsabs, integtype)
                   + gsp->compute(lpp, l1, z3, z1, -1, ttype, ttype_G,
                                  epsrel, epsabs, integtype)
                   * gsp->compute(lp, l2, z3, z2, 1, ttype, ttype_G,
                                  epsrel, epsabs, integtype)));
    auto galbias = gsp->get_galaxy_bias();
    res *= galbias->b1(z3) / (16.*pow(M_PI, 2) * gaunt);
    if (!linear_rsd)
      res *= galbias->b1(z1)*galbias->b1(z2); // gsp->kDensity has no bias.
    return res.real();
  }

  double
  BispectrumDensity::rdc_quadrupole_oneperm(const int l1, const int l2,
                                            const int l3, const double z1,
                                            const double z2, const double z3)
  {
    auto ttype = gsp->kDensity;
    auto ttype_G = linear_rsd ? gsp->kDensityG_kVelocity1 : gsp->kDensity;
    auto gaunt = geometry_reduced_Gaunt(l1, l2, l3, wig);
    if (gaunt==0)               // Total bispectrum not defined.
      return 0.0;
    auto lp_grid = {l2-2, l2, l2+2};
    auto lpp_grid = {l1-2, l1, l1+2};
    auto n = 0;
    std::complex<double> res=0;
    for (auto lp : lp_grid)
      for (auto lpp : lpp_grid)
        res += ((2*lp+1) * (2*lpp+1) * geometry_Q(l1, l2, l3, 2, lp, lpp, wig)
                * gsp->compute(lpp, l1, z3, z1, n, ttype, ttype_G,
                               epsrel, epsabs, integtype)
                * gsp->compute(lp, l2, z3, z2, n, ttype, ttype_G,
                               epsrel, epsabs, integtype));
    auto galbias = gsp->get_galaxy_bias();
    res *= (galbias->b1(z3) + 7./2.*galbias->bS(z3)) / (42.*pow(M_PI, 2) * gaunt);
    if (!linear_rsd)
      res *= galbias->b1(z1)*galbias->b1(z2); // gsp->kDensity has no bias.
    return res.real();
  }

  double
  BispectrumDensity::compute_rdc_oneperm(const int l1, const int l2,
                                         const int l3, const double z1,
                                         const double z2, const double z3)
  {
    auto mono = rdc_monopole_oneperm(l1, l2, z1, z2, z3);
    auto dip = rdc_dipole_oneperm(l1, l2, l3, z1, z2, z3);
    auto quad = rdc_quadrupole_oneperm(l1, l2, l3, z1, z2, z3);
    return mono + dip + quad;
  }

  double
  BispectrumDensity::compute_reduced(const int l1, const int l2,
                                     const int l3, const double z1,
                                     const double z2, const double z3)
  {
    check_multipoles(l1, l2, l3);
    if ((l1==l2) && (l2==l3) && (z1==z2) && (z2==z3))
      {
        return 3.*compute_rdc_oneperm(l1, l2, l3, z1, z2, z3);
      }
    else
      {
        return (compute_rdc_oneperm(l1, l2, l3, z1, z2, z3)
                + compute_rdc_oneperm(l2, l3, l1, z2, z3, z1)
                + compute_rdc_oneperm(l3, l1, l2, z3, z1, z2));
      }
  }

  double
  BispectrumDensity::compute(const int l1, const int l2, const int l3,
                             const double z1, const double z2, const double z3)
  {
    if (is_gaunt_zero(l1, l2, l3))
      return 0.;
    auto gaunt = geometry_reduced_Gaunt(l1, l2, l3, wig);
    auto bisp = compute_reduced(l1, l2, l3, z1, z2, z3);
    return gaunt * bisp;
  }

  double
  BispectrumDensity::compute_variance(const int l1, const int l2, const int l3,
                                      const double z1, const double z2,
                                      const double z3, const double shot_noise)
  {
    check_multipoles(l1, l2, l3);
    if (is_gaunt_zero(l1, l2, l3))
      return 0.;
    auto ttype = linear_rsd ? gsp->kDensityG_kVelocity1 : gsp->kDensity;
    auto galbias = gsp->get_galaxy_bias();
    auto spectrum = [&](int l, double z1, double z2) {
      // linear_rsd already includes galaxy bias.
      auto biases = linear_rsd ? 1.0 : galbias->b1 (z1) * galbias->b1 (z2);
      return (biases
                  * gsp->compute (l, l, z1, z2, 0, ttype, ttype, epsrel,
                                  epsabs, integtype)
                        .real ()
              + shot_noise);
    };
    double res;
    if (zclose(z1, z2) && zclose(z1, z3) && zclose(z2, z3))
      {
        auto kronecker = [](int i, int j) {return i==j? 1.0 : 0.0;};
        auto multiplicity = (1.0 + 2.0*kronecker(l1, l2)*kronecker(l2, l3) +
                             kronecker(l1, l2) + kronecker(l2, l3) +
                             kronecker(l3, l1));
        res = (spectrum(l1, z1, z1) * spectrum(l2, z1, z1)
               * spectrum(l3, z1, z1) * multiplicity);
      }
    else
      {
        res = (spectrum(l1, z1, z1) * spectrum(l2, z2, z2)
               * spectrum(l3, z3, z3));
        // There is computational redundancy for spectrum() that could
        // be avoided if needed for performance.
        auto fact = [&](int la, int lb, double za, double zb) {
          return (std::pow (spectrum (la, za, za), 2.0)
                  * spectrum (lb, zb, zb));
        };
        if ((zclose (z1, z2)) and (l1 == l2))
          res += fact(l1, l3, z1, z3);
        if (zclose(z1, z3) and (l1==l3))
          res += fact(l1, l2, z1, z2);
        if (zclose(z2, z3) and (l2==l3))
          res += fact(l2, l1, z2, z1);
      }
    if (win1 && win2 && win3)
      res *= (win1->eval(z1) * win2->eval(z2) * win3->eval(z3));
    return res;
  }

  BispectrumDensityW::BispectrumDensityW(const std::shared_ptr<GeneralSpectra> gsp,
                                         const std::shared_ptr<Wigner> wig,
                                         const bool linear_rsd,
                                         const double epsrel,
                                         const double epsabs):
    BispectrumDensity(gsp, wig, linear_rsd, epsrel, epsabs,
                      GeneralSpectra::kCquad)
  {
  }

  std::vector<double>
  BispectrumDensityW::compute_rdc_integrand(std::vector<double> zz,
                                            void* params)
  {
    double z1 = zz.at(0);
    double z2 = zz.at(1);
    double z3 = zz.at(2);
    Params* pars = static_cast<Params*>(params);
    auto res = (pars->bsd->compute_rdc_oneperm(pars->l1, pars->l2, pars->l3,
                                               z1, z2, z3)
                + pars->bsd->compute_rdc_oneperm(pars->l2, pars->l3, pars->l1,
                                                 z2, z3, z1)
                + pars->bsd->compute_rdc_oneperm(pars->l3, pars->l1, pars->l2,
                                                 z3, z1, z2));
    std::vector<double> func(1);
    func[0] = res;
    return func;
  }

  double
  BispectrumDensityW::compute_reduced(const int l1, const int l2, const int l3,
                                      const std::shared_ptr<Window> w1,
                                      const std::shared_ptr<Window> w2,
                                      const std::shared_ptr<Window> w3)
  {
    check_multipoles(l1, l2, l3);
    Params params;
    params.bsd = this;
    params.l1 = l1;
    params.l2 = l2;
    params.l3 = l3;
    win1 = w1;
    win2 = w2;
    win3 = w3;
    auto bounds = {std::make_pair(win1->get_zmin(), win1->get_zmax()),
                   std::make_pair(win2->get_zmin(), win2->get_zmax()),
                   std::make_pair(win3->get_zmin(), win3->get_zmax())};

    cuba::Cuba integrator;
    integrator.epsrel = epsrel;
    integrator.epsabs = epsabs;
    integrator.maxeval = maxeval;
    auto result = integrator.suave(&compute_rdc_integrand, bounds, &params);
    if (result.fail)
      throw CosmoxException("Cuba failed", __FILE__, __LINE__);
    return result.integral.at(0);
  }

  double
  BispectrumDensityW::compute(const int l1, const int l2, const int l3,
                              const std::shared_ptr<Window> w1,
                              const std::shared_ptr<Window> w2,
                              const std::shared_ptr<Window> w3)
  {
    if (is_gaunt_zero(l1, l2, l3))
      return 0.;
    auto gaunt = geometry_reduced_Gaunt(l1, l2, l3, wig);
    auto bisp = compute_reduced(l1, l2, l3, w1, w2, w3);
    return gaunt * bisp;
  }

  std::vector<double>
  BispectrumDensityW::compute_variance_integrand (std::vector<double> zz,
                                                  void *params)
  {
    double z1 = zz.at (0);
    double z2 = zz.at (1);
    double z3 = zz.at (2);
    Params *pars = static_cast<Params *> (params);

    double res;
    check_multipoles (pars->l1, pars->l2, pars->l3);
    if (is_gaunt_zero (pars->l1, pars->l2, pars->l3))
      {
        res = 0.;
      }
    else
      {
        auto ttype = pars->bsd->linear_rsd
                         ? pars->bsd->gsp->kDensityG_kVelocity1
                         : pars->bsd->gsp->kDensity;
        auto galbias = pars->bsd->gsp->get_galaxy_bias ();
        auto spectrum = [&](int l, double z1, double z2) {
          // linear_rsd already includes galaxy bias.
          auto biases = pars->bsd->linear_rsd
                            ? 1.0
                            : galbias->b1 (z1) * galbias->b1 (z2);
          return (biases
                      * pars->bsd->gsp
                            ->compute (l, l, z1, z2, 0, ttype, ttype,
                                       pars->bsd->epsrel, pars->bsd->epsabs,
                                       pars->bsd->integtype)
                            .real ()
                  + pars->shot_noise);
        };
        auto c11_l1 = spectrum (pars->l1, z1, z1);
        auto c22_l2 = spectrum (pars->l2, z2, z2);
        auto c33_l3 = spectrum (pars->l3, z3, z3);
        res = c11_l1 * c22_l2 * c33_l3;
        // This overestimates the result for non-equal redshifts.
        if ((pars->l1 == pars->l2) and (pars->l2 == pars->l3))
          {
            res *= 6.0;
          }
        else if ((pars->l1 == pars->l2) or (pars->l1 == pars->l3)
                 or (pars->l2 == pars->l3))
          {
            res *= 2.0;
          }
      }
    if (pars->bsd->win1 and pars->bsd->win2 and pars->bsd->win3)
      res *= (pars->bsd->win1->eval (z1) * pars->bsd->win2->eval (z2)
              * pars->bsd->win3->eval (z3));

    std::vector<double> func (1);
    func[0] = res;
    return func;
  }

  double
  BispectrumDensityW::compute_variance(const int l1, const int l2, const int l3,
                                       const std::shared_ptr<Window> w1,
                                       const std::shared_ptr<Window> w2,
                                       const std::shared_ptr<Window> w3,
                                       const double shot_noise)
  {
    check_multipoles(l1, l2, l3);
    Params params;
    params.bsd = this;
    params.l1 = l1;
    params.l2 = l2;
    params.l3 = l3;
    params.shot_noise = shot_noise;
    win1 = w1;
    win2 = w2;
    win3 = w3;
    auto bounds = {std::make_pair(win1->get_zmin(), win1->get_zmax()),
                   std::make_pair(win2->get_zmin(), win2->get_zmax()),
                   std::make_pair(win3->get_zmin(), win3->get_zmax())};

    cuba::Cuba integrator;
    integrator.epsrel = epsrel;
    integrator.epsabs = epsabs;
    integrator.maxeval = maxeval;
    auto result = integrator.suave(&compute_variance_integrand, bounds,
                                   &params);
    if (result.fail)
      throw CosmoxException("Cuba failed", __FILE__, __LINE__);
    return result.integral.at(0);
  }


} /* namespace cosmox */
