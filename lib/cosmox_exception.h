// cosmox_exception.h -- Exceptions handling

// Copyright 2017, 2018, 2019 Francesco Montanari

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef COSMOX_EXCEPTION_H
#define COSMOX_EXCEPTION_H

#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>

namespace cosmox {

  /**
   * @brief Cosmox exception handling class.
   *
   */
  class CosmoxException : public std::runtime_error {
  public:
    /** Use this constructor to throw an exception as usual. */
    CosmoxException(const std::string &msg);

    /** Use this constructor to display file and line number where the
     * exception is thrown. The message is displayed even without
     * catching it. Typical use:
     *
     *  \code{.cpp}
     *  throw CosmoxException("an error occurred", __FILE__, __LINE__);
     *  \endcode
     */
    CosmoxException(const std::string &msg, const char *file, int line);

    ~CosmoxException();

    /** @brief Error message.  */
    const char *what() const throw();

  private:
    std::string omsg;
  };

} /* namespace cosmox */

#endif  /* COSMOX_EXCEPTION_H */
