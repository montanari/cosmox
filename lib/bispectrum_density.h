// bispectrum_density.h -- Density bispectrum.
//
// Copyright 2017, 2018 Francesco Montanari
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef BISPECTRUM_DENSITY_H
#define BISPECTRUM_DENSITY_H

#include <memory>
#include <mutex>
#include <tuple>
#include <vector>

#include "cosmox_exception.h"
#include "cubapp.h"
#include "general_spectra.h"
#include "numeric.h"
#include "special_functions.h"
#include "survey_specs.h"


namespace cosmox {
  typedef double (*PermutableFunc)(const int l1, const int l2, const int l3,
                                   const double z1, const double z2, const double z3);

  /** @brief check multipoles range.*/
  void check_multipoles(const int l1, const int l2, const int l3);

  /** @brief Is Gaunt factor zero.
   *
   * Gaunt factor is zero unless l1, l2, l3 form a triangle and
   * l1+l2+l3 = even.
   */
  bool is_gaunt_zero(const int l1, const int l2, const int l3);

  /** @brief Check if redshifts are approximately equal*/
  bool zclose(const double z1, const double z2);

  /** @brief Density bispectrum.
   *
   * Definitions follow <a
   * href="https://arxiv.org/abs/1812.09297">arxiv:1812.09297</a>.
   */
  class BispectrumDensity {
  public:
    /**
     * @brief Density tree-level reduced bispectrum.
     *
     * @param[in] gsp        generalized angular power spectra.
     * @param[in] wig        Wigner symbols.
     * @param[in] linear_rsd include linear redshift-space distortions?
     * @param[in] epsrel     relative precision for the integration.
     * @param[in] epsabs     absolute precision for the integration.
     * @param[in] IntegType  integration strategy.
     */
    // Wigner could be defined internally. However, remember that its
    // constructor is not thread-safe. Since we want to allow member
    // functions to be called in parallel, ask user to define Wigner
    // outside the multi-threaded region.
    BispectrumDensity(const std::shared_ptr<GeneralSpectra> gsp,
                      const std::shared_ptr<Wigner> wig,
                      const bool linear_rsd = false,
                      const double epsrel = 1e-4,
                      const double epsabs = 0.,
                      GeneralSpectra::IntegType integtype=GeneralSpectra::kMix);

    /** @brief Compute density tree-level bispectrum. */
    double compute(const int l1, const int l2, const int l3,
                   const double z1, const double z2, const double z3);

    /** @brief Compute reduced density tree-level bispectrum. */
    double compute_reduced(const int l1, const int l2, const int l3,
                           const double z1, const double z2, const double z3);

    /** @brief Compute tree-level bispectrum variance.
     *
     * Include cosmic variance and shot-noise.
     *
     * @note Approximate variance at different redshifts neglecting
     *       terms proportional to \f$C_{\ell}(z_i, z_j)\f$ for \f$i
     *       \neq j\f$. They are expected to be subleading compared to
     *       those \f$i=j\f$ (unless integrated effects such as
     *       lensing are included, not the case here). See <a
     *       href="https://arxiv.org/abs/1812.09297">arxiv:1812.09297</a>
     *       for the full equation.
     */
    double compute_variance(const int l1, const int l2, const int l3,
                            const double z1, const double z2, const double z3,
                            const double shot_noise = 0);

  protected:
    std::shared_ptr<GeneralSpectra> gsp;
    GeneralSpectra::IntegType integtype;
    std::shared_ptr<Wigner> wig;
    double epsrel;
    double epsabs;
    bool linear_rsd;

    // For use in child classes only.
    std::shared_ptr<Window> win1=nullptr;
    std::shared_ptr<Window> win2=nullptr;
    std::shared_ptr<Window> win3=nullptr;

    double compute_rdc_oneperm(const int l1, const int l2, const int l3,
                               const double z1, const double z2, const double z3);

  private:

    double rdc_monopole_oneperm(const int l1, const int l2,
                                const double z1, const double z2,
                                const double z3);
    double rdc_dipole_oneperm(const int l1, const int l2, const int l3,
                              const double z1, const double z2, const double z3);
    double rdc_quadrupole_oneperm(const int l1, const int l2, const int l3,
                                  const double z1, const double z2, const double z3);
  };


  /** @brief Density bispectrum with window functions.
   *
   * @note Methods are already parallelized internally.
   */
  class BispectrumDensityW: public BispectrumDensity {
  public:
    // If needed we could use two different `epsrel` (and `epsabs`)
    // for GeneralSpectra and Cuba integrations.
    BispectrumDensityW(const std::shared_ptr<GeneralSpectra> gsp,
                       const std::shared_ptr<Wigner> wig,
                       const bool linear_rsd = false,
                       const double epsrel = 1e-2,
                       const double epsabs = 0.);

    double compute(const int l1, const int l2, const int l3,
                           const std::shared_ptr<Window> w1,
                           const std::shared_ptr<Window> w2,
                           const std::shared_ptr<Window> w3);
    double compute_reduced(const int l1, const int l2, const int l3,
                           const std::shared_ptr<Window> w1,
                           const std::shared_ptr<Window> w2,
                           const std::shared_ptr<Window> w3);
    /** @brief Compute tree-level bispectrum variance.
     *
     * Include cosmic variance and shot-noise.
     *
     * @warning The current implementation approximates
     *          \f$\sigma^{ijk}_{\ell_1\ell_2\ell_3} \approx
     *          C^{ii}_{\ell_1}C^{jj}_{\ell_2}C^{kk}_{\ell_3} (1 +
     *          \delta_{12} + \delta_{13} + \delta_{23} + 2
     *          \delta_{12} \delta_{23}) \f$. This is only good for
     *          small window functions (\Delta z \approx 10^{-4}),
     *          converging to BispectrumDensity.compute_variance.
     *          However, in general it is not reliable if two or more
     *          multipoles are equal (it overestimates the variance).
     *          (At the moment integration over the non-approximate
     *          variance is only available in an experimental branch.)
     */
    double compute_variance(const int l1, const int l2, const int l3,
                            const std::shared_ptr<Window> w1,
                            const std::shared_ptr<Window> w2,
                            const std::shared_ptr<Window> w3,
                            const double shot_noise = 0);

  private:
    // Unhide base methods only privately, otherwise results without
    // window function mey be inconsistent (they require
    // `win*=nullptr`, but in this class they are not `nullptr`.)
    using BispectrumDensity::compute_variance;

    int maxeval = 100000;
    static std::vector<double> compute_rdc_integrand(std::vector<double> zz,
                                                     void* params);
    static std::vector<double> compute_variance_integrand(std::vector<double> zz,
                                                          void* params);
    struct Params {
      BispectrumDensityW* bsd;
      int l1;
      int l2;
      int l3;
      double shot_noise;
    };
  };
} /* namespace cosmox */

#endif  /* BISPECTRUM_DENSITY_H */
