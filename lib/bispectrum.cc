// bispetrcum.cc -- Bispectrum prototype.
//
// Copyright 2017, 2018, 2019 Francesco Montanari
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "bispectrum.h"

#include "cosmox_exception.h"

namespace cosmox {
  BispectrumRsd::BispectrumRsd(const std::shared_ptr<Perturbations> perturbs,
                               const std::shared_ptr<GalaxyBias> galbias,
                               const bool linear_rsd,
                               const double epsrel,
                               const double epsabs,
                               const int maxeval,
                               const bool ignore_fail)
  {
    this->perturbs = perturbs;
    std::tie (this->kmin, this->kmax) = this->perturbs->get_k_lims ();
    this->galbias = galbias;
    this->linear_rsd = linear_rsd;
    this->epsrel = epsrel;
    this->epsabs = epsabs;
    this->maxeval = maxeval;
    this->ignore_fail = ignore_fail;
  }

  long double
  BispectrumRsd::integrand_kernel(const double l, const double r,
                                  const double chi)
  {
    long double kernel;
    if (chi==r)
      {
        kernel = (l*(l+1.) + 1.) / chi;
      }
    else if (chi>r)
      {
        kernel = l * (l-1.) * pow(r/chi, l) * chi / pow(r, 2.);
      }
    else
      {
        kernel = (l+1.) * (l+2.) * pow(chi/r, l)
          * pow(chi, 2.) / pow(r, 3.);

      }
    return kernel;
  }

  long double
  BispectrumRsd::integrand_linear_transfer(const double k, const double l,
                                           const double z, void* params)
  {
    Params* pars = static_cast<Params*>(params);
    long double kr = k * pars->pt->bg_comoving_distance (z);
    long double linear_transfer = pars->galbias->b1 (z)
      * pars->pt->get_source (Perturbations::kDeltaM, k, z)
      * sf_bessel_j (l, kr);
    if (pars->linear_rsd)
        linear_transfer += (1.+z) / pars->pt->bg_Hubble (z)
          * pars->pt->get_source (Perturbations::kThetaM, k, z)
          * sf_dd_bessel_j (l, kr);
    return linear_transfer;
  }

  std::vector<double>
  BispectrumRsd::integrand(std::vector<double> vars,
                           void* params)
  {
    double k2 = vars.at(0);
    double k3 = vars.at(1);
    double chi = vars.at(2);
    Params* pars = static_cast<Params*>(params);
    long double k2sq = k2*k2;
    long double k3sq = k3*k3;
    long double G0 = -3./28. * pow(k2sq-k3sq, 2.) / k2sq / k3sq;
    auto kernel = integrand_kernel(pars->l1,
                                   pars->pt->bg_comoving_distance (pars->z1),
                                   chi);
    auto linear_transfers_z2 = integrand_linear_transfer(k2, pars->l2,
                                                         pars->z2, pars);
    auto linear_transfers_z3 = integrand_linear_transfer(k3, pars->l3,
                                                         pars->z3, pars);
    long double res = 2. * pow(4*M_PI, 2.) / (2.*pars->l1+1.)
      * (1.+pars->z1) / pars->pt->bg_Hubble (pars->z1)
      * pars->pt->get_primordial_spectrum (k2) / k2
      * pars->pt->get_primordial_spectrum (k3) / k3
      * pars->pt->get_source (Perturbations::kThetaM, k2, pars->z1)
      * pars->pt->get_source (Perturbations::kDeltaM, k3, pars->z1)
      * sf_bessel_j(pars->l2, k2 * chi)
      * sf_bessel_j(pars->l3, k3 * chi)
      * linear_transfers_z2 * linear_transfers_z3
      * kernel * G0;
    std::vector<double> func(1);
    func[0] = res;
    return func;
  }


  double
  BispectrumRsd::integrate(const cuba::CubaIntegrandFunc integrand,
                           const int l1, const int l2, const int l3,
                           const double z1, const double z2, const double z3,
                           const double peak_width_scale)
  {
    if (peak_width_scale < 1)
      throw CosmoxException("peak_width_scale must be > 1",
                            __FILE__, __LINE__);
    Params params;
    params.pt = perturbs;
    params.galbias = galbias;
    params.linear_rsd = linear_rsd;
    params.l1 = l1;
    params.l2 = l2;
    params.l3 = l3;
    params.z1 = z1;
    params.z2 = z2;
    params.z3 = z3;

    auto r1 = perturbs->bg_comoving_distance(z1);
    auto bounds = {std::make_pair(kmin, kmax),
                   std::make_pair(kmin, kmax),
                   std::make_pair(r1/peak_width_scale, r1*peak_width_scale)};
    cuba::Cuba integrator;
    integrator.epsrel = epsrel;
    integrator.epsabs = epsabs;
    integrator.maxeval = maxeval;
    integrator.flags = 0b100;
    auto result = integrator.suave(integrand, bounds, &params);

    if ((!ignore_fail) and (result.fail))
      throw CosmoxException("Cuba failed", __FILE__, __LINE__);
    return result.integral[0];
  }

  double
  BispectrumRsd::get_nonseparable(const int l1, const int l2, const int l3,
                                  const double z1, const double z2,
                                  const double z3,
                                  const double peak_width_scale)
  {
    if (l1==l2 and l1==l3 and z1==z2 and z1==z3)
      return 3. * integrate(&integrand, l1, l2, l3, z1, z2, z3,
                            peak_width_scale);
    return integrate(&integrand, l1, l2, l3, z1, z2, z3, peak_width_scale)
      + integrate(&integrand, l3, l1, l2, z3, z1, z2, peak_width_scale)
      + integrate(&integrand, l2, l3, l1, z2, z3, z1, peak_width_scale);
  }
} /* namespace cosmox */
