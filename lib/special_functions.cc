// special_functions.cc -- Special functions.

// Copyright 2017, 2018, 2019 Francesco Montanari

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <wigxjpf.h>

#include "cosmox_exception.h"
#include "special_functions.h"

namespace cosmox {

  // Adaptation of Python code from
  // <https://en.wikipedia.org/wiki/Lanczos_approximation>.
  std::complex<double>
  sf_gamma(std::complex<double> z)
  {
    std::vector<std::complex<double>> pcoeff = {676.5203681218851,
                                                -1259.1392167224028,
                                                771.32342877765313,
                                                -176.61502916214059,
                                                12.507343278686905,
                                                -0.13857109526572012,
                                                9.9843695780195716e-6,
                                                1.5056327351493116e-7};
    if (real(z) < 0.5)
        return M_PI / (sin(M_PI*z) * sf_gamma(1.-z));

    z -= 1.;
    std::complex<double> x (0.99999999999980993, 0.);
    auto psize = pcoeff.size();
    std::complex<double> y;
    for (size_t i=0; i<psize; ++i)
      {
        x += pcoeff[i] / (z + (double)i + 1.);
        auto t = z + (double)psize - 0.5;
        y = sqrt(2*M_PI) * pow(t, z+0.5) * exp(-t) * x;
      }
    return y;
  }


  double
  sf_bessel_j_less_7(const int l, const double x)
  {
    if ((l < 0) or (x<0))
      throw CosmoxException("Require positive arguments", __FILE__, __LINE__);
    if (l >=7)
      throw CosmoxException("Multipole must be l<7", __FILE__, __LINE__);

    double x2 = x*x;
    double sx = sin(x);
    double cx = cos(x);

    double jl;

    if(l == 0) {
      if (x > 0.1) jl=sx/x;
      else jl=1.-x2/6.*(1.-x2/20.);
      return jl;
    }
    if(l == 1) {
      if (x > 0.2) jl=(sx/x -cx)/x;
      else jl=x/3.*(1.-x2/10.*(1.-x2/28.));
      return jl;
    }
    if (l == 2) {
      if (x > 0.3) jl=(-3.*cx/x-sx*(1.-3./x2))/x;
      else jl=x2/15.*(1.-x2/14.*(1.-x2/36.));
      return jl;
    }
    if (l == 3) {
      if (x > 0.4) jl=(cx*(1.-15./x2)-sx*(6.-15./x2)/x)/x;
      else jl=x*x2/105.*(1.-x2/18.*(1.-x2/44.));
      return jl;
    }
    if (l == 4) {
      if (x > 0.6) jl=(sx*(1.-45./x2+105./x2/x2) +cx*(10.-105./x2)/x)/x;
      else jl=x2*x2/945.*(1.-x2/22.*(1.-x2/52.));
      return jl;
    }
    if (l == 5) {
      if (x > 1) jl=(sx*(15.-420./x2+945./x2/x2)/x
                     - cx*(1.0-105./x2+945./x2/x2)) / x;
      else jl=x2*x2*x/10395.*(1.-x2/26.*(1.-x2/60.));
      return jl;
    }
    if (l == 6) {
      if (x > 1) jl=(sx*(-1.+(210.-(4725.-10395./x2)/x2)/x2)+
                     cx*(-21.+(1260.-10395./x2)/x2)/x)/x;
      else jl=x2*x2*x2/135135.*(1.-x2/30.*(1.-x2/68.));
      return jl;
    }
    throw CosmoxException("This point should not have been reached",
                          __FILE__, __LINE__);
    return EXIT_FAILURE;
  }


  double
  sf_bessel_j_geq_7_smallx(const int l, const double x)
  {
    double nu = l+0.5;

    // // The following expression is the one used in CLASS v1. However,
    // // it misses an overall l-dependent factor and it can be wrong by
    // // several orders of magnitude.
    // double fl = (double)l;
    // return exp(fl*log(x/nu/2.)+nu*(1-log(2.)) -
    // (1.-(1.-3.5/nu2)/nu2/30.)/12./nu) / nu * (1.-x2/(4.*nu+4.) *
    // (1.-x2/(8.*nu+16.) * (1.-x2/(12.*nu+36.))));

    // // 9.3.1., Abramowitz and Stegun. Not accurate enough.
    // return pow(0.5*M_E*x / nu, nu) / sqrt(4.*nu*x);

    // 9.3.2., Abramowitz and Stegun. Roghly < 1% accuracy at
    // l=7. Since x<<l is far away from the Bessel peak, this seems
    // reasonable.
    auto sech_al = x/nu;
    auto sech2_al = sech_al*sech_al;
    auto tanh_al = sqrt(1.-sech2_al);
    auto al = log((1+sqrt(1.-sech2_al)) / sech_al);
    return exp(nu * (tanh_al - al)) / sqrt(4.*x*nu*tanh_al);
  }


  double
  sf_bessel_j_geq_7(const int l, const double x)
  {
    if (x<0)
      throw CosmoxException("Require positive arguments", __FILE__, __LINE__);
    if (l < 7)
      throw CosmoxException("Multipole must be l>=7", __FILE__, __LINE__);

    double jl;
    if (x <= 1.e-40) {
      jl=0.0;
      return jl;
    }

    double fl = (double)l;
    double nu=fl + 0.5;
    double nu2=nu*nu;
    double x2=x*x;

    double beta;
    double sx;
    double cotb;
    double cot3b;
    double cot6b;
    double secb;
    double sec2b;
    double expterm;
    double cosb;

    // The following limit is accurate only up to 1%.
    //
    // if ((x2/fl) < 0.5) {
    //   jl = sf_bessel_j_geq_7_smallx(l, x);
    //   return jl;
    // }

    if ((fl*fl/x) < 0.5) {
      beta = x - M_PI_2*(fl+1.);
      jl = (cos(beta) * (1. - (nu2-0.25) * (nu2-2.25)/8./ x2 *
                         (1.-(nu2-6.25) * (nu2-12.25)/48./x2)) - sin(beta) *
            (nu2-0.25)/2./x * (1. - (nu2-2.25) * (nu2-6.25)/24./x2
                               * (1. - (nu2-12.25) * (nu2-20.25)/80./x2))) / x;
      return jl;
    }

    double l3 = pow(nu,0.325);

    if (x < nu-1.31*l3) {
      cosb=nu/x;
      sx=sqrt(nu2-x2);
      cotb=nu/sx;
      secb=x/nu;
      beta=log(cosb+sx/x);
      cot3b=cotb*cotb*cotb;
      cot6b=cot3b*cot3b;
      sec2b=secb*secb;
      expterm=((2.+3.*sec2b) * cot3b/24.  - ((4.+sec2b) * sec2b *
                                             cot6b/16.  + ((16. - (1512. + (3654.+375.*sec2b) *
                                                                   sec2b) * sec2b) * cot3b/5760.  + (32. + (288. +
                                                                                                            (232.+13.*sec2b)*sec2b) * sec2b) * sec2b *
                                                           cot6b/128./nu) * cot6b/nu) / nu) / nu;
      jl=sqrt(cotb*cosb)/(2.*nu)*exp(-nu*beta+nu/cotb-expterm);

      return jl;
    }

    if (x > nu+1.48*l3) {
      cosb=nu/x;
      sx=sqrt(x2-nu2);
      cotb=nu/sx;
      secb=x/nu;
      beta=acos(cosb);
      cot3b=cotb*cotb*cotb;
      cot6b=cot3b*cot3b;
      sec2b=secb*secb;
      double trigarg=nu/cotb - nu*beta - M_PI_4 - ((2.+3.*sec2b) *
                                                   cot3b/24.  +(16. - (1512. + (3654.+375.*sec2b) *
                                                                       sec2b) * sec2b) * cot3b * cot6b/5760./nu2) / nu;
      expterm=((4.+sec2b) * sec2b * cot6b/16.  -(32. + (288. +
                                                        (232.+13.*sec2b) * sec2b) * sec2b) * sec2b * cot6b *
               cot6b/128. / nu2) / nu2;
      jl=sqrt(cotb*cosb)/nu*exp(-expterm)*cos(trigarg);

      return jl;
    }

    // Last possible case.
    const double kGamma1 = 2.6789385347077476336556;
    const double kGamma2 = 1.3541179394264004169452;

    beta=x-nu;
    double beta2=beta*beta;
    sx=6./x;
    double sx2=sx*sx;
    secb=pow(sx,1./3.);
    sec2b=secb*secb;
    jl=(kGamma1*secb + beta*kGamma2*sec2b -
        (beta2/18.-1./45.)*beta*sx*secb*kGamma1
        -((beta2-1.)*beta2/36.+1./420.)*sx*sec2b*kGamma2
        +(((beta2/1620. - 7./3240.)*beta2 + 1./648.) * beta2 -
          1./8100.) * sx2 * secb * kGamma1 +(((beta2/4536. - 1./810.) *
                                              beta2 + 19./11340.) * beta2 - 13./28350.) * beta * sx2 * sec2b
        * kGamma2 -((((beta2/349920. - 1./29160.) * beta2 +
                      71./583200.) * beta2 - 121./874800.) * beta2 +
                    7939./224532000.) * beta * sx2 * sx * secb * kGamma1) *
      sqrt(sx)/12./sqrt(M_PI);

    return jl;
  }


  double
  sf_bessel_j(const int l, const double x)
  {
    double jl = (l<7) ? sf_bessel_j_less_7(l, x) : sf_bessel_j_geq_7(l, x);
    return jl;
  }


  double
  sf_d_bessel_j(const int l, const double x)
  {
    if (l==0)
      return cos(x)/x - sin(x)/pow(x, 2);
    else
      {
        return sf_bessel_j(l - 1., x) - (l + 1.)*sf_bessel_j(l, x)/x;
      }
  }


  double
  sf_dd_bessel_j(const int l, const double x)
  {
    if (l==0)
      return (-pow(x, 2)*sin(x) - 2*x*cos(x) + 2*sin(x))/pow(x, 3);
    else if (l==1)
      {
        return (pow(x, 3)*cos(x) - 3*pow(x, 2)*sin(x) - 6*x*cos(x) + 6*sin(x))
          /pow(x, 4);
      }
    else
      {
        return (-l*x*sf_bessel_j(l - 1, x) + pow(x, 2)*sf_bessel_j(l - 2, x)
                + (l + 1)*(-x*sf_bessel_j(l - 1, x) + (l + 1)*sf_bessel_j(l, x) + sf_bessel_j(l, x)))
          /pow(x, 2);
      }
  }


  double
  sf_ddd_bessel_j(const int l, const double x)
  {
    if (l==0)
      return (-pow(x, 3)*cos(x) + 3*pow(x, 2)*sin(x) + 6*x*cos(x) - 6*sin(x))
        /pow(x, 4);
    else if (l==1)
      {
        return (-pow(x, 4)*sin(x) - 4*pow(x, 3)*cos(x) + 12*pow(x, 2)*sin(x)
                + 24*x*cos(x) - 24*sin(x))/pow(x, 5);
      }
    else if (l==2)
      {
        return -180.0*pow(x, -6.0)*sin(x) + 180.0*pow(x, -5.0)*cos(x)
          + 87.0*pow(x, -4.0)*sin(x) - 27.0*pow(x, -3.0)*cos(x)
          - 6.0*pow(x, -2.0)*sin(x) + 1.0*1.0/x*cos(x);
      }
    else
      {
        return (-pow(l, 3)*sf_bessel_j(l, x)
                + 3*pow(l, 2)*x*sf_bessel_j(l - 1, x)
                - 6*pow(l, 2)*sf_bessel_j(l, x)
                - 3*l*pow(x, 2)*sf_bessel_j(l - 2, x)
                + 6*l*x*sf_bessel_j(l - 1, x)
                - 11*l*sf_bessel_j(l, x) + pow(x, 3)*sf_bessel_j(l - 3, x)
                + 3*x*sf_bessel_j(l - 1, x) - 6*sf_bessel_j(l, x))/pow(x, 3);
      }
  }


  Wigner::Wigner(const int max_two_j):
    max_two_j(max_two_j)
  {
    if (max_two_j < 0) {
      auto msg = "wigxjpf: Negative max_two_j.";
      throw CosmoxException(msg, __FILE__, __LINE__);
    }
    wig_table_init(max_two_j, wigner_type);
  }


  Wigner::~Wigner()
  {
    wig_table_free();
  }


  double
  Wigner::get_3j(const int two_j1, const int two_j2, const int two_j3,
                 const int two_m1, const int two_m2, const int two_m3)
  {
    wig_thread_temp_init(max_two_j);
    auto res = wig3jj(two_j1, two_j2, two_j3,
                      two_m1, two_m2, two_m3);
    wig_temp_free();
    return res;
  }


  double
  Wigner::get_6j(const int two_j1, const int two_j2, const int two_j3,
                 const int two_j4, const int two_j5, const int two_j6)
  {
    wig_thread_temp_init(max_two_j);
    auto res =  wig6jj(two_j1, two_j2, two_j3,
                       two_j4, two_j5, two_j6);
    wig_temp_free();
    return res;
  }


  double
  Wigner::get_9j(const int two_j1, const int two_j2, const int two_j3,
                 const int two_j4, const int two_j5, const int two_j6,
                 const int two_j7, const int two_j8, const int two_j9)
  {
    wig_thread_temp_init(max_two_j);
    auto res = wig9jj(two_j1, two_j2, two_j3,
                      two_j4, two_j5, two_j6,
                      two_j7, two_j8, two_j9);
    wig_temp_free();
    return res;
  }

  int
  Wigner::get_max_two_j()
  {
    return max_two_j;
  }
}
