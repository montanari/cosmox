// survey_specs.h -- Survey specifications.
//
// Copyright 2017, 2018, 2019 Francesco Montanari
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/** @file survey_specs.h
 *
 *  Galaxy bias.
 */

#ifndef SURVEY_SPECS_H
#define SURVEY_SPECS_H

#include <math.h>
#include <limits>

namespace cosmox {
  /** @brief Galaxy bias.
   *
   *  Always returns \f$b_1=1\f$ and \f$b_2=b_S=0\f$. This class is
   *  supposed to be subclassed to override member methods. Use the
   *  derived class as input to other methods (e.g., GeneralSpectra).
   */
  class GalaxyBias {
  public:
    virtual ~GalaxyBias() {};
    /** @brief Linear galaxy bias. */
    virtual double b1 (double z);
    /** @brief Quadratic galaxy bias. */
    virtual double b2 (double z);
    /** @brief Tidal galaxy bias. */
    virtual double bS (double z);
  };

  /** @brief Window function in redshift.
   *
   *  The window must be normalized to unity within its extent (zmin,
   *  zmax).
   *
   *  `void* params` can be casted statically to pass arbitrary
   *  parameters to the class (use `nullptr` if that's not needed).
   */
  class Window {
  public:
    Window(const double zmin=0,
           const double zmax=std::numeric_limits<double>::infinity());
    virtual ~Window() {};
    double get_zmin();
    double get_zmax();
    virtual double eval(const double z) = 0;

  protected:
    double zmin;
    double zmax;
  };
} /* namespace cosmox */

#endif  /* SURVEY_SPECS_H */
