// numeric.cc -- numerical utilities.
//
// Copyright 2017, 2018, 2019 Francesco Montanari
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "numeric.h"

namespace cosmox {

  GslInterpCspline::GslInterpCspline(const double* const xa,
                                     const double* const ya,
                                     const size_t size)
  {
    xfirst = xa[0];
    xlast = xa[size-1];
    acc = gsl_interp_accel_alloc ();
    spline = gsl_spline_alloc (gsl_interp_cspline, size);
    gsl_spline_init (spline, xa, ya, size);
  }


  GslInterpCspline::~GslInterpCspline()
  {
    gsl_spline_free (spline);
    gsl_interp_accel_free (acc);
  }


  double
  GslInterpCspline::eval(double xi)
  {
    // if (xi<xfirst || xi>xlast)
    //   {
    //     std::ostringstream msg;
    //     msg << "Interpolation x=" << xi << " value out of range ["
    //         << xfirst << ", " << xlast << "]\n";
    //     throw CosmoxException(msg.str(), __FILE__, __LINE__);
    //   }
    return gsl_spline_eval (spline, xi, acc);
  }


  GslInterp2d::GslInterp2d(const double *const xa, const double *const ya,
                           const double *const za_xy, const size_t nx,
                           const size_t ny)
  {
    const gsl_interp2d_type *T = gsl_interp2d_bilinear;
    za = (double *) malloc(nx * ny * sizeof(double));
    spline = gsl_spline2d_alloc(T, nx, ny);
    xacc = gsl_interp_accel_alloc();
    yacc = gsl_interp_accel_alloc();

    /* set z grid values */
    size_t m, n;
    int count = 0;
    for (m = 0; m < nx; ++m)
      for (n = 0; n < ny; ++n)
        {
          gsl_spline2d_set(spline, za, m, n, za_xy[count]);
          ++count;
        }

    /* initialize interpolation */
    gsl_spline2d_init(spline, xa, ya, za, nx, ny);
  }


  GslInterp2d::~GslInterp2d()
  {
    gsl_spline2d_free(spline);
    gsl_interp_accel_free(xacc);
    gsl_interp_accel_free(yacc);
    free(za);
  }


  double
  GslInterp2d::eval(double xi, double yj)
  {
    return gsl_spline2d_eval(spline, xi, yj, xacc, yacc);
  }


  GslIntegrator::GslIntegrator(size_t workspace_limit)
  {
    this->workspace_limit = workspace_limit;
    this->workspace = gsl_integration_workspace_alloc (workspace_limit);
  }


  GslIntegrator::~GslIntegrator()
  {
    gsl_integration_workspace_free (workspace);
  }


  GslIntegratorResult
  GslIntegrator::qag (const GslFunction func, const double a, const double b,
                      void* const userdata)
  {
    if (limit > workspace_limit)
      throw CosmoxException("Limit exceeds integration workspace",
                            __FILE__, __LINE__);

    gsl_function F;
    F.function = func;
    F.params = userdata;

    double integral;
    double error;
    gsl_set_error_handler_off();
    int status = gsl_integration_qag (&F, a, b, epsabs, epsrel, limit, key,
                                      workspace, &integral, &error);
    if (status) {
        std::ostringstream msg;
        msg << "QAG integration failed, GSL error: " << gsl_strerror (status);
        throw CosmoxException(msg.str(), __FILE__, __LINE__);
    }

    GslIntegratorResult result;
    result.integral = integral;
    result.error = error;
    result.intervals = workspace->size;

    return result;
  }


  GslIntegratorCquad::GslIntegratorCquad(size_t nint)
  {
    this->workspace = gsl_integration_cquad_workspace_alloc (nint);
  }


  GslIntegratorCquad::~GslIntegratorCquad()
  {
    gsl_integration_cquad_workspace_free (workspace);
  }


  GslIntegratorResult
  GslIntegratorCquad::cquad (const GslFunction func, const double a,
                             const double b, void* const userdata)
  {
    gsl_function F;
    F.function = func;
    F.params = userdata;

    double integral;
    double error;
    size_t nevals;
    gsl_set_error_handler_off();
    int status = gsl_integration_cquad (&F, a, b, epsabs, epsrel, workspace,
                                        &integral, &error, &nevals);
    if (status) {
        std::ostringstream msg;
        msg << "CQUAD integration failed, GSL error: " << gsl_strerror (status);
        throw CosmoxException(msg.str(), __FILE__, __LINE__);
    }

    GslIntegratorResult result;
    result.integral = integral;
    result.error = error;
    result.intervals = nevals;

    return result;
  }


  TrilinearInterp::TrilinearInterp(const std::vector<double> x_grid,
                                   const std::vector<double> y_grid,
                                   const std::vector<double> z_grid,
                                   const vector3d<double> values):
    x_grid(x_grid), y_grid(y_grid), z_grid(z_grid), values(values)
  {
    if (!std::is_sorted(std::begin(x_grid), std::end(x_grid)) ||
        !std::is_sorted(std::begin(y_grid), std::end(y_grid)) ||
        !std::is_sorted(std::begin(z_grid), std::end(z_grid)))
      throw CosmoxException("Vectors must be sorted", __FILE__, __LINE__);
  }

  int TrilinearInterp::get_arg0(const double w,
                                const std::vector<double> w_grid)
  {
    if ((w<w_grid.at(0)) || (w>w_grid.back()))
      {
        std::stringstream msg;
        msg << "Interpolated values are requested outside "
            << "of the domain of the input data";
        throw CosmoxException(msg.str(), __FILE__, __LINE__);
      }
    for (size_t i=0; i<w_grid.size(); ++i)
      {
        if (w<w_grid.at(i))
          {
            return i-1;
          }
      }
    // Only case left is w==w_grid.back().
    return w_grid.size()-2;
  }

  void TrilinearInterp::set_hexahedron(const double x, const double y,
                                       const double z)
  {
    auto ix0 = get_arg0(x, x_grid);
    auto iy0 = get_arg0(y, y_grid);
    auto iz0 = get_arg0(z, z_grid);
    x0 = x_grid.at(ix0);
    y0 = y_grid.at(iy0);
    z0 = z_grid.at(iz0);
    x1 = x_grid.at(ix0+1);
    y1 = y_grid.at(iy0+1);
    z1 = z_grid.at(iz0+1);
    c000 = values(ix0, iy0, iz0);
    c100 = values(ix0+1, iy0, iz0);
    c001 = values(ix0, iy0, iz0+1);
    c101 = values(ix0+1, iy0, iz0+1);
    c010 = values(ix0, iy0+1, iz0);
    c110 = values(ix0+1, iy0+1, iz0);
    c011 = values(ix0, iy0+1, iz0+1);
    c111 = values(ix0+1, iy0+1, iz0+1);
  }

  double TrilinearInterp::eval(const double x, const double y, const double z)
  {
    set_hexahedron(x, y, z);
    auto xd = (x-x0) / (x1-x0);
    auto yd = (y-y0) / (y1-y0);
    auto zd = (z-z0) / (z1-z0);
    // Interpolate along x.
    auto c00 = c000*(1-xd) + c100*xd;
    auto c01 = c001*(1-xd) + c101*xd;
    auto c10 = c010*(1-xd) + c110*xd;
    auto c11 = c011*(1-xd) + c111*xd;
    // Interpolate along y.
    auto c0 = c00*(1-yd) + c10*yd;
    auto c1 = c01*(1-yd) + c11*yd;
    // Interpolate along z.
    auto c = c0*(1-zd) + c1*zd;
    return c;
  }

  std::vector<double> linspace(double a, double b, size_t N)
  {
    double h = (b - a) / static_cast<double>(N-1);
    std::vector<double> xs(N);
    typename std::vector<double>::iterator x;
    double val;
    for (x = xs.begin(), val = a; x != xs.end(); ++x, val += h)
      *x = val;
    return xs;
  }

  std::vector<double>
  logspace(const double start, const double stop,
           const int num, const double base)
  {
    double realStart = pow(base, start);
    double realBase = pow(base, (stop-start)/num);

    std::vector<double> retval;
    retval.reserve(num);
    std::generate_n(std::back_inserter(retval), num,
                    Logspace<double>(realStart,realBase));
    return retval;
  }
} // namespace cosmox
