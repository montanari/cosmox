// numeric.h -- numerical utilities.
//
// Define convenience classes to access GSL library functions.
//
// Copyright 2017, 2018, 2019 Francesco Montanari
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/** @file numeric.h
 *
 *  Numerical utilities.
 */

#ifndef NUMERIC_H
#define NUMERIC_H

#include <algorithm>
#include <sstream>
#include <string>
#include <vector>

#include <gsl/gsl_errno.h>
#include <gsl/gsl_integration.h>
#include <gsl/gsl_interp2d.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_spline.h>
#include <gsl/gsl_spline2d.h>
#include <stdlib.h>

#include "cosmox_exception.h"

namespace cosmox {

  /** @typedef GSL function.  */
  typedef double (*GslFunction)(double, void*);


  /**
   * @brief 1D cubic spline interpolation.
   *
   * Wrap 1D cubic spline interpolation from the Gnu Scientific Library.
   *
   * Given a set of data points \f$(x_1, y_1) \dots (x_n, y_n)\f$,
   * compute a continuous interpolating function \f$y(x)\f$ such that
   * \f$y(x_i) = y_i\f$.
   *
   * The interpolation method is cubic spline with natural boundary
   * conditions. The resulting curve is piecewise cubic on each
   * interval, with matching first and second derivatives at the
   * supplied data-points. The second derivative is chosen to be zero
   * at the first point and last point.
   */
  class GslInterpCspline {
  public:
    /**
     * @param x    coordinates.
     * @param y    function values on the x grid.
     * @param size number of data points.
     */
    GslInterpCspline(const double* const xa, const double* const ya,
                     const size_t size);
    ~GslInterpCspline();

    /**
     * @brief Evaluate the interpolated function.
     *
     * @param xi interpolation coordinate.
     */
    double eval(double xi);

  private:
    gsl_interp_accel *acc;
    gsl_spline* spline;
    double xfirst;
    double xlast;
  };


  /**
   * @brief 2D interpolation.
   *
   * Wrap 2D interpolation routines form the Gnu Scientific Library.
   *
   * Give a set of x coordinates x_1,...,x_m and a set of y
   * coordinates y_1,...,y_n, each in increasing order, plus a set of
   * function values z_{ij} for each grid point (x_i, y_j). All the
   * variables are pointers to arrays, and the z_{ij} grid must also
   * be arranged listing the rows one after the other. For instance,
   * the matrix
   *
   * {{z_00, z_01},
   *  {z_10, z_11}},
   *
   * should be thought as:
   *
   * {z_00, z_01, z_10, z_11}.
   *
   * Compute a continuous interpolation function z(x,y) such that
   * z(x_i,y_j) = z_{ij}.
   *
   */
  class GslInterp2d {
  public:
    /**
     * @param xa coordinates.
     * @param ya coordinates.
     * @param za_xy function values on the (xa, ya) grid, with rows listed
     *              in sequence.
     * @param nx number of xa coordinates.
     * @param ny number of ya coordinates.
     */
    GslInterp2d(const double *const xa, const double *const ya,
                const double *const za_xy, const size_t nx,
                const size_t ny);
    ~GslInterp2d();

    /**
     * @brief Evaluate the interpolated function.
     *
     * @param xi interpolation coordinate.
     * @param yj interpolation coordinate.
     */
    double eval(double xi, double yj);

  private:
    double *za;
    gsl_spline2d *spline;
    gsl_interp_accel *xacc;
    gsl_interp_accel *yacc;
  };


  /** @brief GSL integration parameters. */
  struct GslIntegratorParams {
    /**@{*/
    /** @name Common parameters (GslIntegrator and GslIntegratorCquad) */

    /** Desired absolute error. */
    double epsabs = 1.49e-08;
    /** Desired relative error. */
    double epsrel = 1.49e-08;
    /**@}*/

    /**@{*/
    /** @name GslIntegrator only */
    /** Maximum number of subintervals. */
    size_t limit = 500;
    /** The integration rule is determined by the value of key, which
     *  should be chosen from the following symbolic names,
     *
     *    - GSL_INTEG_GAUSS15  (key = 1)
     *    - GSL_INTEG_GAUSS21  (key = 2)
     *    - GSL_INTEG_GAUSS31  (key = 3)
     *    - GSL_INTEG_GAUSS41  (key = 4)
     *    - GSL_INTEG_GAUSS51  (key = 5)
     *    - GSL_INTEG_GAUSS61  (key = 6)
     *
     *  corresponding to the 15, 21, 31, 41, 51 and 61 point
     *  Gauss-Kronrod rules. The higher-order rules give better
     *  accuracy for smooth functions, while lower-order rules save
     *  time when the function contains local difficulties, such as
     *  discontinuities. High order rules can also handle oscillatory
     *  functions (at the expenses of accuracy).
     */
    int key = GSL_INTEG_GAUSS41;
    /**@}*/
  };


  /** @brief Store the result of GSL integration. */
  struct GslIntegratorResult {
    double integral;            /**< Result of the integration. */
    double error;               /**< Estimated absolute error. */
    size_t intervals;           /**< Number of subintervals needed. */
  };


  /** @brief GSL QUADPACK integration methods.
   *
   *  GSL numerical integration (quadrature) of a function in one
   *  dimension. The GSL library reimplements the algorithms used in
   *  QUADPACK.
   *
   *  The subintervals and their results are stored in the memory
   *  provided by a GSL workspace. Its maximum size can be set when
   *  instantiating the class.
   *
   *  Each integration method requires as input parameters the
   *  integrand function (declared as a global function, or as a
   *  static member), integration bounds, and a pointer to arbitrary
   *  data structure to be used within the integrand (if none is
   *  needed, pass \c nullptr).
   *
   *  Example:
   *
   *  @code{.cpp}
   *  #include <iostream>
   *  #include <numeric.h>
   *
   *  struct IntegrandData {
   *    double alpha = 1.0;
   *  };
   *
   *  double
   *  integrand (double x, void* userdata)
   *  {
   *    IntegrandData* data = static_cast<IntegrandData*>(userdata);
   *    double f = log(data->alpha*x) / sqrt(x);
   *    return f;
   *  }
   *
   *  int
   *  main()
   *  {
   *    // \int_0^1 x^{-1/2} log(x) dx = -4
   *    GslIntegrator integrator;
   *    IntegrandData data;
   *    auto result = integrator.qag(&integrand, 0.0, 1.0, &data);
   *    std::cout << "result: "      << result.integral
   *              << "\nerror: "     << result.error
   *              << "\nintervals: " << result.intervals << "\n";
   *  }
   *  @endcode
   */
  class GslIntegrator: public GslIntegratorParams {
  public:
    GslIntegrator(size_t workspace_limit=1000);
    ~GslIntegrator();

    /** @brief QAG adaptive integration
     *
     *  The QAG algorithm is a simple adaptive integration
     *  procedure. The integration region is divided into
     *  subintervals, and on each iteration the subinterval with the
     *  largest estimated error is bisected. This reduces the overall
     *  error rapidly, as the subintervals become concentrated around
     *  local difficulties in the integrand. High-order Gauss-Kronod
     *  rules (GslIntegratorParams::key) also handle efficiently
     *  highly oscillatory functions (at the expense of high
     *  accuracy).
     */
    GslIntegratorResult qag (const GslFunction func, const double a,
                             const double b, void* const userdata);

  private:
    size_t workspace_limit;
    gsl_integration_workspace* workspace;
  };



  /** @brief GSL CQUAD integration method.
   *
   *  CQUAD is a doubly-adaptive general-purpose quadrature routine
   *  which can handle most types of singularities, non-numerical
   *  function values such as \c Inf or \c NaN, as well as some
   *  divergent integrals. It generally requires more function
   *  evaluations than the integration routines in QUADPACK, yet fails
   *  less often for difficult integrands.
   *
   *  The underlying algorithm uses a doubly-adaptive scheme in which
   *  Clenshaw-Curtis quadrature rules of increasing degree are used
   *  to compute the integral in each interval. The L_2-norm of the
   *  difference between the underlying interpolatory polynomials of
   *  two successive rules is used as an error estimate. The interval
   *  is subdivided if the difference between two successive rules is
   *  too large or a rule of maximum degree has been reached.
   */
  class GslIntegratorCquad: public GslIntegratorParams {
  public:
    /** The constructor allocates a workspace sufficient to hold the
     *  data for \c nint intervals. The number \c nint is not the
     *  maximum number of intervals that will be evaluated. If the
     *  workspace is full, intervals with smaller error estimates will
     *  be discarded. A minimum of 3 intervals is required and for most
     *  functions, a workspace of size 100 is sufficient.
     */
    GslIntegratorCquad(size_t nint=100);
    ~GslIntegratorCquad();

    /** @brief Compute the integral.
     *
     *  @param[in] func     Integrand function (declared as a global
     *                      function, or as a static member).
     *  @param[in] a        Integration bound.
     *  @param[in] b        Integration bound.
     *  @param[in] userdata Pointer to arbitrary data structure to be
     *                      used within the integrand (if none is
     *                      needed, pass \c nullptr).
     */
    GslIntegratorResult cquad (const GslFunction func, const double a,
                               const double b, void* const userdata);
  private:
    gsl_integration_cquad_workspace* workspace;
  };

  /** @brief Template for 3 dimensional vectors.
   *
   *  In the usage example below we use random numbers to fill-in a
   *  3-dimensional vector with sizes m1, m2, m3.
   *
   *  @code{cpp}
   *  size_t m1 = 10;
   *  size_t m2 = 20;
   *  size_t m3 = 30;
   *  cosmox::vector3d<double> data(m1, m2, m3);
   *
   *  for (size_t i=0; i<m1; ++i)
   *    for (size_t j=0; j<m2; ++j)
   *      for (size_t k=0; k<m3; ++k)
   *        data(i, j, k) = std::rand();
   *  @endcode
   */
  template <typename T>
  class vector3d {
  public:
    vector3d(size_t d1=0, size_t d2=0, size_t d3=0, T const & t=T()) :
      d1(d1), d2(d2), d3(d3), data(d1*d2*d3, t)
    {}

    T & operator()(size_t i, size_t j, size_t k)
    {
      return data[i*d2*d3 + j*d3 + k];
    }

    T const & operator()(size_t i, size_t j, size_t k) const
    {
      return data[i*d2*d3 + j*d3 + k];
    }

  private:
    size_t d1,d2,d3;
    std::vector<T> data;
  };

  /** @brief Interpolation on a regular grid in 3 dimensions.
   *
   * The data must be defined on a regular grid; the grid spacing
   * however may be uneven.
   */
  class TrilinearInterp {
  public:
    /** @param x_grid The x coordinates of the regular grid.
     *
     *  @param y_grid The y coordinates of the regular grid.
     *
     *  @param z_grid The z coordinates of the regular grid.
     *
     *  @param values The data on the regular grid in 3
     *                dimensions. Its shape must be (x_grid.size(),
     *                y_grid.size(), z_grid.size()).
     */
    TrilinearInterp(const std::vector<double> x_grid,
                    const std::vector<double> y_grid,
                    const std::vector<double> z_grid,
                    const vector3d<double> values);

    /** @brief Evaluate interpolating function at point (x, y, z). */
    double eval(const double x, const double y, const double z);

  private:
    std::vector<double> x_grid;
    std::vector<double> y_grid;
    std::vector<double> z_grid;
    vector3d<double> values;
    double x0;
    double y0;
    double z0;
    double x1;
    double y1;
    double z1;
    double c000;
    double c100;
    double c001;
    double c101;
    double c010;
    double c110;
    double c011;
    double c111;
    int get_arg0(const double w, const std::vector<double> w_grid);
    void set_hexahedron(const double x, const double y, const double z);
  };

  /** @brief Return numbers spaced evenly on a log scale.
   *
   *  Usage example  (40 values with base of 2 starting with 1):
   *
   *  @code{.cpp}
   *  std::vector<double> vals;
   *  std::generate_n(std::back_inserter(vals), 40, Logspace<double>(1,2));
   *  @endcode
   */
  template<typename T>
    class Logspace {
  private:
    T curValue, base;

  public:
  Logspace(T first, T base) : curValue(first), base(base) {}

    T operator()()
    {
      T retval = curValue;
      curValue *= base;
      return retval;
    }
  };

  /** @brief Return evenly spaced numbers over a specified interval.
   *
   *  @param[in] start The starting value of the sequence.
   *  @param[in] stop  The end value of the sequence.
   *  @param[in] num   Number of samples to generate.
   *
   * */
  std::vector<double> linspace(const double a, const double b,
                               const size_t N = 50);

  /** @brief Return numbers spaced evenly on a log scale.
   *
   *  @param[in] start <tt>pow(base, start)</tt> is the starting value
   *                   of the sequence.
   *  @param[in] stop  <tt>pow(base, (stop-start)/num)</tt> is the final
   *                   value of the sequence.
   *  @param[in] num   Number of samples to generate.
   *  @param[in] base  The base of the log space.
   */
  std::vector<double> logspace(const double start, const double stop,
                               const int num = 50,
                               const double base = 10);

} /* namespace cosmox */

#endif  /* NUMERIC_H */
