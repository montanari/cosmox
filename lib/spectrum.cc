// spectrum.cc -- angular power spectrum.
//
// Copyright 2017, 2018, 2019 Francesco Montanari
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "spectrum.h"
#include "cosmox_exception.h"

namespace cosmox
{
double
Spectrum::compute (const int ell, const double z1, const double z2,
                   const TransferType ttype_A, const TransferType ttype_B,
                   const double epsrel, const double epsabs)
{
  return GeneralSpectra::compute (ell, ell, z1, z2, 0, ttype_A, ttype_B,
                                  epsrel, epsabs)
      .real ();
}

std::vector<double>
Spectrum::compute_sp_integrand (std::vector<double> zz, void* params)
{
  double z1 = zz.at (0);
  double z2 = zz.at (1);
  Params* pars = static_cast<Params*> (params);
  auto res = pars->w1->eval (z1) * pars->w2->eval (z2)
             * pars->sp->compute (pars->ell, z1, z2, pars->ttype_A,
                                  pars->ttype_B, pars->epsrel, pars->epsabs);
  std::vector<double> func (1);
  func[0] = res;
  return func;
}

double
Spectrum::compute (const int ell, const std::shared_ptr<Window> w1,
                   const std::shared_ptr<Window> w2,
                   const TransferType ttype_A, const TransferType ttype_B,
                   const double epsrel, const double epsabs)
{
  Params params;
  params.sp = this;
  params.ell = ell;
  params.ttype_A = ttype_A;
  params.ttype_B = ttype_B;
  params.epsrel = epsrel;
  params.epsabs = epsabs;
  params.w1 = w1;
  params.w2 = w2;
  auto bounds = { std::make_pair (w1->get_zmin (), w1->get_zmax ()),
                  std::make_pair (w2->get_zmin (), w2->get_zmax ()) };

  cuba::Cuba integrator;
  integrator.epsrel = epsrel;
  integrator.epsabs = epsabs;
  integrator.maxeval = maxeval;
  auto result = integrator.suave (&compute_sp_integrand, bounds, &params);
  if (result.fail)
    throw CosmoxException ("Cuba failed", __FILE__, __LINE__);
  return result.integral.at (0);
}
} /* namespace cosmox */
