// cosmox_exception.cc -- Exceptions handling

// Copyright 2017, 2018, 2019 Francesco Montanari

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "cosmox_exception.h"

namespace cosmox {

  CosmoxException::CosmoxException(const std::string &msg) :
    std::runtime_error(msg)
  {
    std::ostringstream o;
    o << msg;
    omsg = o.str();
  }


  CosmoxException::CosmoxException(const std::string &msg,
                                   const char *file,
                                   int line) :
    std::runtime_error(msg)
  {
    std::ostringstream o;
    o << file << ":" << line << ": " << msg;
    omsg = o.str();
  }


  CosmoxException::~CosmoxException() throw()
  {
  }


  const char *
  CosmoxException::what() const throw()
  {
    return omsg.c_str();
  }

} // namespace cosmox
