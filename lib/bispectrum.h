// bispetrcum.h -- Bispectrum.
//
// Copyright 2017, 2018, 2019 Francesco Montanari
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef BISPECTRUM_H
#define BISPECTRUM_H

#include <limits>
#include <memory>
#include <vector>

#include "cubapp.h"
#include "survey_specs.h"
#include "numeric.h"
#include "perturbations.h"
#include "special_functions.h"

namespace cosmox {
  typedef double (*GslIntegrandFunc)(double, void* params);

  /** @brief RSD reduced bispectrum.
   *
   *  So far compute only the non-separable contribution to the RSD
   *  bispectrum. (Other terms are more conveniently written as
   *  products of generalized spectra.)
   *
   *  @param[in] perturbs    instance of cosmox::Perturbations.
   *  @param[in] galbias     instance of cosmox::GalaxyBias.
   *  @param[in] linear_rsd  compute linear RSD?
   *  @param[in] epsrel      relative precision for the integration.
   *  @param[in] epsabs      absolute precision for the integration.
   *  @param[in] maxeval     maximum number of integrand evaluations allowed.
   *  @param[in] ignore_fail do not throw exception on failure?
   */
  class BispectrumRsd {
  public:
    BispectrumRsd(const std::shared_ptr<Perturbations> perturbs,
                  const std::shared_ptr<GalaxyBias> galbias,
                  const bool linear_rsd = false,
                  const double epsrel = 1e-3,
                  const double epsabs = 0.,
                  const int maxeval = 5e6,
                  const bool ignore_fail = false);

    /** @brief Compute non-separable RSD contribution.
     *
     *  Compute RSD contribution that cannot be written as product of
     *  generalized spectra, and that involves an integral along the
     *  line of sight besides the usual ones over wavelengths.
     *
     *  The integrand is characterized by a sharp peak along the
     *  radial direction. To avoid numerical precision issues, the
     *  integration along the line of sight can be restricted to a
     *  finite range. E.g., setting peak_width_scale=10. will set
     *  boundaries within [chi_peak / 10, chi_peak * 10], where chi is
     *  the comoving distance. If the scale factor is set to infinity,
     *  integrate within [0, inf].
     *
     *  @param[in] l1, l2, l3       multipoles.
     *  @param[in] z1, z2, z3       redshifts.
     *  @param[in] peak_width_scale boundary width scale factor.
     */
    double get_nonseparable(const int l1, const int l2, const int l3,
                            const double z1, const double z2,
                            const double z3,
                            const double peak_width_scale =
                            std::numeric_limits<double>::infinity());

  private:
    struct Params {
      std::shared_ptr<Perturbations> pt;
      std::shared_ptr<GalaxyBias> galbias;
      bool linear_rsd;
      int l1;
      int l2;
      int l3;
      double z1;
      double z2;
      double z3;
    };

    double integrate(const cuba::CubaIntegrandFunc integrand,
                     const int l1, const int l2, const int l3,
                     const double z1, const double z2, const double z3,
                     const double peak_width_scale);
    static long double integrand_kernel(const double l, const double r,
                                        const double chi);
    static long double integrand_linear_transfer(const double k,
                                                 const double l,
                                                 const double z,
                                                 void* params);
    static std::vector<double> integrand(std::vector<double> vars,
                                         void* params);

    std::shared_ptr<Perturbations> perturbs; /* Only set in constructor. */
    std::shared_ptr<GalaxyBias> galbias; /* Only set in constructor. */
    bool linear_rsd;
    double kmin;
    double kmax;
    double epsrel;
    double epsabs;
    int maxeval;
    bool ignore_fail;
  };

} /* namespace cosmox */

#endif  /* BISPECTRUM_H */
