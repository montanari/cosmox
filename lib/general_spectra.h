// general_spectra.h -- generalized angular power spectra.
//
// Copyright 2017, 2018, 2019 Francesco Montanari
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/** @file general_spectra.h
 *
 *  Generalized angular spectra.
 */


#ifndef GENERAL_SPECTRA_H
#define GENERAL_SPECTRA_H

#include <complex>
#include <tuple>

#include "numeric.h"
#include "perturbations.h"
#include "special_functions.h"
#include "survey_specs.h"

namespace cosmox {

  typedef double (*GslIntegrandFunc)(double, void* params);

  void check_wigner(const int two_max_j, const std::shared_ptr<Wigner> wig);

  /** @brief Equation (A.72) <a
   *         href="https://arxiv.org/abs/1510.04202">arXiv:1510.04202</a>
   */
  double geometry_A (const int l1, const int l2, const int l3,
                     const std::shared_ptr<Wigner> wig);

  /** @brief Factor \f$g_{\ell_1\ell_2\ell_3}\f$ in equation (A.6) <a
   *         href="https://arxiv.org/abs/1510.04202">arXiv:1510.04202</a>
   */
  double geometry_reduced_Gaunt (const int l1, const int l2, const int l3,
                                 const std::shared_ptr<Wigner> wig);

  /** @brief Equation (A.11) <a
   *         href="https://arxiv.org/abs/1510.04202">arXiv:1510.04202</a>
   *
   *  Compute \f$Q^{\ell_1 \ell_2 \ell_3}_{\ell_4 \ell_5 \ell_6}\f$.
   */
  double geometry_Q (const int l1, const int l2, const int l3,
                     const int l4, const int l5, const int l6,
                     const std::shared_ptr<Wigner> wig);

  /** @brief Generalized angular power spectra.
   *
   *  \f$ {}^n C_{l_1\ l_2}^{AB}(z_1, z_2) = i^{\ell_1-\ell_2} 4\pi
   *      \int_{k_{min}}^{k_{max}} k^{n-1} \mathcal{P}_{\mathcal{R}}(k)
   *      \Delta^A_{\ell_1}(k, z_1) \Delta^B_{\ell_2}(k, z_2) dk
   *      \f$
   *
   *  Transfer functions \f$ \Delta^A_{\ell}(k, z) \f$ are determined
   *  by GeneralSpectra::TransferType.  Note that usual angular power
   *  spectra correspond to \f$ C_{l}^{AB}(z_1, z_2) \equiv {}^0 C_{l
   *  \ l}^{AB}(z_1, z_2) \f$. (See also equation A.8 of <a
   *  href="https://arxiv.org/abs/1510.04202">arXiv:1510.04202</a>.)
   */
  class GeneralSpectra {
  public:
    /** @brief Transfer function types.
     *
     *  Each type determines a transfer function \f$
     *  \Delta^A_{\ell}(k, z) \f$. Transfer functions are shown below
     *  in terms of source functions \f$S_A(k, z)\f$ (computed by the
     *  perturbations module). Source functions are defined in
     *  Appendix A of <a
     *  href="https://arxiv.org/abs/1307.1459">arXiv:1307.1459</a>.
     */
    enum TransferType {
      kDensity,      /**< \f$ S_{\delta}(k, z) j_{\ell}(kr(z)) \f$ */
      kDensity1,     /**< \f$ S_{\delta}(k, z) j'_{\ell}(kr(z)) \f$ */
      kVelocity0,     /**< \f$ \frac{1}{aH} S_{\Theta}(k, z)
                           j_{\ell}(kr(z)) \f$ */
      kVelocity,     /**< \f$ \frac{1}{k} S_{\Theta}(k, z)
                      *   j'_{\ell}(kr(z)) \f$ */
      kVelocity1,    /**< \f$\frac{1}{aH} S_{\Theta}(k, z)
                      *   j''_{\ell}(kr(z))\f$ */
      kVelocity2,    /**< \f$\frac{k}{(aH)^2} S_{\Theta}(k, z)
                      *   j'''_{\ell}(kr(z))\f$ */
      kDensityG_kVelocity1,     /**< \f$b_1(z)\f$kDensity + kVelocity1,
                                 *   where \f$b_1(z)\f$ is linear galaxy
                                 *   bias.*/
      kLensing,      /**< \f$ \ell(\ell+1) \int_0^r
                      * dr'\frac{r-r'}{rr'} S_{\Psi+\Phi}\left(k,
                      * z(r')\right) j_{\ell}(kr') \f$ */
    };

    /** @brief Integration strategy.  */
    enum IntegType {
      kQag,                     /**< QAG adaptive integration. */
      kCquad,                   /**< CQUAD doubly-adaptive
                                 *   integration. Less efficient than
                                 *   GeneralSpectra::kQag, but fails
                                 *   less often. */
      kMix,                     /**< Use GeneralSpectra::kQag for same
                                 *   redshift arguments,
                                 *   GeneralSpectra::kCquad
                                 *   otherwise.  */
    };

    GeneralSpectra (std::shared_ptr<Perturbations> perturbs,
                    std::shared_ptr<GalaxyBias> galbias);

    GeneralSpectra (std::shared_ptr<Perturbations> perturbs);

    /** @brief Compute generalized power spectra.
     *
     *  @param[in] l1      multipole.
     *  @param[in] l2      multipole.
     *  @param[in] z1      ttype_A redshift.
     *  @param[in] z2      ttype_B redshift.
     *  @param[in] n       integrand order.
     *  @param[in] stype_1 transfer function type.
     *  @param[in] stype_2 transfer function type.
     *  @param[in] epsrel  relative precision for the integration.
     *  @param[in] epsabs  absolute precision for the integration.
     */
    std::complex<double> compute(const int l1, const int l2,
                                 const double z1, const double z2,
                                 const int n,
                                 const TransferType ttype_A,
                                 const TransferType ttype_B,
                                 const double epsrel=1e-4,
                                 const double epsabs=0,
                                 const IntegType integtype=kMix);

    /** @brief Retrieve perturbations instance.  */
    std::shared_ptr<Perturbations> get_perturbations ();

    /** @brief Retrieve galaxy bias instance.  */
    std::shared_ptr<GalaxyBias> get_galaxy_bias ();

  private:
    std::shared_ptr<Perturbations> perturbs; /* Only set in constructor. */
    double kmin;
    double kmax;

    std::shared_ptr<GalaxyBias> galbias;
    static double integrand (double k, void* params);

    struct Params {
      std::shared_ptr<Perturbations> pt;
      int l1;
      int l2;
      double z1;
      double z2;
      int n;
      TransferType ttype_A;
      TransferType ttype_B;
      double galbias_z1;
      double galbias_z2;
    };
  };

  struct LensingParams {
    std::shared_ptr<Perturbations> pt;
    int ell;
    double k;
    double z;
  };

  double transfer_density (const std::shared_ptr<Perturbations> perturbs,
                           const int ell,
                           const double k,
                           const double z);

  double transfer_density_1 (std::shared_ptr<Perturbations> perturbs,
                             const int ell,
                             const double k,
                             const double z);

  double transfer_velocity (const std::shared_ptr<Perturbations> perturbs,
                           const int ell,
                           const double k,
                           const double z);

  double transfer_velocity_1 (std::shared_ptr<Perturbations> perturbs,
                              const int ell,
                              const double k,
                              const double z);

  double transfer_velocity_2 (std::shared_ptr<Perturbations> perturbs,
                              const int ell,
                              const double k,
                              const double z);

  double transfer_lensing (std::shared_ptr<Perturbations> perturbs,
                           const int ell,
                           const double k,
                           const double z,
                           double epsrel=1e-4);

  double get_transfer (const GeneralSpectra::TransferType ttype,
                       const std::shared_ptr<Perturbations> perturbs,
                       const int ell,
                       const double k,
                       const double z,
                       const double gal_bias);
} /* namespace cosmox */

#endif  /* GENERAL_SPECTRA_H */
