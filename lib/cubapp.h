// cubapp.h -- friendly interface to the Cuba library.
//
// Cuba <http://www.feynarts.de/cuba/> is a library for
// multidimensional integration.
//
// This header and the respective source file can be easily imported
// into separate projects.
//
// Copyright 2017, 2018, 2019 Francesco Montanari
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


/** @file cubapp.h
 *  @brief Friendly interface to the Cuba library
 *
 *  Cuba <http://www.feynarts.de/cuba/> is a library for multidimensional
 *  integration. We refer to the original "Cuba" library as "libcuba".
 *
 *  Here follows a basic usage example.
 *
 *  \note The integrand must always be defined as a global function, or as a
 *        static class member. Additional parameters can be passed by casting
 *        the \c void* \c userdata argument to an arbitrary type (as shown
 *        below). This makes integration methods susceptible to malign race
 *        conditions if used in multi-threaded code, which should be anyway
 *        avoided given that libcuba already internally parallelizes
 *        computations.
 *
 *  \code{.cpp}
 *  #include<iostream>
 *  #include <utility>
 *
 *  #include "cubapp.h"
 *
 *  struct Userdata {
 *    double a = 2.0;
 *  };
 *
 *  std::vector<double>
 *  integrand(std::vector<double> xx, void* userdata)
 *  {
 *    std::vector<double> func(2);
 *    double x = xx.at(0);
 *    double y = xx.at(1);
 *    double z = xx.at(2);
 *
 *    // Cast the void* argument to arbitrary type (in this case,
 *    // to Userdata*).
 *    Userdata* data = static_cast<Userdata*>(userdata);
 *
 *    // A two-component, three-dimensional function.
 *    func[0] = data->a * sin(x)*cos(y)*exp(z);
 *    func[1] = 1./(1. - x*y*z);
 *
 *    return func;
 *  }
 *
 *  int
 *  main()
 *  {
 *    cuba::Cuba integrator;
 *
 *    // Change default Cuba parameters.
 *    integrator.epsrel = 1e-8;
 *    integrator.maxeval = 500000;
 *
 *    // Arbitrary integration bounds.
 *    auto bounds = {std::make_pair(-0.5, 0.0),
 *                   std::make_pair(0.0, 0.5),
 *                   std::make_pair(0.0, 0.9)};
 *
 *    // Arbitrary data type providing extra parameters to the integrand.
 *    Userdata data;
 *
 *    // If no user data is needed in the integrand, use `nullptr':
 *    // integrator.cuhre(&integrand, bounds, nullptr).
 *    auto result = integrator.cuhre(&integrand, bounds, &data);
 *
 *    // Check if the requested accuracy is reached.
 *    if (result.fail) throw std::runtime_error("Integration failed");
 *
 *    // Print the integrated components.
 *    for (auto x : result.integral)
 *      std::cout << x << std::endl; // {-0.171328, 0.219058}
 *
 *    return 0;
 *  }
 *  \endcode
 */


#ifndef CUBAPP_H
#define CUBAPP_H

#include <limits>
#include <cmath>
#include <stdlib.h>
#include <sstream>
#include <string>
#include <vector>

#include <cuba.h>

namespace cuba {

  /** \typedef CubaIntegrandFunc
   *
   *  Usage example:
   *
   *  \code{.cpp}
   *  #include "cubapp.h"
   *
   *  struct Userdata {
   *    double two = 2.0;
   *  };
   *
   *  // Multiply vector by two. For illustrative purposes, retrieve
   *  // the multiplicative constant from a user defined struct.
   *  std::vector<double>
   *  func(std::vector<double> vec, void* userdata)
   *  {
   *    Userdata* data = static_cast<Userdata*>(userdata);
   *    std::vector<double> twice;
   *    for (auto x : vec) twice.push_back(data->two * x);
   *    return twice;
   *  }
   *
   *  std::vector<double>
   *  call_func(Cuba::IntegrandFunc func)
   *  {
   *    std::vector<double> vec = {1.0, 2.0};
   *    Userdata data;
   *    return func(vec, &data);  // If no data, call as func(vec, nullptr).
   *  }
   *
   *  int
   *  main()
   *  {
   *    call_func(&func);  // {2.0, 4.0}
   *    return 0;
   *  }
   *  \endcode
   */
  typedef std::vector<double> (*CubaIntegrandFunc)(std::vector<double>,
                                                   void* userdata);

  /** Integration bounds. Use a global variable to make them accessible
   *  to the static member function Cuba::set_unit_integrand.
   */
  static std::vector<std::pair<double, double>> BOUNDS;

  /** Pointer to integrand function. Use a global variable to make it
   *  accessible to the static member function
   *  Cuba::set_unit_integrand.
   */
  static CubaIntegrandFunc INTEGRAND_PTR;


  /** @brief Cuba parameters.
   *
   *  Cuba methods depend on several parameters. Here we set
   *  reasonable default values. If the integration is not
   *  satisfactory, the first parameters you probably want to tune are
   *  the accuracy (CubaParams::epsrel, CubaParams::epsabs) and the
   *  maximum number of integrand evaluation allowed
   *  (CubaParams::maxeval).
   *
   *  While the parameters are here briefly documented, some of them
   *  necessitate a non-trivial explanation.  Divonne-specific
   *  parameters are particularly involved. See the official <a
   *  href="http://www.feynarts.de/cuba/">libcuba</a> documentation
   *  (from which we extracted our short descriptions) for more
   *  information.
   *
   *  Parameters defined as \c cubareal can be easily casted to
   *  \c double type as \c (double)epsrel.
   */
  struct CubaParams{
    /**@{*/
    /** @name Common parameters */
    /** Maximum number of points to be given to the integrand routine
     *   at each invocation. */
    int nvec = 1;
    /** Relative accuracy. */
    cubareal epsrel = 1e-3;
    /** Absolute accuracy. */
    cubareal epsabs = 1e-12;
    /** Flags governing the integration.
     *
     *  - 1st and 2nd bits: verbosity level.
     *
     *  - 3rd bit: Use all samples (0) or only the last set (1) in the
     *    final result.
     *
     *  - 4th bit: (Vegas and Suave only) apply (0) or not (1) additional
     *    smoothing to the importance function.
     *
     *  - 5th bit: delete (0) or retain (1) state file.
     *
     *  - 6th bit: (Vegas only) take the integrator state from the
     *    state file if present (0), or always ignore the state file
     *    (1).
     *
     *  - 9th-32nd bits: \c level random-number generator, see
     *    CubaParams::seed.
     *
     *  Usage example (binary literals are allowed since C++14):
     *
     *  - <tt>flags = 0b11</tt> (decimal 3), sets maximum
     *    verbosity level (two rightmost bits are 1).
     *
     *  - <tt>flags = 0b100</tt> (decimal 4), sets minimum verbosity level (two
     *    rightmost bits are zero), and keeps only the last sample
     *    (third bit set to 1).
     */
    int flags = 0;
    /** Seed for the pseudo-random-number generator (\c level
     *  indicates the 9th-32nd bits of CubaParams::flags.):
     *
     *  <table>
     *  <tr><th>\c seed <th>\c level <th> Generator </tr>
     *  <tr><td>zero <td> N/A <td>Sobol (quasi-random)
     *  <tr><td>non-zero <td>zero <td> Mersenne Twister (pseudo-random)
     *  <tr><td>non-zero <td>non-zero <td> Ranlux (pseudo-random).
     *  </table>
     */
    int seed = 0;
    /** Minimum number of integrand evaluations required.*/
    int mineval = 0;
    /** Approximate maximum number of integrand evaluations allowed.*/
    int maxeval = 50000;
    /** Filename for storing the internal state, or null pointer. */
    char* statefile = nullptr;
    /** Spinning cores pointer. */
    void* spin = nullptr;
    /**@}*/

    /**@{*/
    /** @name Vegas only */
    /** Starting number of integrand evaluations per iteration. */
    int nstart = 1000;
    /** Increase in the number of integrand evaluations per
     *  iteration. */
    int nincrease = 500;
    /** Batch size for sampling. */
    int nbatch = 1000;
    /** Slot in the internal grid table. */
    int gridno = 0;
    /**@}*/

    /**@{*/
    /** @name Suave only */
    /** Number of new integrand evaluations in each subdivision.*/
    int nnew = 1000;
    /** Minimum number of samples a former pass must contribute to a
     *  subregion to be considered in that region compound integral
     *  value. Increasing nmin may reduce jumps in the chi-square
     *  value.
     */
    int nmin = 2;
    /** Type of norm used to compute the fluctuation of a sample. Use
     *  larger values for flat integrands and small for volatile
     *  integrands with high peaks. Since flatness appears in the
     *  exponent, too large values should be avoided (no more than a
     *  few hundred).
     */
    double flatness = 25.;
    /**@}*/

    /**@{*/
    /** @name Divonne only
     *
     *  These parameters are particularly involved, see <a
     *  href="http://www.feynarts.de/cuba/">libcuba</a> documentation
     *  for more detailed descriptions. */
    /** Determines sampling in the partitioning phase.
     *
     *  \c key1 = 7, 9, 11, 13 selects the cubature rule of degree \c
     *  key1.  The degree-11 rule is available only in 3 dimensions,
     *  the degree-13 rule only in 2 dimensions.

     *  For other values of \c key1, a quasi-random sample of |\c
     *  key1| points is used, where the sign of \c key1 determines the
     *  type of sample:
     *
     *  - \c key1 > 0, use a Korobov quasi-random sample,
     *
     *  - \c key1 < 0, use a standard sample (a Sobol quasi-random
     *    sample if \c seed = 0, otherwise a pseudo-random sample).
     */
    int key1 = 47;
    /** Determines sampling in the final integration phase.
     *
     *  \c key2 = 7, 9, 11, 13 selects the cubature rule of degree \c
     *  key2. Note that the degree-11 rule is available only in 3
     *  dimensions, the degree-13 rule on ly in 2 dimensions.
     *
     *  For other values of \c key2, a quasi-random sample is
     *  used, where the sign of \c key2 determines the type of
     *  sample:
     *
     *  - \c key2 > 0, use a Korobov quasi-random sample.
     *
     *  - \c key2 < 0, use a standard sample (see
     *    CubaParams::key1).
     *
     *  and |\c key2| determines the number of points:
     *
     *  - |\c key2| >= 40, sample |\c key2| points.
     *
     *  - |\c key2| < 40, sample |\c key2|\f$n_{need}\f$ points,
     *    where \f$n_{need}\f$ is the number of points needed to
     *    reach the prescribed accuracy, as estimated by Divonne from
     *    the results of the partitioning phase.
     */
    int key2 = 1;
    /** Sets the strategy for the refinement phase.
     *
     *  - \c key3 = 0, do not treat the subregion any further.
     *
     *  - \c key3 = 1, split the subregion up once more.
     *
     *  Otherwise, the subregion is sampled a third time with \c key3
     *  specifying the sampling parameters exactly as CubaParams::key2.
     */
    int key3 = 1;
    /** Controls the thoroughness of the partitioning phase.
     *
     *  \c maxpass can be understood as the number of safety
     *  iterations that are performed before the partition is accepted
     *  as final and counting consequently restarts at zero whenever
     *  new structures are found.
     */
    int maxpass = 5;
    /** Width of the border of the integration region.
     *
     * Use a non-zero border if the integrand subroutine cannot
     * produce values directly on the integration boundary.
     */
    cubareal border = 0.;
    /** Maximum \f$\chi^2\f$ value a single subregion is allowed to
     *  have in the final integration phase.
     */
    cubareal maxchisq = 10.;
    /** A bound, given as the fraction of the requested error of the
     *  entire integral, which determines whether it is worthwhile
     *  further examining a region that failed the \f$\chi^2\f$
     *  test. */
    cubareal mindeviation = .25;
    /** Number of points in the CubaParams::xgiven array. */
    int ngiven = 0;
    /** Leading dimension of CubaParams::xgiven, i.e. the offset
     *  between one point and the next in memory. A reasonable value
     *  may be \c ndim (number of integral dimensions).*/
    int ldxgiven = 0;
    /** A list of points where the integrand might have peaks.
     *
     *  The array dimensions must be [CubaParams::ldxgiven,
     *  CubaParams::ngiven].
     */
    cubareal* xgiven = nullptr;
    /** Maximum number of extra points the peak-finder subroutine will
     *  return. If \c nextra is zero, Cuba::peakfinder is not called
     *  and an arbitrary object may be passed in its place, e.g. just
     *  0. */
    int nextra = 0;
    /** Peak-finder procedure. This function is called whenever a
     *  region is up for subdivision and is supposed to point out
     *  possible peaks lying in the region, thus acting as the dynamic
     *  counterpart of the static list of points supplied in
     *  CubaParams::xgiven.
     *
     *  The prototype is defined in libcuba.
     */
    peakfinder_t peakfinder = nullptr;
    /**@}*/

    /**@{*/
    /** @name Cuhre only*/
    /** \c key = 7, 9, 11, 13 selects the cubature rule of degree \c
     *  key. Note that the degree-11 rule is available only in 3
     *  dimensions, the degree-13 rule only in 2 dimensions.  For
     *  other values, the default rule is taken, which is the
     *  degree-13 rule in 2 dimensions, the degree-11 rule in 3
     *  dimensions, and the degree-9 rule otherwise.  */
    int key = 0;
    /**@}*/
  };


  /** @brief Store the result of an integration. */
  struct CubaResult {
    std::vector<double> integral; /**< Result of the integration. */
    std::vector<double> error;    /**< Estimated numerical error of
                                   *   the integral.  */
    std::vector<double> prob;   /**< Probability that the error is
                                 *   *not* a reliable estimate. (Note
                                 *   that Suave often does not provide
                                 *   reliable errors.)  */
    int fail;                   /**< Flag: zero if the integral
                                 *   accuracy has been reached, non-zero
                                 *   otherwise. */
    int nregions;               /**< Actual number of subregions
                                 *   needed. Not used in Vegas, which
                                 *   always returns -1. */
    int neval;                  /**< Actual number of integrand
                                 *   evaluations needed. */
  };


  /** @brief Friendly interface to the <a
   *         href="http://www.feynarts.de/cuba/">Cuba</a> library.
   *
   *  Improvements over the standard libcuba C interface:
   *
   *  - Allow arbitrary integration limits.
   *
   *    The standard C interface requires to scale the integrand so that
   *    the integration bounds always coincide with the unit
   *    hypercube. The rescaling is simple, but it is confusing (from
   *    libcuba homepage: "Upward of 75% of all questions [regarding
   *    libcuba] have to do with how to choose bounds different from
   *    the unit hypercube [...]").
   *
   *    To specifiy infinite ranges, simply use the
   *    std::numeric_limits::infinity special value.
   *
   *  - Default parameters.
   *
   *    The original libcuba C interface requires an explicit call
   *    passing *many* involved parameters. Here we simplify the call
   *    to integration methods. Most parameters are set automatically
   *    to reasonable values (CubaParams). Their value can be changed,
   *    too.
   *
   *  For a usage example see cubapp.h top-level documentation.
  */
  class Cuba : public CubaParams {
  public:
    /** @brief Vegas
     *
     *  Use importance sampling for variance reduction. Compared to
     *  other algorithms, it is only in some cases competitive in
     *  terms of the number of samples needed to reach a prescribed
     *  accuracy.
     */
    CubaResult vegas(const CubaIntegrandFunc integrand,
                     const std::vector<std::pair<double, double>> bounds,
                     void* userdata);
    /** @brief Suave
     *
     *  Combine the advantages of two popular methods: importance
     *  sampling as done by Cuba::vegas and subregion sampling in a
     *  manner similar to Miser. By dividing into subregions, Suave
     *  manages to a certain extent to get around Vegas difficulty to
     *  adapt its weight function to structures not aligned with the
     *  coordinate axes.
     */
    CubaResult suave(const CubaIntegrandFunc integrand,
                     const std::vector<std::pair<double, double>> bounds,
                     void* userdata);
    /** @brief Divonne
     *
     *  Further development of the CERNLIB routine D151. Divonne works
     *  by stratified sampling, where the partitioning of the
     *  integration region is aided by methods from numerical
     *  optimization. It allows to to supply knowledge about the
     *  integrand. Narrow peaks in particular are difficult to find
     *  without sampling very many points, especially in high
     *  dimensions. Often the exact or approximate location of such
     *  peaks is known from analytic considerations, however, and with
     *  such hints the desired accuracy can be reached with far fewer
     *  points.
     *
     *  @note Divonne evaluates the integrand directly on the
     *        integration boundary. If the integrand is not defined on
     *        the border, or if a (semi-)infinite range is requested,
     *        set CubaParams::border to a non-zero value.
     *
     *  @warning A memory leak (difficult to reproduce) has been
     *           detected when calling Divonne. Backtrace (files refer
     *           to libcuba):
     *           @code{.unparsed}
     *           Syscall param socketcall.sendto(msg) points to uninitialised byte(s)
     *              at 0x732561F: send (send.c:26)
     *              by 0x5CE2999: writesock (sock.h:60)
     *              by 0x5CE2999: ExploreParallel (Parallel.c:245)
     *              by 0x5CE2999: Explore (Parallel.c:262)
     *              by 0x5CE2999: Iterate (Iterate.c:40)
     *              by 0x5CE641F: Integrate (Integrate.c:139)
     *              by 0x5CE69D2: Divonne (Divonne.c:61)
     *           @endcode
     *           Check (e.g., with Valgrind) that this is not your case.
     */
    CubaResult divonne(const CubaIntegrandFunc integrand,
                       const std::vector<std::pair<double, double>> bounds,
                       void* userdata);
    /** @brief Cuhre
     *
     *  Employ a cubature rule for subregion estimation in a globally
     *  adaptive subdivision scheme. It is hence a deterministic, not
     *  a Monte Carlo method. In each iteration, the subregion with
     *  the largest error is halved along the axis where the integrand
     *  has the largest fourth difference. Cuhre is quite powerful in
     *  moderate dimensions, and is usually the only viable method to
     *  obtain high precision, say relative accuracies much below
     *  \f$10^{-3}\f$.
     */
    CubaResult cuhre(const CubaIntegrandFunc integrand,
                     const std::vector<std::pair<double, double>> bounds,
                     void* userdata);

  private:
    /** @brief Initialize each new integration.
     *
     *  This function must be called before each call to libcuba
     *  integration functions.
     *
     *  \warning This function is delicate, since it sets the global
     *           variables to make them available to the static method
     *           Cuba::set_unit_integrand. (It also retrieves the
     *           integrand dimension and number of components.)
     * */
    void init_new_integ(const CubaIntegrandFunc integrand,
                        const std::vector<std::pair<double, double>> bounds,
                        void* userdata);

    /** @brief Function to be integrated along the unit hypercube.
     *
     *  Define the actual integrand array (f[] argument) along the
     *  unit hypercube, based on the integrand provided by the user as
     *  an argument of the integration public methods.
     *
     *  The rescale from arbitrary integration bounds to the unit
     *  hypercube is, in one dimension, \f$\int_a^b dx f[x] \to \int_0^1
     *  dy f[a + (b - a) y] (b - a)\f$.  This generalizes
     *  straightforwardly to more than one dimension.
     *
     *  This function needs to be static because libcuba prototypes
     *  require a `int (*)' pointer to function. As a non-static
     *  member we would have instead an incompatible `int (Cuba::*)'
     *  type. (Pointers to member functions are <a
     *  href="https://isocpp.org/wiki/faq/pointers-to-members">tricky</a>).
     *  This implies that the actual integrand function (defined by
     *  the user) that will set the f[] argument in
     *  Cuba::set_unit_integrand can only be accessed via the global
     *  variable UNIT_INTEGRAND_PTR.
     */
    static int set_unit_integrand(const int *ndim, const cubareal x[],
                                  const int *ncomp, cubareal f[],
                                  void *userdata);
    /** @brief Throw runtime error printing the critical point. */
    static void throw_isnan(const std::vector<double> xx);

    /** @brief Fill in the CubaResult instance members.  */
    CubaResult set_result(const double* const integral,
                          const double* const error,
                          const double* const prob,
                          const int fail,
                          const int nregions,
                          const int neval);

    int ndim;                   /**< Number of integral dimensions. */
    int ncomp;                  /**< Number of integral components.*/
  };

} /* namespace cuba */

#endif  /* CUBAPP_H */
