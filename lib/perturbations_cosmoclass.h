// perturbations_cosmoclass.h -- perturbations from CLASS code.
//
// Source functions are linear combinations of cosmological
// perturbations, dependent on wavelength and redshift. Each class of
// this module corresponds to a method to compute source
// functions. This may require an interface to an external Boltzmann
// solver code. The class Perturbations retrieves the correct
// computation method and it is the interface meant to be imported in
// other modules.
//
// Copyright 2017, 2018, 2019 Francesco Montanari
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef PERTURBATIONS_COSMOCLASS_H
#define PERTURBATIONS_COSMOCLASS_H

#include <memory>
#include <utility>

#pragma push_macro("I")
#pragma push_macro("MAX")
#pragma push_macro("MIN")
#undef I
#undef MAX
#undef MIN
#include <class.h>
#pragma pop_macro("I")
#pragma pop_macro("MAX")
#pragma pop_macro("MIN")

#include "numeric.h"
#include "perturbations.h"

namespace cosmox {

  /**
   * @brief Retrieve perturbations from the CLASS Boltzmann solver.
   *
   * Strictly speaking, this class retrieves CLASS <em>source
   * functions</em> (linear combinations of perturbations normalized
   * to the initial curvature perturbation).
   */
  class PerturbsCosmoclass : public Perturbations {
  public:
    /** @brief Constructor.*/
    PerturbsCosmoclass(std::string inifile);
    virtual ~PerturbsCosmoclass();

    const double get_source(const SourceType stype,
                            const double wavenumber,
                            const double redshift) override;
    const double get_primordial_spectrum(const double wavenumber) override;

    std::pair<double, double> get_k_lims() override;

    /** Background. */
    double bg_comoving_age() override;
    double bg_comoving_distance(const double redshift) override;
    double bg_Hubble(const double redshift) override;
    double bg_velocity_growth_factor(const double redshift) override;
    double bg_z_of_tau(const double tau) override;

  private:
    /** Initialize CLASS structures. */
    const int setup();
    /** Clear CLASS structures. */
    const int teardown();
    /** Set the required CLAS pt.index_... values. In particular, set
     *  the correspondence between source types and CLASS indices into
     *  the index_tp hash table.
     */
    void set_index_pt();
    /** Interpolate all source functions.*/
    void interpolate();
    /** Compute a given source type at the interpolation points.  */
    void fill_source(const int index_type, const int k_size,
                     const int tau_size, double *source_kt);
    void print_version();
    /** Compute `double' background quantity corresponding to index
     *  defined in struct background at given redshift. (If needed,
     *  the return value may be generalized using a template.)
     */
    double get_background_double(const double redshift, const int index);

    int argc;
    char **argv;

    struct precision pr;        /* precision parameters */
    struct background ba;       /* cosmological background */
    struct thermo th;           /* thermodynamics */
    struct perturbs pt;         /* source functions */
    struct transfers tr;        /* transfer functions */
    struct primordial pm;       /* primordial spectra */
    struct spectra sp;          /* output spectra */
    struct nonlinear nl;        /* non-linear spectra */
    struct lensing le;          /* lensed spectra */
    struct output op;           /* output files */
    ErrorMsg errmsg;            /* error messages */

    /** Only scalar modes and adiabatic initial conditions are
     *  implemented. In principle CLASS would allow to add a loop over
     *  the modes and over the initial conditions (similarly to what
     *  we do for PerturbsCosmoclass::index_tp).
     */
    int index_md;
    int index_ic;
    /** Hash table containing the correspondence between source types
     *  (keys) and CLASS indices (values). In this case we loop over
     *  different types.
     */
    std::unordered_map<int, int> index_tp;

    /** Hash table containing interpolation tables (values) to
     *  evaluate a specific source (keys).
     */
    // We need a pointer to the class because to create a hash table
    // with a custom class as value, the class would need to provide a
    // default constructor.
    std::unordered_map<int, std::unique_ptr<GslInterp2d>> interp2d;

    std::unique_ptr<GslInterpCspline> z_of_tau_interp = nullptr;
  };
} /* namespace cosmox */

#endif  /* PERTURBATIONS_COSMOCLASS_H */
