// cubapp.cc -- friendly interface to the Cuba library.
//
// Cuba <http://www.feynarts.de/cuba/> is a library for
// multidimensional integration.
//
// This header and the respective source file can be easily imported
// in other projects.
//
// Copyright 2017, 2018, 2019 Francesco Montanari
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "cubapp.h"

namespace cuba {

  void
  Cuba::init_new_integ(const CubaIntegrandFunc integrand,
                       const std::vector<std::pair<double, double>> bounds,
                       void* userdata)
  {
    // Global variables available to the static method
    // Cuba::set_unit_integrand.
    INTEGRAND_PTR = integrand;
    BOUNDS = bounds;

    // Compute number of integral dimensions and components.
    ndim = bounds.size();
    std::vector<double> lows(ndim);    // Lower integration bound.
    for(auto i=0; i<ndim; ++i) {
      lows.at(i) = bounds.at(i).first;
    }
    ncomp = integrand(lows, userdata).size(); // Dumb call, just get the number
                                              // of components.
  }


  void
  Cuba::throw_isnan(const std::vector<double> xx)
  {
    std::stringstream msg;
    msg << "Integrand is NaN at point {";
    for(size_t i = 0; i < xx.size(); ++i)
      {
        if(i != 0)
          msg << ",";
        msg << xx[i];
      }
    msg << "} \n";
    throw std::runtime_error(msg.str());
  }


  int
  Cuba::set_unit_integrand(const int* pndim, const cubareal x[],
                           const int* pncomp, cubareal f[],
                           void* userdata)
  {
    std::vector<double> xx;
    for (auto i=0; i<(*pndim); ++i)
      xx.push_back(x[i]);

    auto inf = std::numeric_limits<double>::infinity();
    double jacobian = 1.;
    std::vector<double> scaled_args (*pndim);

    for(auto j=0; j<(*pndim); ++j) {
      auto low = BOUNDS.at(j).first;
      auto up = BOUNDS.at(j).second;
      auto x = xx.at(j);
      if ((low==0.) and (up==1.))
        {
          scaled_args.at(j) = x;
          jacobian *= 1.;
        }
      if ((std::min(low, up)==-inf) and (std::max(low, up)==inf))
        {
          scaled_args.at(j) = (2.*x-1.) / (pow(x, 2.)-x); // [-inf, inf]
          jacobian *= ((2.*pow(x, 2.) - 2.*x + 1.)
                       / (pow(x, 4.) - 2*pow(x, 3.) + pow(x, 2.)));
          if (low==inf) jacobian *= -1.; // [inf, -inf]
        }
      else if (std::max(low, up)==inf)
        {
          scaled_args.at(j) = std::min(low, up) + (1.-x)/x; // [a, inf]
          jacobian *= pow(x, -2.);
          if (low==inf) jacobian *= -1.; // [inf, a]
        }
      else if (std::min(low, up)==-inf)
        {
          scaled_args.at(j) = std::max(low, up) - (1.-x)/x; // [-inf, a]
          jacobian *= pow(x, -2.);
          if (up==-inf) jacobian *= -1.; // [a, -inf]
        }
      else
        {
          scaled_args.at(j) = low + (up-low)*x; // [a, b]
          jacobian *= (up-low);
        }
    }

    // Scale the integrand to the unit hypercube.
    for(auto i=0; i<(*pncomp); ++i) {
      f[i] = INTEGRAND_PTR(scaled_args, userdata).at(i) * jacobian;
      if (std::isnan(f[i])) throw_isnan(xx);
    }

    return 0;
  }


  CubaResult
  Cuba::set_result(const double* const integral,
                   const double* const error,
                   const double* const prob,
                   const int fail,
                   const int nregions,
                   const int neval)
  {
    CubaResult result;

    for (auto i=0; i<ncomp; ++i) {
      result.integral.push_back((double)integral[i]);
      result.error.push_back((double)error[i]);
      result.prob.push_back((double)prob[i]);
    }
    result.fail = fail;
    result.nregions = nregions;
    result.neval = neval;

    return result;
  }


  CubaResult
  Cuba::vegas(const CubaIntegrandFunc integrand,
              const std::vector<std::pair<double, double>> bounds,
              void* userdata)
  {
    init_new_integ(integrand, bounds, userdata);

    int nregions = -1;               // Not used by Vegas.
    int fail,  neval;
    cubareal integral[ncomp], error[ncomp], prob[ncomp];
    Vegas(ndim, ncomp, set_unit_integrand, userdata, nvec,
          epsrel, epsabs, flags, seed,
          mineval, maxeval, nstart, nincrease, nbatch,
          gridno, statefile, spin,
          &neval, &fail, integral, error, prob);

    return set_result(integral, error, prob, fail, nregions, neval);
  }


  CubaResult
  Cuba::suave(const CubaIntegrandFunc integrand,
              const std::vector<std::pair<double, double>> bounds,
              void* userdata)
  {
    init_new_integ(integrand, bounds, userdata);

    int fail, nregions, neval;
    cubareal integral[ncomp], error[ncomp], prob[ncomp];
    Suave(ndim, ncomp, set_unit_integrand, userdata, nvec,
          epsrel, epsabs, flags, seed,
          mineval, maxeval, nnew, nmin, flatness,
          statefile, spin,
          &nregions, &neval, &fail, integral, error, prob);

    return set_result(integral, error, prob, fail, nregions, neval);
  }


  CubaResult
  Cuba::divonne(const CubaIntegrandFunc integrand,
                const std::vector<std::pair<double, double>> bounds,
                void* userdata)
  {
    init_new_integ(integrand, bounds, userdata);

    if ((ndim < 2) || (ndim>33))
      throw
        std::invalid_argument("Divonne requires 2 <= ndim <= 33 dimensions");

    int fail, nregions, neval;
    cubareal integral[ncomp], error[ncomp], prob[ncomp];
    Divonne(ndim, ncomp, set_unit_integrand, userdata, nvec,
            epsrel, epsabs, flags, seed,
            mineval, maxeval, key1, key2, key3, maxpass,
            border, maxchisq, mindeviation,
            ngiven, ldxgiven, xgiven, nextra, peakfinder,
            statefile, spin,
            &nregions, &neval, &fail, integral, error, prob);

    return set_result(integral, error, prob, fail, nregions, neval);
  }


  CubaResult
  Cuba::cuhre(const CubaIntegrandFunc integrand,
              const std::vector<std::pair<double, double>> bounds,
              void* userdata)
  {
    init_new_integ(integrand, bounds, userdata);

    if ((ndim < 2))
      throw std::invalid_argument("Cuhre requires ndim >= 2 dimensions");

    int fail, nregions, neval;
    cubareal integral[ncomp], error[ncomp], prob[ncomp];
    Cuhre(ndim, ncomp, set_unit_integrand, userdata, nvec,
          epsrel, epsabs, flags,
          mineval, maxeval, key,
          statefile, spin,
          &nregions, &neval, &fail, integral, error, prob);

    return set_result(integral, error, prob, fail, nregions, neval);
  }
} /* namespace cuba */
