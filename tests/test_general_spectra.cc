// test_general_spectra.cc -- Check generalized angular spectra.

// Copyright 2017, 2018, 2019 Francesco Montanari

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "lib/general_spectra.h"
#include "lib/special_functions.h"
#include "lib/survey_specs.h"
#include "tutils.h"

namespace cosmox
{
void
test_geometry_A (std::shared_ptr<Wigner> wig)
{
  std::vector<std::vector<int> > grid
      = { { 3, 2, 1 }, { 30, 20, 10 }, { 300, 200, 100 } };
  std::vector<double> expected
      = { 0.57735026919, 0.930484210398, 0.992558887009 };
  int i = 0;
  for (auto point : grid)
    {
      int l1 = point[0];
      int l2 = point[1];
      int l3 = point[2];
      bool almosteq = almost_eq (geometry_A (l1, l2, l3, wig), expected[i]);
      assert (almosteq);
      ++i;
    }
}

void
test_geometry_reduced_Gaunt (std::shared_ptr<Wigner> wig)
{
  auto l1 = 100;
  auto l2 = 200;
  auto l3 = 300;
  auto expected = 21.03700730573995;
  auto almosteq
      = almost_eq (geometry_reduced_Gaunt (l1, l2, l3, wig), expected);
  if (almosteq == false)
    {
      std::cout << std::setprecision (16)
                << "Computed: " << geometry_reduced_Gaunt (l1, l2, l3, wig)
                << std::endl
                << "Expected: " << expected << std::endl;
    }
  assert (almosteq);
}

void
test_geometry_Q (std::shared_ptr<Wigner> wig)
{
  auto Q = geometry_Q (10, 20, 30, 1, 19, 11, wig);
  auto expected = 0.3298677898485691;
  auto almosteq = almost_eq (Q, expected);
  if (almosteq == false)
    {
      std::cout << std::setprecision (16) << "Computed: " << Q << std::endl
                << "Expected: " << expected << std::endl;
    }
  assert (almosteq);
}

bool
almosteq_spectra (std::complex<double> computed, double expected)
{
  // Imaginary part of 'expected' is zero, tutils will only use
  // absolute tolerance.
  auto acr = fabs (computed.real ());
  auto abstol = acr > 0 ? 1e-4 * acr : 1e-8;
  auto aeq = almost_eq (computed, expected, abstol);
  std::cout << "Computed    Expected" << std::endl;
  if (!aeq)
    std::cout << std::setprecision (16) << computed << "    " << expected
              << std::endl;
  return aeq;
}

void
test_general_spectra_at (GeneralSpectra gsp,
                         GeneralSpectra::TransferType ttype)
{
  auto n = 0;
  auto l1 = 20;
  auto l2 = 22;
  std::vector<std::vector<double> > zgrid = { { 0.50, 0.50 }, { 0.49, 0.50 } };
  std::vector<double> expected (2);
  switch (ttype)
    {
    case GeneralSpectra::kDensity:
      expected = { 0.0001317366132985237, -8.825147786058054e-05 };
      break;
    case GeneralSpectra::kDensity1:
      expected = { 1013.836068474934, -8.633073402810034 };
      break;
    case GeneralSpectra::kVelocity0:
      expected = { 7.567924331824456e-05, -5.091703917328229e-05 };
      break;
    case GeneralSpectra::kVelocity:
      expected = { 1.018661173646683e-09, -1.354954469700643e-09 };
      break;
    case GeneralSpectra::kVelocity1:
      expected = { 8.052007500435927e-05, -2.862571299984728e-05 };
      break;
    case GeneralSpectra::kVelocity2:
      expected = { 581.8093517044874, -6.897818360368078 };
      break;
    case GeneralSpectra::kDensityG_kVelocity1:
      expected = { 0.000432989167300936, -0.0002036520795141233 };
      break;
    case GeneralSpectra::kLensing:
      expected = { -5.205493635358257e-08, -5.307491720006993e-08 };
      break;
    }

  auto z1 = 0.50;
  auto z2 = 0.51;

  // Same redshifts.
  auto computed = gsp.compute (l1, l2, z1, z1, n, ttype, ttype, 1e-3);
  auto aeq = almosteq_spectra (computed, expected.at (0));
  assert (aeq);

  // Different redshifts (i.e., different integration method).
  computed = gsp.compute (l1, l2, z1, z2, n, ttype, ttype, 1e-3);
  aeq = almosteq_spectra (computed, expected.at (1));
  assert (aeq);
}

void
test_general_spectra ()
{
  std::string datadir = DATADIR; // DATADIR from Automake.
  auto inifile = datadir + "test.ini";
  auto pt
      = cosmox::Perturbations::create_perturbs (cosmox::kCosmoclass, inifile);
  GeneralSpectra gsp (pt);
  test_general_spectra_at (gsp, GeneralSpectra::kDensity);
  test_general_spectra_at (gsp, GeneralSpectra::kDensity1);
  test_general_spectra_at (gsp, GeneralSpectra::kVelocity0);
  test_general_spectra_at (gsp, GeneralSpectra::kVelocity);
  test_general_spectra_at (gsp, GeneralSpectra::kVelocity1);
  test_general_spectra_at (gsp, GeneralSpectra::kVelocity2);
  test_general_spectra_at (gsp, GeneralSpectra::kDensityG_kVelocity1);
  test_general_spectra_at (gsp, GeneralSpectra::kLensing);
}

class TestGalaxyBias : public GalaxyBias
{
  double
  b1 (double z) override
  {
    return sqrt (1 + z);
  }
};

void
test_general_spectra_bias ()
{
  std::string datadir = DATADIR; // DATADIR from Automake.
  auto inifile = datadir + "test.ini";
  auto pt
      = cosmox::Perturbations::create_perturbs (cosmox::kCosmoclass, inifile);
  auto bias = std::make_shared<TestGalaxyBias> ();
  GeneralSpectra gsp (pt, bias);
  auto n = 0;
  auto l1 = 10;
  auto l2 = 10;
  auto z = 0.50;
  auto computed = gsp.compute (l1, l2, z, z, n, GeneralSpectra::kDensity,
                               GeneralSpectra::kDensityG_kVelocity1);
  auto expected = 0.000470115;
  auto aeq = almosteq_spectra (computed, expected);
  assert (aeq);
}

class WindowSpec : public cosmox::Window
{
public:
  using Window::Window;

  double
  eval (const double z)
  {
    return 1 / (get_zmax () - get_zmin ());
  }
};

bool
in_ell_selection (const int ell, const double half_w_width)
{
  bool in_selection;
  if (almost_eq (half_w_width, 0))
    {
      in_selection = ((ell % 20 == 0) && (ell <= 1000));
    }
  else
    {
      in_selection = ((ell == 20) || ((ell % 100 == 0) && (ell <= 500)));
    }
  return in_selection;
}

} /* namespace cosmox */

int
main ()
{
  auto wig = std::make_shared<cosmox::Wigner> (1000);
  cosmox::test_geometry_A (wig);
  cosmox::test_geometry_reduced_Gaunt (wig);
  cosmox::test_geometry_Q (wig);
  cosmox::test_general_spectra ();
  cosmox::test_general_spectra_bias ();
}
