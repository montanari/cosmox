// test_special_functions.cc -- Check special functions.

// Copyright 2017, 2018, 2019 Francesco Montanari

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "lib/numeric.h"
#include "tutils.h"


namespace cosmox {
  void
  test_GslInterpCspline()
  {
    int n = 10;
    double x[n];
    double y[n];
    for (auto i = 0; i < n; ++i)
      {
        x[i] = i + 0.5 * sin (i);
        y[i] = i + cos (i * i);
      }

    /* Interpolate.  */
    GslInterpCspline interp(x, y, n);
    double xi;
    double yi;
    double yi_expected[] = {1.0, 1.54101, 1.30493, 1.99057, 4.30892,
                            6.399, 5.87446, 6.89748, 7.85726, 9.33011};
    auto i=0;
    for (xi = x[0]; xi < x[9]; xi += 1.0)
      {
        yi = interp.eval (xi);
        almost_eq(yi, yi_expected[i]);
        ++i;
      }
  }

  void
  test_GslInterp2d()
  {
    const double xa[] = {0.0, 1.0}; /* Grid: unit square */
    const double ya[] = {0.0, 1.0};
    const size_t nx = sizeof(xa) / sizeof(double); /* x grid points */
    const size_t ny = sizeof(ya) / sizeof(double); /* y grid points */
    const double za_xy[nx * ny] = {0.0, 1.0,       /* Interpolated function */
                                   1.0, 0.5};

    /* Interpolate.  */
    cosmox::GslInterp2d interp2d(xa, ya, za_xy, nx, ny);

    /* Grid of values to be interpolated. */
    const size_t ng = 3;
    const double xi[ng] = {0.0, 0.5, 1.0};
    const double yj[ng] = {0.0, 0.5, 1.0};
    const double zanswer[ng][ng] = {{0.0, 0.5, 1.0},
                                    {0.5, 0.625, 0.75},
                                    {1.0, 0.75, 0.5}};

    size_t i, j;
    for(i=0; i<ng; ++i)
      for(j=0; j<ng; ++j)
        {
          almost_eq(interp2d.eval(xi[i], yj[j]),
                    zanswer[i][j]);
        }
  }

  struct IntegrandData {
    double alpha = 1.0;
  };

  double
  integrand (double x, void* userdata)
  {
    IntegrandData* data = static_cast<IntegrandData*>(userdata);
    double f = log(data->alpha*x) / sqrt(x);
    return f;
  }

  void
  test_GslIntegrator_qag()
  {
    double expected = -4.0;     // \int_0^1 x^{-1/2} log(x) dx = -4
    GslIntegrator integrator;
    IntegrandData data;
    auto result = integrator.qag(&integrand, 0.0, 1.0, &data);
    assert(almost_eq(result.integral, expected));
  }


  void
  test_GslIntegratorCquad()
  {
    double expected = -4.0;     // \int_0^1 x^{-1/2} log(x) dx = -4
    GslIntegratorCquad integrator;
    IntegrandData data;
    auto result = integrator.cquad(&integrand, 0.0, 1.0, &data);
    assert(almost_eq(result.integral, expected));
  }

  void test_trilinear_interp()
  {
    auto x_grid = linspace(1., 4., 11);
    auto y_grid = linspace(4., 7., 22);
    auto z_grid = linspace(7., 9., 33);

    auto f = [](double x, double y, double z){return 2 * x*x*x + 3 * y*y - z;};

    cosmox::vector3d<double> data(x_grid.size(), y_grid.size(), z_grid.size());
    for (size_t i=0; i<x_grid.size(); ++i)
      for (size_t j=0; j<y_grid.size(); ++j)
        for (size_t k=0; k<z_grid.size(); ++k)
          data(i, j, k) = f(x_grid.at(i), y_grid.at(j), z_grid.at(k));

    auto interp = cosmox::TrilinearInterp(x_grid, y_grid, z_grid, data);

    auto abstol = 0.;

    // Edge cases.
    auto reltol = 1e-8;
    assert(almost_eq(interp.eval(x_grid.at(0), y_grid.at(0), z_grid.at(0)),
                     f(x_grid.at(0), y_grid.at(0), z_grid.at(0)),
                     abstol, reltol));
    assert(almost_eq(interp.eval(x_grid.back(), y_grid.back(), z_grid.back()),
                     f(x_grid.back(), y_grid.back(), z_grid.back()),
                     abstol, reltol));
    // Intermediate points.
    reltol = 1e-2;
    assert(almost_eq(interp.eval(2.1, 6.2, 8.3), f(2.1, 6.2, 8.3),
                     abstol, reltol));
    assert(almost_eq(interp.eval(3.3, 5.2, 7.1), f(3.3, 5.2, 7.1),
                     abstol, reltol));
  }
} // namespace cosmox

int
main()
{
  using namespace cosmox;

  test_GslInterpCspline();
  test_GslInterp2d();
  test_GslIntegrator_qag();
  test_GslIntegratorCquad();
  test_trilinear_interp();

  return 0;
}
