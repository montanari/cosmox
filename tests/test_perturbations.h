// test_perturbations.h -- Check perturbations module.

// Copyright 2017, 2018, 2019 Francesco Montanari

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef TEST_PERTURBATIONS_H
#define TEST_PERTURBATIONS_H

#include <memory>
#include <string>
#include <utility>

#include "lib/perturbations.h"
#include "tutils.h"

namespace cosmox {

  /** Test public members of PerturbsCosmoclass.
   */
  class TestPerturbsCosmoclass {
  public:
    TestPerturbsCosmoclass(std::string inifile);
    /** Compute and check source functions on the testing wave-number
     *  and redshift grids.  */
    void get_source_delta_m();
    void get_source_theta_m();
    /** Check primordial P(k) on the testing wave-number grid.  */
    void get_primordial_spectrum();
    void get_k_lims();
    /* Background quantities. */
    void bg_comoving_distance();
    void bg_Hubble();
    void bg_velocity_growth_factor();

  private:
    static bool compare_vector(std::vector<double> computed,
                               std::vector<double> expected);
    bool compare_source(std::vector<double> expected,
                        Perturbations::SourceType stype);

    std::shared_ptr<Perturbations> perturbs;

    const std::vector<double> kgrid {0.001, 0.1, 1.1826};
    const std::vector<double> zgrid {0.0, 1.0, 1000.0};
  };


} // namespace cosmox

#endif  /* PERTURBATIONS_H */
