// test_cuba.h -- Check Cuba interface for multidimensional integration.

// Copyright 2017, 2018, 2019 Francesco Montanari

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef TEST_CUBA_H
#define TEST_CUBA_H

#include <iostream>
#include <limits>
#include <utility>

#include "lib/cubapp.h"
#include "tutils.h"


namespace cuba {

  struct Userdata {
    double two = 2.0;
  };


  class TestCuba {
  public:
    TestCuba();
    void test_vegas_3d();       /**< 3-dim, 2 components. */
    void test_suave_3d();
    void test_divonne_3d();
    void test_cuhre_3d();
    void test_cuhre_3d_2();    /**< Use a second Cuba istance.*/
    void test_cuhre_2d();      /**<  2-dim, 1 component, user data. */
    void test_3d_inf();           /**<  3-dim, (semi-)infinite bounds. */
  private:
    static std::vector<double> integrand_2d(std::vector<double> xx,
                                            void* userdata);
    static std::vector<double> integrand_3d(std::vector<double> xx,
                                            void* userdata);
    static std::vector<double> integrand_3d_inf(std::vector<double> xx,
                                                void* userdata);
    bool get_almost_eq(std::vector<double> computed,
                       std::vector<double> expected,
                       double abstol, double reltol);

    Cuba integrator_2d;
    Cuba integrator_3d;
    Cuba integrator_3d_2;
    Cuba integrator_3d_inf;

    /* As non-members, bounds are more easily declared as:
     *
     * auto bounds_2d = {std::make_pair(-0.5, 2.0)};
     */
    std::vector<std::pair<double, double>>
      bounds_2d = {std::make_pair(-0.5, 2.0),
                   std::make_pair(0., 1.0)};

    std::vector<std::pair<double, double>>
      bounds_3d = {std::make_pair(-0.5, 0.),
                   std::make_pair(0.0, 0.5),
                   std::make_pair(0.0, 0.9)};

    double inf = std::numeric_limits<double>::infinity();
    std::vector<std::pair<double, double>>
      bounds_3d_inf = {std::make_pair(inf, 0.5),
                       std::make_pair(1.5, -inf),
                       std::make_pair(inf, -inf)};

    Userdata data;

    /* Maxima:
     * float(2.0 *integrate(sin(x), x, -0.5, 2.0)
     *       * integrate(exp(y), y, 0.0, 1.0));
     */
    std::vector<double> expected_2d = {1.293729398437516};

    /* Maxima, first component:
     * float(integrate(sin(x), x, -0.5, 0.0) * integrate(cos(y), y, 0.0, 0.5)
     *       * integrate(z, z, 0.0, 0.9));
     *
     * Second component computed numerically with another code.
     *
     */
    std::vector<double> expected_3d = {-0.08566417402783636, 0.219058};

    /* Maxima:
     * float(integrate(1/(1+(x+1)^2), x, inf, 0.5)
     *       * integrate(1/(1+(x+1)^2), x, 1.5, -inf)
     *       * integrate(exp(-x^2), x, inf, -inf));
     * */
    std::vector<double> expected_3d_inf = {-2.877624767510018};
  };

} // namespace cuba

#endif  /* TEST_CUBA_H */
