// tutils.cc -- Testing utilities.

// Copyright 2017, 2018, 2019 Francesco Montanari

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "tutils.h"

namespace cosmox {

  bool
  almost_eq(const double x1, const double x2,
            const double abstol, const double reltol)
  {
    if ((abstol < 0) or (reltol < 0))
      throw std::invalid_argument("abstol and reltol must be "
                                  "non-negative.");

    // To determine the relative difference divide by x2. If
    // it is zero, always use absolute precision.
    if ((reltol >= abstol) and (x2 != 0))
      {
        if ((fabs(1. - x1/x2) <= reltol)) // Here division is well defined.
          {
            return true;
          }
      }
    else if ((fabs(x1-x2) <= abstol))
      {
        return true;
      }

    return false;
  }


  bool
  almost_eq(std::complex<double> z1, std::complex<double> z2,
            double abstol, double reltol)
  {
    double re_z1 = z1.real();
    double im_z1 = z1.imag();
    double re_z2 = z2.real();
    double im_z2 = z2.imag();

    if ((abstol < 0) or (reltol < 0))
      throw std::invalid_argument("abstol and reltol must be "
                                  "non-negative.");

    // To determine the relative difference divide by Re(z2), Im(z2). If
    // one of them is zero, always use absolute precision.
    if ((reltol >= abstol)
        and (re_z2 != 0) and (im_z2 != 0))
      {
        // If we arrive here, the division is well defined.
        if ((fabs(1. - re_z1/re_z2) <= reltol)
            and (fabs(1. - im_z1/im_z2) <= reltol))
          {
            return true;
          }
      }
    else if ((fabs(re_z1-re_z2) <= abstol)
             and (fabs(im_z1-im_z2) <= abstol))
      {
        return true;
      }

    return false;
  }


  bool
  almost_eq(const std::valarray<double> x1, const std::valarray<double> x2,
            const double abstol, const double reltol)
  {
    if ((abstol < 0) or (reltol < 0))
      throw std::invalid_argument("abstol and reltol must be "
                                  "non-negative.");
    if (x1.size() != x2.size())
      return false;

    // To determine the relative difference divide by x2 if all of its
    // elements are non-zero. Otherwise, use absolute precision.
    if ((reltol >= abstol) and
        (std::all_of(std::begin(x2), std::end(x2),
                     [](double i){return i != 0;})))
      {
        std::valarray<bool> releq = (sqrt(pow(1. - x1/x2, 2.)) <= reltol);
        if (std::all_of(begin(releq), end(releq), [](bool i){return i;}))
          {
            return true;
          }
      }
    else
      {
        std::valarray<bool> abseq = (sqrt(pow(x1-x2, 2.)) <= abstol);
        if (std::all_of(begin(abseq), end(abseq), [](bool i){return i;}))
          return true;
      }

    return false;
  }


  bool
  almost_eq(const std::vector<double> x1, const std::vector<double> x2,
            const double abstol, const double reltol)
  {
    if ((abstol < 0) or (reltol < 0))
      throw std::invalid_argument("abstol and reltol must be "
                                  "non-negative.");
    if (x1.size() != x2.size())
      return false;

    // To determine the relative difference divide by x2 if all of its
    // elements are non-zero. Otherwise, use absolute precision.
    if ((reltol >= abstol) and
        (std::all_of(std::begin(x2), std::end(x2),
                     [](double i){return i != 0;})))
      {
        std::valarray<bool> releq(x2.size());
        for (size_t i=0; i<releq.size(); ++i)
          releq = (fabs(1. - x1[i]/x2[i]) <= reltol);
        if (std::all_of(begin(releq), end(releq), [](bool i){return i;}))
          {
            return true;
          }}
    else
      {
        std::valarray<bool> abseq(x2.size());
        for (size_t i=0; i<abseq.size(); ++i)
          {
            abseq[i] = (fabs(x1[i]-x2[i]) <= abstol);
          }
        if (std::all_of(begin(abseq), end(abseq), [](bool i){return i;}))
          return true;
      }

    return false;
  }



} /* namespace cosmox */
