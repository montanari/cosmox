// test_bispectrum.cc -- Check bispectrum.

// Copyright 2017, 2018, 2019 Francesco Montanari

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "lib/bispectrum.h"
#include "lib/survey_specs.h"
#include "tutils.h"

namespace cosmox {
  void
  test_bispectrum_rsd ()
  {
    std::string datadir = DATADIR; // DATADIR from Automake.
    auto inifile = datadir + "test.ini";
    auto pt = cosmox::Perturbations::create_perturbs (cosmox::kCosmoclass,
                                                      inifile);
    auto galbias = std::make_shared<GalaxyBias>();
    auto linear_rsd = true;
    auto epsrel = 1e-3;
    auto epsabs = 0.;
    auto maxeval = 1e4;
    auto ignore_fail = true;
    BispectrumRsd rsd(pt, galbias, linear_rsd, epsrel, epsabs,
                      maxeval, ignore_fail);
    std::vector<int> ls = {100, 200, 300};
    std::vector<double> zgrid = {0.1, 0.5};
    std::vector<double> expected = {1.29402e-07, 2.37462e-08};

    int i=0;
    for (auto &z: zgrid)
      {
        double abstol = 1e-10;
        double reltol = 1e-3;
        bool aeq = almost_eq(rsd.get_nonseparable(ls[0], ls[1], ls[2],
                                                  z, z, z),
                             expected[i], abstol, reltol);
        std::cout << rsd.get_nonseparable(ls[0], ls[1], ls[2],
                                          z, z, z) << " "
                  << expected[i] << "\n";
        assert(aeq);
        ++i;
      }
  }
} /* namespace cosmox */

int
main()
{
  cosmox::test_bispectrum_rsd ();
  return EXIT_SUCCESS;
}
