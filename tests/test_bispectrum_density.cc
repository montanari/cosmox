// test_bispectrum_density.cc -- Check density bispectrum.

// Copyright 2017, 2018 Francesco Montanari

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <iomanip>
#include <omp.h>
#include <sstream>

#include "lib/bispectrum_density.h"
#include "lib/cosmox_exception.h"
#include "lib/general_spectra.h"
#include "lib/perturbations.h"
#include "lib/survey_specs.h"
#include "tutils.h"

namespace cosmox {
  class EuclidBias: public GalaxyBias {
  public:
    double
    b1 (double z)
    {
      check_range(z);
      return 0.4*z + 0.9;
    }

    double
    b2 (double z)
    {
      check_range(z);
      return (-0.00771288*pow(z, 3.) + 0.18302286*pow(z, 2.) - 0.20799274*z -
              0.70417186);
    }

    double
    bS (double z)
    {
      return -2./7.*(b1(z)-1.);
    }

  private:
    void check_range(double z)
    {
      if ((z<0.7) || (z>2.0))
        throw CosmoxException("Galaxy bias out of range",
                              __FILE__, __LINE__);
    }
  };


  /* @note If BispectrumDensity.compute() succeeds, then
   *       BispectrumDensity.compute_reduced() is also ok. Then,
   *       because it is time-consuming, we can avoid to compute
   *       directly the latter.
   */
  void
  test_bispectrum(const std::shared_ptr<BispectrumDensity> bdens)
  {
    auto abstol = 0.;
    auto reltol = 1e-4;
    std::vector<std::vector<int>> lgrid = {{10, 10, 10},
                                           {11, 11, 10}};
    std::vector<std::vector<double>> zgrid = {{1., 1., 1.},
                                              {1.00, 1.01, 1.02}};
    std::vector<std::vector<double>> expected = {{-8.72905e-08, 8.3647e-10},
                                                 {8.94527e-08, 4.2886e-10}};
    std::cout << "# (l1 l2 l3) (z1 z2 z3) computed expected" << std::endl;

    auto aeq = true;
    std::stringstream stream;   // Avoid omp critical(std::cout)
#pragma omp parallel for collapse(2)
    for (size_t i=0; i<lgrid.size(); ++i)
      for (size_t j=0; j<zgrid.size(); ++j)
        {
          auto ls = lgrid.at(i);
          auto zs = zgrid.at(j);
          auto computed = bdens->compute(ls.at(0), ls.at(1), ls.at(2),
                                         zs.at(0), zs.at(1), zs.at(2));
          stream << "(" << ls.at(0) << " " << ls.at(1) << " " << ls.at(2)
                 << ") ("
                 << zs.at(0) << " " << zs.at(1) << " " << zs.at(2)
                 << ") "
                 << computed << " " << expected.at(i).at(j) << "\n";
          aeq *= almost_eq(computed, expected.at(i).at(j), abstol, reltol);
        }
    std::cout << stream.str() << std::flush;
    assert(aeq);
  }

  void
  test_variance(const std::shared_ptr<BispectrumDensity> bdens)
  {
    auto abstol = 0.;
    auto reltol = 1e-4;
    std::vector<std::vector<int>> lgrid = {{10, 10, 10},
                                           {11, 11, 10}};
    double z = 1.0;
    std::vector<double> expected = {7.06792e-11, 2.3385e-11};
    std::cout << "# (l1 l2 l3) computed expected" << std::endl;

    auto aeq = true;
    std::stringstream stream;   // Avoid omp critical(std::cout)
#pragma omp parallel for
    for (size_t i=0; i<lgrid.size(); ++i)
      {
        auto ls = lgrid.at(i);
        auto shotnoise = 0;
        auto computed = bdens->compute_variance(ls.at(0), ls.at(1), ls.at(2),
                                                z, z, z, shotnoise);
        stream << "(" << ls.at(0) << " " << ls.at(1) << " " << ls.at(2)
               << ") "
               << computed << " " << expected.at(i) << "\n";
        aeq *= almost_eq(computed, expected.at(i), abstol, reltol);
      }
    std::cout << stream.str() << std::flush;
    assert(aeq);
  }

  class WindowSpec: public cosmox::Window {
  public:
    using Window::Window;

    double
    eval(const double z)
    {
      return 1/(get_zmax()-get_zmin());
    }
  };

  /* @note If BispectrumDensity.compute() succeeds, then
   *       BispectrumDensity.compute_reduced() is also ok.
   */
  void
  test_bispectrum_win(const std::shared_ptr<BispectrumDensityW> bdens)
  {
    auto abstol = 0.;
    auto reltol = 1e-4;
    auto win = std::make_shared<WindowSpec>(0.8, 0.81);
    auto ell = 10;
    auto expected = 5.9542e-08;
    assert(almost_eq(bdens->compute(ell, ell, 2*ell,
                                win, win, win),
                     expected,
                     abstol, reltol));
  }

  void
  test_variance_win_helper (const std::shared_ptr<GeneralSpectra> gsp,
                            const std::shared_ptr<Wigner> wig, const int l1,
                            const int l2, const int l3, const bool linear_rsd)
  {
    auto epsrel = 1e-3;
    auto zz = 1.0;
    auto win = std::make_shared<WindowSpec> (zz, zz + 1e-4);
    auto abstol = 0.;
    auto reltol = 0.1;
    auto bdens_w = BispectrumDensityW (gsp, wig, linear_rsd, epsrel);
    auto bdens = BispectrumDensity (gsp, wig, linear_rsd, epsrel);
    auto computed = bdens_w.compute_variance (l1, l2, l3, win, win, win);
    auto expected = bdens.compute_variance (l1, l2, l3, zz, zz, zz);
    auto aeq = almost_eq (computed, expected, abstol, reltol);
    if (!aeq)
      {
        std::cout << std::boolalpha;
        std::cout << "l1, l2, l3 = " << l1 << ", " << l2 << ", " << l3 << "\n"
                  << "linear_rsd = " << linear_rsd << "\n"
                  << "  expected: " << expected << "\n"
                  << "  computed: " << computed << std::endl;
      }
    assert (aeq);
  }

  /** Check whether integration over small window converges to no
      window result.
  */
  void
  test_variance_win (const std::shared_ptr<GeneralSpectra> gsp,
                     const std::shared_ptr<Wigner> wig)
  {
    std::vector<std::vector<int> > triangles{ { 100, 127, 165 },
                                              { 84, 195, 195 },
                                              { 100, 100, 100 } };
    auto linear_rsd = false;
    for (auto const &t : triangles)
      test_variance_win_helper (gsp, wig, t.at (0), t.at (1), t.at (2),
                                linear_rsd);
    linear_rsd = true;
    for (auto const &t : triangles)
      test_variance_win_helper (gsp, wig, t.at (0), t.at (1), t.at (2),
                                linear_rsd);
  }

} /* namespace cosmox */

int
main()
{
  std::string datadir = DATADIR; // DATADIR from Automake.
  auto inifile = datadir + "test.ini";
  auto pt = cosmox::Perturbations::create_perturbs(cosmox::kCosmoclass,
                                                   inifile);
  auto galbias = std::make_shared<cosmox::EuclidBias>();
  auto gsp = std::make_shared<cosmox::GeneralSpectra>(pt, galbias);
  auto wig = std::make_shared<cosmox::Wigner>(1000);
  auto linear_rsd = true;
  auto epsrel = 1e-3;
  auto bdens = std::make_shared<cosmox::BispectrumDensity>(gsp, wig,
                                                           linear_rsd,
                                                           epsrel);
  cosmox::test_bispectrum(bdens);
  cosmox::test_variance(bdens);

  linear_rsd = false;           // linear_rsd is time consuming
  epsrel = 1e-1;
  auto bdens_w = std::make_shared<cosmox::BispectrumDensityW>(gsp, wig,
                                                              linear_rsd,
                                                              epsrel);
  cosmox::test_bispectrum_win(bdens_w);
  cosmox::test_variance_win(gsp, wig);

  return EXIT_SUCCESS;
}
