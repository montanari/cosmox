// tutils.h -- Testing utilities.

// Copyright 2017, 2018, 2019 Francesco Montanari

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef TUTILS_H
#define TUTILS_H

#include <assert.h>
#include <cmath>
#include <complex>
#include <cstdio>
#include <iomanip>
#include <iostream>
#include <valarray>
#include <vector>

namespace cosmox {

  /**
   * @brief Determine if two numbers are almost equal.
   *
   * Return true if x1 and x2 are almost equal, within the given
   * tolerance. The largest among the relative and absolute tolerance
   * is considered. If the relative difference is infinite (i.e., x2
   * is zero), the absolute tolerance is considered.
   */
  bool almost_eq(const double x1, const double x2,
                 const double abstol=1e-8, const double reltol=1e-8);

  /**
   * @brief Determine if two complex numbers are almost equal.
   *
   * Return true if the real and imaginary parts of z1 and z2 are almost
   * equal, within the given tolerance. The largest among the relative
   * and absolute tolerance is considered. If the relative difference is
   * infinite (i.e., z2 is zero), the absolute tolerance is
   * considered.
   */
  bool almost_eq(std::complex<double> z1, std::complex<double> z2,
                 double abstol=1e-8, double reltol=1e-8);

  /**
   * @brief Determine if two valarrays are almost equal.
   *
   * Return true if all elements pairs x1[i] and x2[i] are almost equal,
   * within the given tolerance. The largest among the relative and
   * absolute tolerance is considered. If the relative difference of any
   * pair is infinite (i.e., x2[i] is zero), the absolute tolerance
   * is considered.
   *
   * \note The absolute magnitude of each array value v[i] is computed
   *       applying sqrt(v^2) to the array. This can be optimized. This
   *       whole function could actually be replaced by a template in
   *       common, e.g., with vectors comparison.
   */
  bool almost_eq(const std::valarray<double> x1, const std::valarray<double> x2,
                 const double abstol=1e-8, const double reltol=1e-8);

  /**
   * @brief Determine if two vectors are almost equal.
   *
   * Return true if all elements pairs x1[i] and x2[i] are almost equal,
   * within the given tolerance. The largest among the relative and
   * absolute tolerance is considered. If the relative difference of any
   * pair is infinite (i.e., x2[i] is zero), the absolute tolerance
   * is considered.
   */
  bool almost_eq(const std::vector<double> x1, const std::vector<double> x2,
                 const double abstol=1e-8, const double reltol=1e-8);

} /* namespace cosmox */

#endif  /* TUTILS_H */
