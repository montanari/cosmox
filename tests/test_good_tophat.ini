# test_good_tophat.ini -- Cosmoclass configuration file for good
# precision, with window function.

# Copyright 2020 Francesco Montanari

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Commentary:
#
# This cosmoclass configuration file is prepared to compare the redshift space
# harmonic power spectrum directly with the cosmoclass code output, using good
# precision parameters. Cosmoclass output file was obtained running CLASS
# v2.6.3:
#
#   /path/to/class test_good_tophat.ini

A_s = 2.3e-9
h = 0.67
n_s = 0.962
alpha_s = 0.
Omega_b = 0.05
Omega_cdm = 0.27

output = nCl
number count contributions = density, rsd
selection = tophat
selection_mean = 1.0
selection_width = 0.01
non_diagonal = 0
modes = s
ic = ad

l_switch_limber_for_nc_local_over_z = 10000.
l_switch_limber_for_nc_los_over_z = 10000.
selection_sampling_bessel = 20
k_max_tau0_over_l_max = 15
q_linstep = 1.0
l_max_lss = 1000

root = test_cosmoclass_tophat_
write parameters = n
