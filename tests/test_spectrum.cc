// test_spectrum.cc -- Check angular power spectrum.

// Copyright 2020 Francesco Montanari

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// Commentary:

// This is an important test set, because we compare a wrapper to
// cosmox::GeneralSpectra against the reliable cosmoclass output.
// cosmox::GeneralSpectra are the building blocks of the bispectrum,
// so it is important to assure success of this test set.
//
// TODO: The computation of spectra at different multipoles should be
// parallelized because it is time consuming.

#include <fstream>

#include "lib/spectrum.h"
#include "lib/survey_specs.h"
#include "tutils.h"

namespace cosmox
{

class WindowSpec : public cosmox::Window
{
public:
  using Window::Window;

  double
  eval (const double z)
  {
    return 1 / (get_zmax () - get_zmin ());
  }
};

bool
in_ell_selection (const int ell, const double half_w_width)
{
  bool in_selection;
  if (almost_eq (half_w_width, 0))
    {
      in_selection = ((ell % 20 == 0) && (ell <= 1000));
    }
  else
    {
      in_selection = ((ell == 20) || ((ell % 100 == 0) && (ell <= 500)));
    }
  return in_selection;
}

double
dispatch_sp_compute (const std::shared_ptr<Spectrum> sp, const int ell,
                     const double z, const double half_w_width,
                     const double epsrel)
{
  auto epsabs = 0;
  double cosmox_cl;
  if (almost_eq (half_w_width, 0))
    {
      cosmox_cl
          = sp->compute (ell, z, z, GeneralSpectra::kDensityG_kVelocity1,
                         GeneralSpectra::kDensityG_kVelocity1, epsrel, epsabs);
    }
  else
    {
      auto win
          = std::make_shared<WindowSpec> (z - half_w_width, z + half_w_width);

      cosmox_cl
          = sp->compute (ell, win, win, GeneralSpectra::kDensityG_kVelocity1,
                         GeneralSpectra::kDensityG_kVelocity1, epsrel, epsabs);
    }
  return cosmox_cl;
}

void
test_spectrum_helper (const std::string inifile, const std::string clfile,
                      const double z, const double half_w_width,
                      const double epsrel, const double reltol)
{
  std::string datadir = DATADIR;
  auto pt = Perturbations::create_perturbs (cosmox::kCosmoclass,
                                            datadir + inifile);
  auto sp = std::make_shared<Spectrum> (pt);

  std::ifstream datafile (datadir + clfile);
  if (datafile.fail ())
    throw CosmoxException ("Failed to open file", __FILE__, __LINE__);
  if (half_w_width < 0)
    throw CosmoxException ("Got negative redshift window width", __FILE__,
                           __LINE__);
  std::string line;
  auto aeq = true;
  while (std::getline (datafile, line))
    {
      if (line.rfind ("#", 0) == 0)
        continue;
      std::istringstream iss (line);
      int ell;
      double cl;
      if (!(iss >> ell >> cl))
        throw CosmoxException ("Error parsing file", __FILE__, __LINE__);
      if (in_ell_selection (ell, half_w_width))
        {
          auto norm = ell * (ell + 1) / 2 / M_PI;
          auto cosmox_cl
              = norm * dispatch_sp_compute (sp, ell, z, half_w_width, epsrel);
          auto abstol = 0;
          auto line_aeq = almost_eq (cl, cosmox_cl, abstol, reltol);
          if (!line_aeq)
            std::cout << ell << " " << cl << " " << cosmox_cl << std::endl;
          aeq *= line_aeq;
        }
    }
  assert (aeq);
}

/** Test angular power spectrum. Here we compare directly against
    the results obtained with cosmoclass, therefore we want
    excellent agreement < 0.1%.
*/
void
test_spectrum ()
{
  auto z = 0.1;
  auto half_w_width = 0;
  auto epsrel = 1e-3;
  auto reltol = 1e-3;
  test_spectrum_helper ("test_good.ini", "test_good_cl.dat", z, half_w_width,
                        epsrel, reltol);
}

/** Test angular power spectrum with window function. Also here we
    compare against results obtained with cosmoclass, but we aim at
    a modest < 10% agreement due to sub-optimal precision parameters
    in Cosmox::Spectrum::compute when using window functions.
 */
void
test_spectrum_w ()
{
  auto z = 1;
  auto half_w_width = 0.01;
  auto epsrel = 1e-1;
  auto reltol = 1e-1;
  test_spectrum_helper ("test_good_tophat.ini", "test_good_tophat_cl.dat", z,
                        half_w_width, epsrel, reltol);
}
} /* namespace cosmox */

int
main ()
{
  cosmox::test_spectrum ();
  cosmox::test_spectrum_w ();
}
