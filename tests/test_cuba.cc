// test_cuba.cc -- Check Cuba library interface.

// Copyright 2017, 2018, 2019 Francesco Montanari

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "tests/test_cuba.h"

namespace cuba {

  std::vector<double>
  TestCuba::integrand_2d(std::vector<double> xx, void* userdata)
  {
    std::vector<double> func(1);
    double x = xx.at(0);
    double y = xx.at(1);
    Userdata* data = static_cast<Userdata*>(userdata);
    func[0] = data->two * sin(x) * y;
    // double* data = static_cast<double*>(userdata);
    // func[0] = (*data) * sin(x) * y;
    return func;
  }


  std::vector<double>
  TestCuba::integrand_3d(std::vector<double> xx, void* userdata)
  {
    std::vector<double> func(2);
    double x = xx.at(0);
    double y = xx.at(1);
    double z = xx.at(2);

    func[0] = sin(x)*cos(y)*exp(z);
    func[1] = 1./(1. - x*y*z);

    return func;
  }


  std::vector<double>
  TestCuba::integrand_3d_inf(std::vector<double> xx, void* userdata)
  {
    std::vector<double> func(1);
    double x = xx.at(0);
    double y = xx.at(1);
    double z = xx.at(2);
    func[0] = (1. / (1. + pow(x+1.,2.))
               * 1. / (1. + pow(y+1.,2.))
               * exp(-z*z));
    return func;
  }


  TestCuba::TestCuba()
  {
    integrator_3d.maxeval = 500000;
    integrator_3d_2.maxeval = 500000;
  }


  bool
  TestCuba::get_almost_eq(std::vector<double> computed,
                             std::vector<double> expected,
                             double abstol,
                             double reltol)
  {
    bool almosteq = cosmox::almost_eq(computed, expected, abstol, reltol);
    if (!almosteq)
      {
        std::cerr << "Expected    Computed:" << std::endl;
          for (size_t i=0; i<computed.size(); ++i)
            {
              std::cerr << expected[i] << "    " << computed[i]
                        << std::endl;
            }
      }
    return almosteq;
  }


  void
  TestCuba::test_vegas_3d()
  {
    auto result = integrator_3d.vegas(&integrand_3d, bounds_3d, nullptr);
    bool almosteq = get_almost_eq(result.integral, expected_3d,
                                  integrator_3d.epsabs,
                                  integrator_3d.epsrel);
    assert(almosteq);
  }


  void
  TestCuba::test_suave_3d()
  {
    auto result = integrator_3d.suave(&integrand_3d, bounds_3d, nullptr);
    bool almosteq = get_almost_eq(result.integral, expected_3d,
                                  integrator_3d.epsabs,
                                  integrator_3d.epsrel);
    assert(almosteq);
  }


  void
  TestCuba::test_divonne_3d()
  {
    auto result = integrator_3d.divonne(&integrand_3d, bounds_3d, nullptr);
    bool almosteq = get_almost_eq(result.integral, expected_3d,
                                  integrator_3d.epsabs,
                                  integrator_3d.epsrel);
    assert(almosteq);
  }


  void
  TestCuba::test_cuhre_3d()
  {
    auto result = integrator_3d.cuhre(&integrand_3d, bounds_3d, nullptr);
    bool almosteq = get_almost_eq(result.integral, expected_3d,
                                  integrator_3d.epsabs,
                                  integrator_3d.epsrel);
    assert(almosteq);
  }


  void
  TestCuba::test_cuhre_3d_2()
  {
    auto result = integrator_3d_2.cuhre(&integrand_3d, bounds_3d, nullptr);
    bool almosteq = get_almost_eq(result.integral, expected_3d,
                                  integrator_3d_2.epsabs,
                                  integrator_3d_2.epsrel);
    assert(almosteq);
  }


  void
  TestCuba::test_cuhre_2d()
  {
    auto result = integrator_2d.cuhre(&integrand_2d, bounds_2d, &data);
    bool almosteq = get_almost_eq(result.integral, expected_2d,
                                  integrator_2d.epsabs,
                                  integrator_2d.epsrel);
    assert(almosteq);
  }

  void
  TestCuba::test_3d_inf()
  {
    // Vegas
    auto result = integrator_3d_inf.vegas(&integrand_3d_inf, bounds_3d_inf,
                                      nullptr);
    bool almosteq = get_almost_eq(result.integral, expected_3d_inf,
                                  integrator_3d_inf.epsabs,
                                  integrator_3d_inf.epsrel);
    assert(almosteq);

    // Suave
    result = integrator_3d_inf.suave(&integrand_3d_inf, bounds_3d_inf,
                                 nullptr);
    almosteq = get_almost_eq(result.integral, expected_3d_inf,
                             integrator_3d_inf.epsabs,
                             integrator_3d_inf.epsrel);
    assert(almosteq);

    // Cuhre
    result = integrator_3d_inf.cuhre(&integrand_3d_inf, bounds_3d_inf,
                                 nullptr);
    almosteq = get_almost_eq(result.integral, expected_3d_inf,
                             integrator_3d_inf.epsabs,
                             integrator_3d_inf.epsrel);
    assert(almosteq);

    // Divonne
    integrator_3d_inf.border = 10.; // Jacobian is singular at the border.
    result = integrator_3d_inf.divonne(&integrand_3d_inf, bounds_3d_inf,
                                        nullptr);
    almosteq = get_almost_eq(result.integral, expected_3d_inf,
                                  integrator_3d_inf.epsabs,
                                  integrator_3d_inf.epsrel);
    assert(almosteq);
  }
} // namespace cuba


int
main()
{
  cuba::TestCuba tester;
  tester.test_vegas_3d();
  tester.test_suave_3d();
  tester.test_divonne_3d();
  tester.test_cuhre_3d();
  tester.test_cuhre_3d_2();
  tester.test_cuhre_2d();
  tester.test_3d_inf();

  return 0;
}
