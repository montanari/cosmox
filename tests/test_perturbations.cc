// test_perturbations.cc -- Check perturbations module.

// Copyright 2017, 2018, 2019 Francesco Montanari

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "test_perturbations.h"


namespace cosmox {

  TestPerturbsCosmoclass::TestPerturbsCosmoclass(std::string inifile)
  {
    this->perturbs = Perturbations::create_perturbs(kCosmoclass,
                                                    inifile);
  }


  bool
  TestPerturbsCosmoclass::compare_vector(std::vector<double> computed,
                                         std::vector<double> expected)
  {
    const double abstol = 0.;
    const double reltol = 5e-4;

    bool almosteq = almost_eq(computed, expected, abstol, reltol);

    if (!almosteq)
      {
        std::cerr << "Expected    Computed:" << std::endl;
          for (size_t i=0; i<computed.size(); ++i)
            {
              std::cerr << expected[i] << "    " << computed[i]
                        << std::endl;
            }
      }

    return almosteq;
  }


  bool
  TestPerturbsCosmoclass::compare_source(std::vector<double> expected,
                                         Perturbations::SourceType stype)
  {
    const double abstol = 0.;
    const double reltol = 1e-4;

    size_t source_size = this->kgrid.size()*this->zgrid.size();
    if (source_size != expected.size())
      throw std::runtime_error("vectors have different size");

    // Compare the sources.
    std::vector<double> source(source_size);
    int count = 0;
    for (auto k: this->kgrid)
      for (auto z: this->zgrid)
        {
          source.at(count) = this->perturbs->get_source(stype, k, z);
          ++count;
        }

    bool almosteq = almost_eq(source, expected, abstol, reltol);

    // Print the sources if the comparison fails.
    if (!almosteq)
      {
        std::cout << "Expected    Computed" << std::endl;
        count = 0;
        for (auto k: this->kgrid)
          for (auto z: this->zgrid)
            {
              std::cerr << expected.at(count) << "    "
                        << source.at(count)
                        << std::endl;
              std::cerr << 1.-source.at(count)/expected.at(count)
                        << std::endl;
              ++count;
            }
      }

    return almosteq;
  }


  void
  TestPerturbsCosmoclass::bg_comoving_distance()
  {
    std::vector<double> expected = {3398.9, 13866.1};
    std::vector<double> computed;
    // Skip z=0, that would require abstol in compare_vector().
    for (size_t i=1; i<this->zgrid.size(); ++i)
      {
        computed.push_back(this->perturbs->bg_comoving_distance(this->zgrid.at(i)));
      }
    bool almosteq = compare_vector(computed, expected);
    assert(almosteq);
  }


  void
  TestPerturbsCosmoclass::bg_Hubble()
  {
    std::vector<double> expected {0.000225343, 0.000402203, 4.53505};
    std::vector<double> computed;
    for (size_t i=0; i<this->zgrid.size(); ++i)
      {
        computed.push_back(this->perturbs->bg_Hubble(this->zgrid.at(i)));
      }
    bool almosteq = compare_vector(computed, expected);
    assert(almosteq);
  }

  void
  TestPerturbsCosmoclass::bg_velocity_growth_factor()
  {
    std::vector<double> expected {0.524291, 0.875017, 0.836658};
    std::vector<double> computed;
    for (size_t i=0; i<this->zgrid.size(); ++i)
      {
        computed.push_back(this->perturbs->bg_velocity_growth_factor(this->zgrid.at(i)));
      }
    bool almosteq = compare_vector(computed, expected);
    assert(almosteq);
  }


  void
  TestPerturbsCosmoclass::get_k_lims()
  {
    std::vector<double> expected = {7.05963e-06, 23.9707};

    double k_min;
    double k_max;
    std::tie(k_min, k_max) = this->perturbs->get_k_lims();
    std::vector<double> computed = {k_min, k_max};

    bool almosteq = compare_vector(computed, expected);

    assert(almosteq);
  }


  void
  TestPerturbsCosmoclass::get_primordial_spectrum()
  {
    const std::vector<double> expected = {2.57101e-09, 2.15727e-09,
                                          1.96349e-09};

    if (this->kgrid.size() != expected.size())
      throw std::runtime_error("vectors have different size");
    std::vector<double> computed(this->kgrid.size());
    for (size_t i=0; i<this->kgrid.size(); ++i)
        computed[i] = this->perturbs->get_primordial_spectrum(this->kgrid[i]);

    bool almosteq = compare_vector(computed, expected);

    assert(almosteq);
  }


  void
  TestPerturbsCosmoclass::get_source_delta_m()
  {
    std::vector<double>
      source = {-19.465122, -11.833323, -0.017211, -16342.122626,
                -9935.484881, -25.516975, -52137.255084,
                -31697.948043,  -82.176724};
    bool almosteq = compare_source(source, Perturbations::kDeltaM);
    assert(almosteq);
  }


  void
  TestPerturbsCosmoclass::get_source_theta_m()
  {
    std::vector<double>
      source = {0.0022998, 0.00208115, 7.91662e-05, 1.93078, 1.74723,
                0.093228, 6.15994, 5.57433, 0.285642};
    bool almosteq = compare_source(source, Perturbations::kThetaM);

    assert(almosteq);
  }

} // namespace cosmox


int
main()
{
  using namespace cosmox;

  std::shared_ptr<TestPerturbsCosmoclass> test_pt_class;
  std::string datadir = DATADIR;
  test_pt_class = std::make_shared<TestPerturbsCosmoclass>(datadir+"test.ini");

  test_pt_class->get_source_delta_m();
  test_pt_class->get_source_theta_m();
  test_pt_class->get_primordial_spectrum();
  test_pt_class->get_k_lims();
  test_pt_class->bg_comoving_distance();
  test_pt_class->bg_Hubble();
  test_pt_class->bg_velocity_growth_factor();

  return 0;
}
