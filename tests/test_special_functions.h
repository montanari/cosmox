// test_special_functions.cc -- Check special functions.

// Copyright 2017, 2018, 2019 Francesco Montanari

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef TEST_SPECIAL_FUNCTIONS_H
#define TEST_SPECIAL_FUNCTIONS_H

#include "lib/special_functions.h"
#include "tutils.h"

namespace cosmox {

  typedef double (*BesselFn)(int, double);

  void test_sf_gamma();

  class TestBessel {
  public:
    void test_sf_bessel_j();
    void test_sf_d_bessel_j();
    void test_sf_dd_bessel_j();
    void test_sf_ddd_bessel_j();
  private:
    void assert_bessel(BesselFn bessel_fn, std::vector<double> expected,
                       double abstol);

    const std::vector<int> lvec = {0, 1, 2, 3, 4, 5, 6, 7, 10, 50,
                                   100, 1000, 10000};
    const std::vector<double> xvec = {0.1, 1.0, 4., 5., 8., 10., 20.,
                                      100., 1000.};
  };

  class TestWigner {
  public:
  TestWigner() : wig(2*100) {};

    void test_3j();
    void test_6j();
    void test_9j();

  private:
    Wigner wig;
  };

} /* namespace cosmox */

#endif /* TEST_SPECIAL_FUNCTIONS_H */
