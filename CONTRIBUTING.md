<!-- -*- mode: markdown -*- -->

This is CosmoX, a cosmological correlations calculator.

  Copyright 2017-2020 Francesco Montanari

  Copying and distribution of this file, with or without modification,
  are permitted in any medium without royalty provided the copyright
  notice and this notice are preserved.

Home page: https://gitlab.com/montanari/cosmox/

CosmoX is free software.  See the file COPYING for copying conditions.


# Autotools and Libtool

This distribution uses GNU Autotools and Libtool. If you are getting
the sources from git (or change `configure.ac`), you'll need to have
these tools installed to (re)build. All of these programs are
available from <ftp://ftp.gnu.org/gnu>.

*Debian users:*
```shell
apt install autotools-dev autoconf autoconf-archive libtool
```

## Configuration files

All the `Makefile`'s are automatically generated. They are meant to be
adapted by the users, but development should proceed through autotools
configuration files instead. This simplifies the procedure to obtain
highly portable `configure` and `Makefile` scripts.

See the
[Automake](https://www.gnu.org/software/automake/manual/automake.html)
manual for more information. In short, every source code
(sub-)directory to be processed by Automake must contain a
`Makefile.am` configuration file, to be updated when source files are
added (or removed). The `configure.ac` file should also be adapted
when needed, for instance to link a new library. Its creation or
modification can be assisted running the following command from the
top-level directory:

```shell
autoscan
```

This produces a `configure.scan` file based on the structure of the
code (and on the existing `configure.ac`). In principle
`configure.scan` can replace `configure.ac`, but in practice it likely
needs manual adjustments. Hence, it is mostly useful to assist the
modification of the existing `configure.ac`.

## Build the distribution

Generate the `configure` file running the `autogen` script:

```shell
sh autogen.sh
```

It is convenient to run `./configure` with a script exporting the
necessary flags (run as `source ./myconfigure.sh` to export flags also
in the current context).

```shell
#! /bin/bash
export LIBS="-L/path/to/class/ -L/path/to/wigxjpf/lib/ <...>"
export CPPFLAGS="-I/path/to/class/include/ -I/path/to/wigxjpf/inc <..>"
export CXXFLAGS="-Wall -ggdb3"
./configure
```

Install the program as described in the README, verifying that it
passes all the tests.

## Releases

Prepare the distribution:

```shell
make distcheck     # Remember to export non-installed library flags.
```

(If it fails try `make dist` instead, until the problem is fixed.)

This will create a tarball to install the software simply following
the [README](README.md) so that users do not need to build the
distribution from scratch with Autotools as outlined above. Releases
tarballs are distributed via the Gitlab repository. First, create a
tag, e.g.,

```shell
git tag -a v0.1 -m 'version 0.1'
git push --tags
```

Then go on Gitlab repository tags section, edit [release
notes](https://docs.gitlab.com/ee/workflow/releases.html) and attach
the tarball.


<!-- ## Third-Party Makefile's -->

<!-- A subdirectory could be a third-party project with its own build -->
<!-- system, not using Automake. It is possible to list arbitrary -->
<!-- directories in `SUBDIRS` or `DIST_SUBDIRS` provided each of these -->
<!-- directories has a `Makefile` that recognizes all the required -->
<!-- recursive targets (see section [23.2 Third-Party -->
<!-- Makefiles](https://www.gnu.org/software/automake/manual/html_node/Third_002dParty-Makefiles.html) -->
<!-- of Automake manual). Lightweight, standalone libraries likely not -->
<!-- packaged in Debian should be included this way to reduce the number of -->
<!-- dependencies to be built by the user. For instance, `src/wigxjpf` has -->
<!-- a crude Makefile that builds a library without installation (to use it -->
<!-- as an external library users should build it and then link it during -->
<!-- CosmoX `./configure` step). -->

<!-- The preferred way to proceed is to keep the third-party sources -->
<!-- untouched to ease upgrades to new versions. If possible, simply add a -->
<!-- `GNUmakefile.in` in the build directory and have it processed with -->
<!-- `AC_CONFIG_FILES` from the outer package. The `GNUmakefile.in` should -->
<!-- specify all the targets required by Automake, and redefine those -->
<!-- non-compliant. For example if we assume `Makefile` defines all targets -->
<!-- except the documentation targets, and that the check target is -->
<!-- actually called test, we could write `GNUmakefile.in` like this (see -->
<!-- section [23.2 Third-Party -->
<!-- Makefiles](https://www.gnu.org/software/automake/manual/html_node/Third_002dParty-Makefiles.html) -->
<!-- of Automake manual): -->

<!-- ``` -->
<!-- # First, include the real Makefile -->
<!-- include Makefile -->
<!-- # Then, define the other targets needed by Automake Makefiles. -->
<!-- .PHONY: dvi pdf ps info html check -->
<!-- dvi pdf ps info html: -->
<!-- check: test -->
<!-- ``` -->

<!-- If it is necessary to modify third-party sources, please create and -->
<!-- document patches using the `quilt` program. -->

<!-- *Important:* Before including third-party sources, always check that -->
<!-- they are distributed under a -->
<!-- [GPL-Compatible](https://www.gnu.org/licenses/license-list.en.html#GPLCompatibleLicenses) -->
<!-- free software license. -->


# Debug and profiling

To debug with [GDB](https://www.gnu.org/software/gdb/), configure the
installation including the following switches to `CXXFLAGS`:

```shell
./configure CXXFLAGS="-ggdb3"
```

Please run [Valgrind](http://valgrind.org/) for memory debugging.

Valgrind also provides a profiling tool,
[Callgrind](http://valgrind.org/docs/manual/cl-manual.html) (use
`KCachegrind` for graphical visualization of the data).


# Unit tests

Add tests for all relevant functions. Each module should have its
respective test file.

If `make check` fails, see the log files under the test directory for
the program output information.


# Coding Standards

The project follows GNU coding standards
<https://www.gnu.org/prep/standards/standards.html>. Please have a
look at least the highlighted points below and just copy the style of
existing code. Patches will be adapted if needed.

For detailed guidelines more specific to C++ please refer, in order of
importance, to
[GDB](https://sourceware.org/gdb/wiki/Internals%20GDB-C-Coding-Standards)
and [Google](https://google.github.io/styleguide/cppguide.html) coding
conventions, respectively. (In particular, differently from Google we
welcome exception handling.)

Finally, always refer unambiguously to the CLASS code (the Cosmic
Linear Anisotropy Solving System) to avoid confusion with references
to the program-code-template 'class' (for creating objects). As a
convention, in CosmoX source code CLASS should be referred to as
`cosmoclass` (capitalized as `Cosmoclass`).


## Highlights

> Thou shalt not cross 80 columns in thy
> file. ([Emacs Wiki](https://www.emacswiki.org/emacs/EightyColumnRule).)

Keep lines length to less than 80 characters and indent the code
meaningfully (using spaces, not tabs). This is extremely useful to
check the quality of the code. For instance, if more than three-level
nested control flow structures (for, if, ...) are present, this
typically results in long columns, suggesting that the algorithm
should almost surely be improved. (A correct formatting can be insured
with `clang-format`, see [.clang-format](.clang-format).)

Use CamelCase for classes and structs, and lower case letters
separated by underscores `_` for methods and variables (e.g.,
my_method). Names should be compact but meaningful.

Write short methods, meaning that they should do one thing and do it
well. As a rule of a thumb, if a function does not fit your head
(literally: if you put your head on the screen and the function does
not fit), then it is almost surely too long and you probably want to
re-factor it into smaller ones.


# Bugs and issues

- Valgrind reports `Possibly lost` bytes when calling
  `omp_get_num_threads()` in the `perturb_init()` function from the
  CLASS `perturbations.c` module. This is not ok (however, it cannot
  be fixed within CosmoX), unless interior-pointers are involved.

- Suave and Divonne algorithms (Cuba library) give memory leaks for
  some highly oscillating 3-dimensional integral. Results seem fine,
  but that's of course potentially dangerous.

- Setting `-Ofast` optimization flag causes linking problems with Cuba
  on some platform. That option is not portable so this can be
  expected. Try using, e.g., `-O3` instead.

- Several dependencies are now required and not all Python users know
  how to install C/C++ source files (especially on remote clusters
  without root privileges). Providing a Docker image does not make
  much sense because Docker is typically not available on clusters
  (however, one could import the image, e.g., with Singularity if
  available on the cluster) and, more importantly, developing Python
  packages using CosmoX within its Docker container would not quite be
  optimal. A solution to this problem may be to create a Python wheel,
  see commit
  [7d2feda9](https://gitlab.com/montanari/cosmox-dev/-/commit/7d2feda920ebdbf70eae2ca972705f623039c1c6).
