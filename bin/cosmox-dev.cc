// cosmox.cc -- main execution.

// Copyright 2017, 2018, 2019 Francesco Montanari

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <iostream>
#include "config.h"

void
print_version()
{
  std::cout << PACKAGE_STRING << std::endl;
}

/* Cosmox is meant to be used as a library. This program is solely
   provided as a fast testing ground during development.
*/
int
main (int argc, char **argv)
{
  print_version();
  return 0;
}
